cmake_minimum_required(VERSION 2.8)

project(obe)


ADD_DEFINITIONS(-std=c++11  -pthread -DARMA_DONT_USE_WRAPPER
-g
-Ofast
)

include_directories(/app/boost)
include_directories(/app/mongo/include)
include_directories(/app/arma/usr/include)

include_directories(Core)
include_directories(Generators)
include_directories(Calculators)
include_directories(Tests)

add_subdirectory(Core)
add_subdirectory(Generators)
add_subdirectory(Calculators)
#add_subdirectory(Tests)

set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} ")
#set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS}  -Wall -Werror -pedantic")

add_executable(obe main.cpp init.cpp)
add_executable(obe_cc main_cc.cpp init.cpp)
add_executable(obe2l main2l.cpp init.cpp)
add_executable(obe2l_daemon main2l_daemon.cpp init.cpp)
add_executable(obe_mag main_mag.cpp init.cpp)
add_executable(obe_d1 maind1.cpp init.cpp)
add_executable(energy_curve main_energy.cpp)
add_executable(obe_rb87 maind1Rb87.cpp init.cpp)
#add_executable(run_tests Tests/test_main.cpp Tests/assigment_test.cpp init.cpp)

target_link_libraries(obe_d1 core generators calc)
target_link_libraries(obe_cc core generators calc)
target_link_libraries(obe core generators  calc)
target_link_libraries(obe2l core generators  calc)
target_link_libraries(obe2l_daemon boost_program_options core generators  calc)
target_link_libraries(obe_mag core generators  calc)
target_link_libraries(obe_rb87 core generators  calc)
target_link_libraries(energy_curve boost_thread core generators calc)
#target_link_libraries(run_tests core generators calc  gtest)

