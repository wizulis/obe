
#include "assigment.hpp"
#include "scheduler.hpp"

void init();//defined in init.cpp



int main(int argc, char * argv[]){
 
    init();
    OBE::Scheduler sched(4);
    OBE::vec<OBE::dbl> rv={0.1,1};
    for(double x=1;x<60;){
        x*=2;
        rv.push_back(x);
    }
    rv=OBE::vec<OBE::dbl>{3.6,36,360};
    auto exp=  "533fa685de70e2c028322857";
    
    for(auto rab:rv){


      auto  BVec=OBE::vec<OBE::dbl>{0};//0.1,0.2,0.5,0.8,1,2,4,8,10,12,14,16,20,24,48,64,100};
     
      for(OBE::dbl B:BVec){

        std::shared_ptr<OBE::Assigment> asg(new OBE::Assigment());
        OBE::vec<std::string> mods={"N20","MT"};//,"Numeric"};

        asg->SetModification(mods);
        asg->SetExperiment(exp).SetB(B).SetDopplerP(-1.5,1.5,3.0/300).SetRabi(rab).SetOffset(0);
    asg->Marker="MAG_test4";
        sched.AddAssigment(asg);
    }
    }
    return 0;
}
