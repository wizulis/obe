#include "assigment.hpp"
#include "basicgenerator.hpp"
#include "statemixinggenerator.hpp"
#include "etcgenerator.hpp"
#include "basicsystem.hpp"
#include "linearzeeman.hpp"
#include "nonlinearzeeman.hpp"
#include "mtsystem.hpp"
#include "twogroundlaser.hpp"
#include "secondlasergenerator.hpp"
#include "fullequationgenerator.hpp"
#include "numericsystem.hpp"
#include "twolasersystem.hpp"
#include "transit_system.hpp"

void init(){
    using namespace OBE;
    AssigmentBase::registerGenerator<OBE::BasicGenerator>("BasicGenerator");
    AssigmentBase::registerGenerator<OBE::StateMixingGenerator>("StateMixingGenerator");
    AssigmentBase::registerGenerator<OBE::ETCGenerator>("ETCGenerator");
    AssigmentBase::registerGenerator<OBE::TwoGroundLaser>("TwoGroundLaserGenerator");
    AssigmentBase::registerGenerator<OBE::SecondLaserGenerator>("SecondLaserGenerator");
    AssigmentBase::registerGenerator<FullEqETC>("FullEqETC");

    AssigmentBase::registerSystem<OBE::BasicSystem>("BasicSystem");
    AssigmentBase::registerSystem<OBE::MTSystem>("MTSystem");
    AssigmentBase::registerSystem<OBE::NumericSystem>("NumericSystem");
    AssigmentBase::registerSystem<OBE::TwoLaserSystem>("TwoLaserSystem");
    AssigmentBase::registerSystem<OBE::TransitSystem<OBE::BasicSystem>>("TransitSystem");
    AssigmentBase::registerSystem<OBE::TransitSystem<OBE::TwoLaserSystem>>("2LTransitSystem");

    AssigmentBase::registerFrequencyCalculator<OBE::LinearZeeman>("LinearZeeman");
    AssigmentBase::registerFrequencyCalculator<OBE::NonLinearZeeman>("NonLinearZeeman");

}
