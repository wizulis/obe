#include "Core/transition.hpp"
#include "Core/assigmentbase.hpp"
#include "Calculators/nonlinearzeeman.hpp"
#include <iomanip>
#include "dbcontainer.hpp"
int main(){

    OBE::AssigmentBase base;
    base.trans=OBE::Transition("Cs","D2");
    OBE::DBContainer obj;
    base.trans.Energy_calc=std::unique_ptr<OBE::FrequencyCalculator>(new OBE::NonLinearZeeman(&base,obj));
    OBE::Rho r(base.trans);
    std::ofstream file("izvads.txt");


    for(double B=0;B<100;B+=0.002){
        base.trans.Energy_calc->SetB(B/1e4);
        file << B;
        for(auto i: r.ref(OBE::Rho::LEVEL::EXITED))
          file << "\t" << std::setprecision (15) << base.trans.Energy_calc->GetEnergie(i)/2/M_PI;
        file << std::endl;

    }
    return 0;
}
