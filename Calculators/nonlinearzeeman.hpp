#ifndef NONLINEARZEEMAN_HPP
#define NONLINEARZEEMAN_HPP

#include "frequencycalculator.hpp"
#include "matrix.hpp"

#include <mongo/client/dbclient.h>

namespace OBE {

    class NonLinearZeeman : public FrequencyCalculator{

        class MixingMatrix:public Matrix{
            std::map<int,int> indexMap;
        public:
            MixingMatrix();
            MixingMatrix(const std::map<int,int> & fmap);
            dbl Mixing(int Fa,int Fb)const;
            void Normalize();
        };

        std::map<Rho::LEVEL,std::map<int,MixingMatrix> > Mixings;
        //LEVEL,F,MF
        vec<std::map<int,vec<dbl> > > Energies;

    public:
        NonLinearZeeman(const AssigmentBase *parent,const DBContainer & p);

        virtual dbl GetEnergie(const Rho::State &a)const;
        virtual dbl MixingCoeficient(const Rho::State &,const Rho::State &) const override;
        virtual void SetB(const dbl val) override;
    private:
        void GenerateEnergies(Rho::LEVEL lvl,const int J_spin,const int I_spin,const dbl Lande_J,const dbl Lande_I,const int L_spin,std::map<int,dbl> freq);
    };


}

#endif // NONLINEARZEEMAN_HPP
