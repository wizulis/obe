#ifndef LINEARZEEMAN_HPP
#define LINEARZEEMAN_HPP

#include "frequencycalculator.hpp"
#include <mongo/client/dbclient.h>
#include "dbcontainer.hpp"

namespace OBE {

    class LinearZeeman : public FrequencyCalculator {
    public:
        LinearZeeman(const AssigmentBase * asg,const DBContainer  & p);

        dbl GetEnergie(const Rho::State &) const override;
        dbl LandeFactor(const Rho::State & i) const;
        dbl MixingCoeficient(const Rho::State &,const Rho::State &) const override;
    };
}
#endif // LINEARZEEMAN_HPP
