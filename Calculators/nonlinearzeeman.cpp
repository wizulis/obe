#include "assigment.hpp"
#include "transition.hpp"
#include "nonlinearzeeman.hpp"
#include <gsl/gsl_sf_coupling.h>

namespace OBE {

    NonLinearZeeman::MixingMatrix::MixingMatrix(){
        throw std::runtime_error("Mixing matrix empty");
    }

    NonLinearZeeman::MixingMatrix::MixingMatrix(const std::map<int, int> &fmap):Matrix(fmap.size(),fmap.size()),indexMap(fmap){

    }
    dbl NonLinearZeeman::MixingMatrix::Mixing(int Fa, int Fb)const{
        auto itr_a=indexMap.find(Fa);
        auto itr_b=indexMap.find(Fb);
        return (*this)(itr_a->second,itr_b->second);
    }
  void NonLinearZeeman::MixingMatrix::Normalize(){
    
    for(int i=0;i<colC;i++){
        dbl tmpVal=0;
        dbl max=0;

        for(int j=0;j<rowC;j++){

            tmpVal+=pow((*this)(j,i),2);
        }

        tmpVal=sqrt(tmpVal);

        if((*this)(i,i)<0)
            tmpVal=-tmpVal;

        for(int j=0;j<rowC;j++)
            (*this)(j,i)/=tmpVal;

    }


  }
    NonLinearZeeman::NonLinearZeeman(const AssigmentBase * parent,const DBContainer & p):FrequencyCalculator(parent){
        SetB(0);
    }

    dbl NonLinearZeeman::GetEnergie(const Rho::State &a) const{


        auto f_itr=Energies[a.lvl].find(a.F);
        if(f_itr!=Energies[a.lvl].end())
            return f_itr->second[(a.F+a.mF)/2];



        throw std::runtime_error("Invalid State, Energie not found");
    }


    dbl NonLinearZeeman::MixingCoeficient(const Rho::State & a,const Rho::State &b) const{
        if(a.lvl!=b.lvl)
            return 0;
        if(a.mF!=b.mF)
            return 0;

        auto lvl_itr=Mixings.find(a.lvl);
        if(lvl_itr==Mixings.end())
            throw std::runtime_error("Unable to find mixings");

        auto mf_itr=lvl_itr->second.find(a.mF);
        if(mf_itr==lvl_itr->second.end())
            throw std::runtime_error("Unable to find mixings");

        return mf_itr->second.Mixing(b.F,a.F);

    }

    void NonLinearZeeman::SetB(const dbl val){

        FrequencyCalculator::SetB(val);

        Mixings.clear();
        Energies.clear();
        Energies.resize(Rho::LEVEL::SIZE);
        GenerateEnergies(gr,
                         asg->trans.J_ground,
                         asg->trans.Nuclear_spin,
                         asg->trans.Lande_J,
                         asg->trans.Lande_nuclear,
                         asg->trans.L_ground,
                         asg->trans.Ground_freq);

        GenerateEnergies(ex,
                         asg->trans.J_exited,
                         asg->trans.Nuclear_spin,
                         asg->trans.Lande_J_exited,
                         asg->trans.Lande_nuclear,
                         asg->trans.L_exited,
                         asg->trans.Exited_freq);
    }

    void NonLinearZeeman::GenerateEnergies(Rho::LEVEL lvl,
                                           const int J_spin,
                                           const int I_spin,
                                           const dbl Lande_J,
                                           const dbl Lande_I,
                                           const int L_spin,
                                           std::map<int,dbl> freq){

        int Fmax=J_spin+I_spin;

        for(int mF=-Fmax;mF<=Fmax;mF+=2){
            std::map<int,int> fInd;
            vec<int> Fvec;

            //Iterate over F values,
            // Store in Fvec thoes values that are possible with current mF
            // Store F locations in fInd map for MixingMatrix to lookup later

            for(int i=0,F=std::abs(J_spin-I_spin);F<=std::abs(J_spin+I_spin);F+=2){
                if(std::abs(mF)<=F){
                    fInd[F]=i;
                    Fvec.push_back(F);
                    i++;
                }
            }

            Matrix Hmat(Fvec.size(),Fvec.size());
            for(std::size_t i=0;i<Fvec.size();i++)
                Hmat(i,i)=freq[Fvec[i]];

            for(std::size_t i=0;i<Fvec.size();i++){
                for(std::size_t j=0;j<Fvec.size();j++){
                    Hmat(i,j)+=MBOHR/HBAR*B*sqrt((Fvec[i]+1)*(Fvec[j]+1))
                            *pow(-1,(J_spin+I_spin+Fvec[i]+Fvec[j]-mF+2)/2)
                            *gsl_sf_coupling_3j(Fvec[i],2,Fvec[j],-mF,0,mF)
                            *(
                              Lande_J*sqrt(J_spin/2.0*(J_spin/2.0+1)*(J_spin+1))
                              *gsl_sf_coupling_6j(J_spin,Fvec[i],I_spin,Fvec[j],J_spin,2)

                              +Lande_I*sqrt(I_spin/2.0*(I_spin/2.0+1)*(I_spin+1))
                              *gsl_sf_coupling_6j(I_spin,Fvec[i],L_spin,Fvec[j],I_spin,2)
                            );
                }
            }

            vec<dbl> energ;
            //if(lvl==Rho::LEVEL::GROUND)
            //std::cout << "MF " << mF << std::endl;
            MixingMatrix mix(fInd);
            Hmat.EigenValues(energ,mix);

            mix.Normalize();
            //if(lvl==Rho::LEVEL::GROUND)
           // mix.print();

            Mixings[lvl].insert(std::make_pair(mF,mix));
            for(std::size_t i=0;i<Fvec.size();i++)
                Energies[lvl][Fvec[i]].push_back(energ[i]);

        }
       // if(lvl==Rho::LEVEL::GROUND)

        //getchar();

    }
}
