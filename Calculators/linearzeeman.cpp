#include "linearzeeman.hpp"
#include "assigment.hpp"

namespace OBE {
    LinearZeeman::LinearZeeman(const AssigmentBase *  asg_ptr,const DBContainer & p):FrequencyCalculator(asg_ptr){

    }

    dbl LinearZeeman::GetEnergie(const Rho::State & i) const{


        std::map<int,dbl>::const_iterator freq_itr;
        if(i.lvl==ex){
            freq_itr=asg->trans.Exited_freq.find(i.F);
            if(freq_itr==asg->trans.Exited_freq.end())
                throw std::runtime_error("Unable to find energie of a level");
        }
        else{
            freq_itr=asg->trans.Ground_freq.find(i.F);
            if(freq_itr==asg->trans.Ground_freq.end())
                throw std::runtime_error("Unable to find energie of a level");
        }
        return freq_itr->second+i.mF/2.0*LandeFactor(i)*B*MBOHR/HBAR;

    }

    dbl LinearZeeman::LandeFactor(const Rho::State & a) const{
        if(a.F==0)
          return 0;

        dbl lande_J,Jp,
                I=asg->trans.Nuclear_spin,
                F=a.F;

        if(a.lvl==ex){
            lande_J=asg->trans.Lande_J_exited;
            Jp=asg->trans.J_exited;
        }else{
            lande_J=asg->trans.Lande_J;
            Jp=asg->trans.J_ground;
        }

        return lande_J*((F*F/4.0+F/2.0)-(I*I/4.0+I/2.0)+(Jp*Jp/4.0+Jp/2.0))/F/(F/2.0+1)+
             asg->trans.Lande_nuclear*((F*F/4.0+F/2.0)+(I*I/4.0+I/2.0)-(Jp*Jp/4.0+Jp/2.0))/F/(F/2.0+1);

    }

    dbl LinearZeeman::MixingCoeficient(const Rho::State &a,const Rho::State &b) const{
        if(a.lvl!=b.lvl)
            return 0.0;

        if(a.F==b.F && a.mF==b.mF)
            return 1.0;
        else
            return 0.0;
    }
}
