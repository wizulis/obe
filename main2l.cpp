#include "assigment.hpp"
#include "scheduler.hpp"

void init();//defined in init.cpp
#include "twogroundlaser.hpp"

int main(int argc, char * argv[]){
 
    init();
    int c=0;
    OBE::Scheduler sched(8);
    auto BVec=OBE::vec<OBE::dbl>{0,0.1,0.3,0.5,1,2,4,8};
	//auto BVec=OBE::vec<OBE::dbl>{2,4};
    for(auto & exp:std::vector<std::string>{
	//	              "533a79b63746749fdb9baf2a",//3-3
	  "533fa5abde70e2c028322855"//4-4
	  })
      for(auto & trans:std::vector<std::string>{/*"SUB_3-3_N6","SUB_3-4_N6",*/"SUB_4-3_N6"})
	for(auto sec:OBE::vec<OBE::dbl>{300})
	  for(auto rab:OBE::vec<OBE::dbl>{300})
	    for(OBE::dbl B:BVec){
       c++;
//       if(exp=="533fa5abde70e2c028322855" && (trans=="SUB_3-3_N6"))//..||trans=="SUB_4-3_N6"))
//       		 continue;
//       if(exp!="533fa5abde70e2c028322855" && (trans=="SUB_3-4_N6"))// || trans=="SUB_4-3_N6" ))
//	 	 continue;
       //if(c<=24)
       // continue;*/
        std::shared_ptr<OBE::Assigment> asg(new OBE::Assigment());
        OBE::vec<std::string> mods={"N20","MR10","FWHM1.35",trans};
        asg->Marker="2L_050315_"+std::to_string((int)sec);
        asg->SetModification(mods);
        asg->SetExperiment(exp).SetB(B).SetDopplerP(-2,2,4.0/400).SetRabi(rab).SetOffset(0);
        asg->Load();
        dynamic_cast<OBE::TwoGroundLaser*>(asg->generator.get())->SetSecondLaserIntensity(sec);
        sched.AddAssigment(asg);
    }

    return 0;
}
