#include <gtest/gtest.h>

#include "assigment.hpp"
#include <memory>
#include <iostream>


std::unique_ptr<OBE::Assigment> load_assigment(){
    std::unique_ptr<OBE::Assigment> asg = std::unique_ptr<OBE::Assigment>(new OBE::Assigment);
    asg->SetExperiment("53175422dc2c2c59192d9fa3").SetB(0);
    asg->Load();
    return asg;
}

TEST(ASSIGMENTLOADING,CorrectTransiton){
    auto asg=load_assigment();
    EXPECT_EQ(asg->GetParam(OBE::Assigment::PARAMS::MAGNETIC_FIELD),0);
    EXPECT_EQ(asg->trans.element,"Cs");
    EXPECT_EQ(asg->trans.Nuclear_spin,7);
    EXPECT_FLOAT_EQ(asg->trans.Atom_mass,132.905452*1e-3);
}
