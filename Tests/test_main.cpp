#include <gtest/gtest.h>

void init();

int main(int argc, char * argv[]){
    init();
    ::testing::InitGoogleTest(&argc,argv);

    return RUN_ALL_TESTS();
}
