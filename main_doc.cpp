
#include "assigment.hpp"
#include "scheduler.hpp"

void init();//defined in init.cpp
#include "etcgenerator.hpp"

int main(int argc, char * argv[]){
 
  using OBE::vec;
  using OBE::dbl;
    init();
    OBE::Scheduler sched(4);
    //   auto BVec=OBE::vec<OBE::dbl>{0.5};for(OBE::dbl B=0;B<10;B+=1)BVec.push_back(B);
    auto BVec=vec<dbl>{0,0.05,0.1,0.2,0.5,1,2,3,4,10,20,40};
    using std::sqrt;
    vec<dbl> rr{1};


    for(OBE::dbl rab: rr){
        for(const std::string & def_id:
	      std::vector<std::string>{
	      "533fa685de70e2c028322857"
	      ///	      "5342eed9ab9242c56bd9dca2"
 	  }){
    for(OBE::dbl B:BVec){

                std::shared_ptr<OBE::Assigment> asg(new OBE::Assigment());
                OBE::vec<std::string> mods={"N100"};
		asg->marker="DOC";
                asg->SetModification(mods);
		asg->SetExperiment(def_id).SetB(B).SetDopplerP(-1,1,2.0/200).SetRabi(rab).SetOffset(0);
                sched.AddAssigment(asg);
            }
        }
    }
    return 0;
}
