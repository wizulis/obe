
#include <boost/date_time/posix_time/ptime.hpp>
#include <boost/date_time/gregorian/gregorian.hpp>
#include <boost/program_options.hpp>
void init();//defined in init.cpp
#include "assigment.hpp"
#include "scheduler.hpp"
#include "twogroundlaser.hpp"

#include <sys/wait.h>
int main(int argc, char * argv[]){

  auto now = boost::posix_time::second_clock::local_time();
  auto now_d = now.date();
  std::string tmp = boost::gregorian::to_iso_string(now_d);
  std::string marker_d = tmp.substr(6,2)+tmp.substr(4,2)+tmp.substr(2,2);//ddmmyy


  auto pid1 = fork();

  if(!pid1) {
    assert(!chdir("/"));
    setsid();
    auto pid2 = fork();
    if(!pid2) {
      assert(freopen(("/home/uldis/logs/"+marker_d+"_"+std::to_string(getpid())+".log").c_str(),"w",stdout));
    } else {
      exit(0);
    }
  } else {
    int status;
    waitpid(pid1,&status,0);
    return status;
  }

  namespace po = boost::program_options;
  po::options_description desc("Allowed options");
  desc.add_options()
    ("help,h", "produce help message")
    ("rabi,r", po::value<std::vector<double>>(), "rabi list")
    ("second_rabi,s", po::value<std::vector<double>>(), "second rabi list")
    ;

  po::variables_map vm;
  po::store(po::parse_command_line(argc, argv, desc), vm);
  po::notify(vm);

  if (vm.count("help")) {
    std::cout << desc << "\n";
    return 1;
  }

  std::vector<double> rabis, sec_rabis;
  if (vm.count("rabi") == 0 || vm.count("second_rabi") == 0 ) {
    std::cout << "Rabi frequencies not set\n";
    return -1;
  } else {
    rabis = vm["rabi"].as<std::vector<double>>();
    sec_rabis = vm["second_rabi"].as<std::vector<double>>();
  }

  init();
  OBE::Scheduler sched(8);
  auto BVec=OBE::vec<OBE::dbl>{0,0.1,0.3,0.5,1,2,4,8};
  std::string exp = "533fa5abde70e2c028322855";//4-4
//  std::string trans = "SUB_4-3_N6";
  std::string trans ="SUB_4-3_N6_W0.26";

  std::cout << "Starting calculation for: ";

  for(auto rab: rabis)
    for(auto sec:sec_rabis){
      std::cout << "\t"<<rab << "\t"<<sec << std::endl;
      for(OBE::dbl B:BVec){
	std::shared_ptr<OBE::Assigment> asg(new OBE::Assigment());
	OBE::vec<std::string> mods={"N20","MR10","FWHM1.35",trans};
	asg->Marker="2L_"+marker_d+"_"+std::to_string((int)sec);
	asg->SetModification(mods);
	asg->SetExperiment(exp).SetB(B).SetDopplerP(-2,2,4.0/400).SetRabi(rab).SetOffset(0);
	asg->Load();
	dynamic_cast<OBE::TwoGroundLaser*>(asg->generator.get())->SetSecondLaserIntensity(sec);
	sched.AddAssigment(asg);
      }
    }
  return 0;
}
