#ifndef OBESYSTEM_H
#define OBESYSTEM_H
#include <string>
#include <map>
#include <memory>
#include <functional>

#include "obegenerator.h"
#include "obematrix.h"
#include "assigment.hpp"

struct RadP
{
  dbl vol;
  dbl x1,x2;
  dbl y1,y2;
  dbl r;
};


class OBEsystem
{

public:
  
  bool Progress;

  OBEsystem(std::shared_ptr<OBEGenerator>,OBEmatrix::MatTypes matType);
  std::string GetConfig(){return ConfigName;}
  virtual void InitConfig(std::string conf);
  
  void 	SetRabiFreq(dbl Rab);
  dbl 	GetRabiFreq();
  virtual dbl 	AvgIntensity();
  void SetB(dbl val);
  dbl GetB();
  dbl SpeedProb(dbl v);
  
  virtual vec<vec<dbl>> GenerateMatrixAndSolve();

  vec<vec<dbl>> Calculate(dbl from, dbl to,int steps);
  std::map<dbl,vec<dbl>> CalculateMT(dbl from,dbl to, int steps);
  
  void PrintRho(cvec&);
  virtual void test();
  
  int GetN(){return nReg;}
  int GetNF(){return nFull;}
  virtual void SetN(int);
  

  dbl GetDoplerStdDev();
  dbl GetMaxSpeed();
  std::string PrintFluroVec(int);
  
  void CalculateAssigment(std::shared_ptr<OBEassigment> a);
  virtual ~OBEsystem(){}
  
protected:
  
  void	SetLocalN(int n);
  virtual void 	GenerateIntensityDistribution();

  dbl 	PIntens;
  dbl 	RedMElem;
  

  vec<std::unique_ptr<OBEmatrix>> mat;

  std::shared_ptr<OBEGenerator> systemGenerator;
  
  vec <dbl> laserIntens;
  vec <RadP> laserRad;
  
  vec <dbl> RadPoints;
  
  dbl 	FWHM;
  dbl 	MaxRad;
  dbl 	TRC;
  
  std::string ConfigName;
  OBEmatrix::MatTypes matT;
  int size;
  int nReg;
  int nFull;

  dbl 	FlowSpeed;				//Plumsas starp apgabaliem atrums
  dbl 	LaserRadStep;
 
};
double GetVol(double R, double y1,double y2);
#endif // OBESYSTEM_H
