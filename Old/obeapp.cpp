#include "obeapp.h"
#include <thread>
#include <iostream>
#include <fstream>
#include <sstream>
#include <future>
#include <boost/asio.hpp>

using boost::asio::ip::tcp;



void OBEapp::CreateSocket(int p)
{
  port_=p;
  new std::thread(&OBEapp::ListenToCommands,this);
}

void OBEapp::ListenToCommands()
{
  boost::asio::io_service service;
  tcp::acceptor acceptor(service,tcp::endpoint(tcp::v4(),port_));
  for(;;)
  {
    tcp::socket socket(service);
    acceptor.accept(socket);
    for(;;)
    {   
      std::vector<char> data(1000); 
      boost::system::error_code ignored_error;
      
      size_t len = socket.read_some(boost::asio::buffer(data), ignored_error);
      if(len==0)
	break;
      
      std::string cmd(data.begin(),data.begin()+len);
      if(cmd == std::string("Results"))
      {
	resultMut.lock();
	try
	  {
	  std::ostringstream ss;
	  //Send Number of Results;
	  ss << Results.size();
	  boost::asio::write(socket, boost::asio::buffer(ss.str()),ignored_error);
	  //Send Each Result
	  for(int i=0;i<Results.size();i++)
	  {
	    ss.str("");
	    for(int j=0;j<Results[i].size();j++)
	    {
	      ss << Results[i][j];
	      
	      if(j+1!=Results[i].size())
		ss << '\t';
	    }
	    boost::asio::write(socket, boost::asio::buffer(ss.str()),ignored_error);
	    len = socket.read_some(boost::asio::buffer(data), ignored_error);
	  }
	  
	  Results.erase(Results.begin(),Results.end());
	}
	catch (...)
	{
	  std::cout << "Had Error on Results CMD\n";
	  
	}
	resultMut.unlock();
      }
      else if(cmd == std::string("AddAssigment"))
      {
	boost::asio::write(socket,boost::asio::buffer(std::string("Waiting")));
	len = socket.read_some(boost::asio::buffer(data), ignored_error);
	
	if(len==0)
	  break;
	std::stringstream ss;
	ss << std::string(data.begin(),data.begin()+len);
	boost::archive::text_iarchive ia(ss);
	Assigment asag;
	ia >> asag;
	std::cout << "Recived Assigment " << asag.N << " " << asag.Rabi << " " << asag.B << " " << asag.OutputFile << std::endl; 
	AddAssigment(asag);
      }
    }
  }
}

OBEapp::OBEapp(): isRunning(false)
{

}


// void OBEapp::Calc(OBEsystem* sy, std::string outFile)
// {
// 
//   sy->Progress=false;
//   auto a = sy->Calculate();
//   
//   fileMut.lock();
//   std::ofstream out(outFile,std::ios::app);
//   out << sy->GetRabiFreq() << "\t" << sy->GetB();
//   for(auto i: a)
//     out << "\t" << i;
//   out << std::endl;
//   out.close();
//   fileMut.unlock();
//   std::vector <double> re;
//   re.push_back(sy->getN());
//   re.push_back(sy->GetRabiFreq());
//   re.push_back(sy->GetB());
//   re.insert(re.end(),a.begin(),a.end());
//   resultMut.lock();
//   Results.push_back(re);
//   resultMut.unlock();
// }
std::vector<std::map<double,double>> OBEapp::Calc2(OBEsystem* sy, std::pair<int,int> thrC)
{
  if(thrC.second!=0)
    sy->Progress=false;
  else
    sy->Progress=true;

  double speedmin,speedmax;
  speedmin=-sy->getMaxSpeed()+(2*sy->getMaxSpeed())/thrC.first*thrC.second;
  speedmax=-sy->getMaxSpeed()+(2*sy->getMaxSpeed())/thrC.first*(thrC.second+1);
  return sy->Calculate(speedmin,speedmax,500/thrC.first);

}

void OBEapp::AddAssigment(Assigment s)
{
  assiMut.lock();
  assigments.push_back(s);
  assiMut.unlock();
}

// void OBEapp::Calculate(int threads, std::string Config,SystemType sysT,SolverType solv)
// {
//   if(assigments.size()==0)
//     return;
//   
//   for(int i=0;i<threads;i++)
//   {
//     if(solv==UMF)
//       matr.push_back(new UMFPACKmatrix);
//     else if(solv==ARM)
//       matr.push_back(new ARMAmatrix);
//     
//     if(sysT==LINEAR)
//       sys.push_back(new OBEsystem(Config,matr[i],assigments[i<assigments.size()? i:0].N));
//     else if (sysT==NONLINEAR)
//       sys.push_back(new OBEnonLinear(Config,matr[i],assigments[i<assigments.size()? i:0].N));
//   }
//   isRunning=true;
//  
//   while(isRunning)
//   {
//    if(assigments.size()==0)
//    {
//      std::cout << "Nothing to do....Sleeping for 2 seconds" << std::endl;
//      sleep(2);
//      break;
//      continue;
//    }
//    
//    
//    for(int i=0;i<sys.size() && i< assigments.size();i++)
//    {
//      if(sys[i]->getN()!=assigments[i].N)
//      {
//        delete sys[i];
//        sys[i]=new OBEsystem(Config,matr[i],assigments[i].N);
//      }
//      sys[i]->SetRabiFreq(assigments[i].Rabi);
//      sys[i]->SetB(assigments[i].B);
//    } 
//    std::vector<std::thread *> thr;
//    for(int i=0;i<sys.size() && i < assigments.size();i++)
//     thr.push_back(new std::thread(&OBEapp::Calc,this,sys[i],assigments[i].OutputFile));
//    for(int i=0;i<thr.size();i++)
//     thr[i]->join();
//   
//   assiMut.lock();
//   for(int i=0;i<thr.size();i++)
//     assigments.erase(assigments.begin());
//   assiMut.unlock();
//   }
// }
#include <chrono>
void OBEapp::SingleAssigmentCalculate(int threads,std::string Config,SystemType sysT,SolverType solv)
{
  if(assigments.size()==0)
    return;
  for(int i=0;i<threads;i++)
    {
      if(solv==UMF)
	matr.push_back(new UMFPACKmatrix);
      else if(solv==ARM)
	matr.push_back(new ARMAmatrix);

      if(sysT==LINEAR)
	sys.push_back(new OBEsystem(Config,matr[i],assigments[0].N));
      else if (sysT==NONLINEAR)
	sys.push_back(new OBEnonLinear(Config,matr[i],assigments[0].N));
      sys[i]->SetRabiFreq(assigments[0].Rabi);
    }
  isRunning=true;

  while(isRunning)
    {
      if(assigments.size()==0)
	{
	  std::cout << "Nothing to do....Sleeping for 2 seconds" << std::endl;
	  break;
	  continue;
	}


      for(int i=0;i<threads;i++)
	{
	  if(sys[i]->getN()!=assigments[0].N)
	    {
	      delete sys[i];
	      sys[i]=new OBEsystem(Config,matr[i],assigments[0].N);
	      sys[i]->SetRabiFreq(assigments[0].Rabi);
	    }
	  if(std::abs(sys[i]->GetRabiFreq()-assigments[0].Rabi)>1e-3)
	    sys[i]->SetRabiFreq(assigments[0].Rabi);
	  sys[i]->SetB(assigments[0].B);
	}
	auto xxx=std::chrono::steady_clock::now();
      std::vector<std::future<std::vector<std::map<double,double>>>> thr;
      for(int i=0;i<threads;i++)
	thr.push_back(std::async(std::launch::async,&OBEapp::Calc2,this,sys[i],std::pair<int,int>(threads,i)));

      std::vector<std::vector<std::map<double,double>>> result;

      for(int i=0;i<thr.size();i++)
     	result.push_back(thr[i].get());
      

      std::vector<std::map<double,double>> output(result[0].size());
      for(auto & r: result)
      {
	for(int j=0;j<r.size();j++)
	    for(auto &b:r[j])
	      output[j][b.first]+=b.second;
      }

      
      std::ofstream of(assigments[0].OutputFile.c_str(),std::ios::app);


      for(int i =0;i<output.size();i++)
      {
	auto & x = output[i];
	of << assigments[0].Rabi << "\t" << assigments[0].B;
	of << "\t" << sys[0]->PrintFluroVec(i);
	for(auto &y:x)
	  of << "\t[" << y.first << "," << y.second <<"]";
	of << std::endl;
      }
      
      
	std::cout << "                                                                                    \r" << std::flush;
      std::cout << assigments[0].Rabi << "\t" << assigments[0].B;
     /* for( double x: output)
	std::cout << "\t" << x;*/
      std::cout << " Done in: " << 
std::chrono::duration_cast<std::chrono::seconds>(std::chrono::steady_clock::now()-xxx).count() 
<<  std::endl;
      assiMut.lock();
      assigments.erase(assigments.begin());
      assiMut.unlock();

    }

}

