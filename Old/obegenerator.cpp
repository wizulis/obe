#include "obegenerator.h"

#include <gsl/gsl_sf_coupling.h>
#include <gsl/gsl_integration.h>

#include <boost/property_tree/xml_parser.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/foreach.hpp>
#include <boost/lexical_cast.hpp>



OBEGenerator::OBEGenerator(const std::string & config)
{
  InitConfig(config);
   
}
void OBEGenerator::InitConfig(const std::string & config)
{
    if(ConfigName==config)
        return;

    using boost::property_tree::ptree;
    ConfigName=config;
    int FgT,FeT;
    dbl TrFr;
    
    std::ifstream file(config.c_str());

    if(file.is_open())
    {
      ptree pt;
      read_xml(config, pt);
      
      J=pt.get<int>("Config.J");
      Jp=pt.get<int>("Config.Jp");
      gJ=pt.get<dbl>("Config.gJ");
      gJp=pt.get<dbl>("Config.gJp");
      
      try //If I is not defined
      {
	I=pt.get<int>("Config.I");
	gINuc=pt.get<dbl>("Config.gI");
      
      }
      catch (...)
      {
	std::cout << "Setting Nuclear spin to 0" << std::endl;
	I=0;
	gINuc=0;
      }
      
      Gamma=pt.get<dbl>("Config.Gamma")*1e6*2*M_PI;
      cTransFreq=pt.get<dbl>("Config.TransitionFrequency")*1e12*2*M_PI;
      
      FgT=pt.get<int>("Config.Transition.Fg");
      FeT=pt.get<int>("Config.Transition.Fe");
      
      TrFr=pt.get<dbl>("Config.Transition.Detuning")*1e6*2*M_PI;
      LaserWidth=pt.get<dbl>("Config.LaserWidth")*1e6*2*M_PI;
      
      RedMElem=pt.get<dbl>("Config.ReducedMatrixElement")*ELECTRON_CHARGE*BOHR_RADIUS;
   
      FWHM=pt.get<dbl>("Config.FWHM");

      MolMass=pt.get<dbl>("Config.AtomMass")*1e-3;
      Temp=pt.get<dbl>("Config.Temperature");
   
      SetPolar(pt.get<dbl>("Config.LaserPolarization.x"),
	       pt.get<dbl>("Config.LaserPolarization.y"),
	       pt.get<dbl>("Config.LaserPolarization.z"));
         
      try 
      {
	ColProb = pt.get<dbl>("Config.ColisionProbability")*1e6*2*M_PI;
      }
      catch (...)
      {
	std::cout << "Setting Colision Probability to 0" << std::endl;
	ColProb=0;
      }

     
      BOOST_FOREACH(ptree::value_type &v, pt.get_child("Config.Fluorescence"))
        SetFluro(v.second.get<dbl>("x"),
		 v.second.get<dbl>("y"),
		 v.second.get<dbl>("z"));
      
      

      BOOST_FOREACH(ptree::value_type &v, pt.get_child("Config.ExitedFreqShift"))
        freqExited.push_back(boost::lexical_cast<dbl>(v.second.data())*1e6*2*M_PI);
      
      BOOST_FOREACH(ptree::value_type &v, pt.get_child("Config.GroundFreqShift"))
      	freqGround.push_back(boost::lexical_cast<dbl>(v.second.data())*1e6*2*M_PI);
      
      //Try to fill ground levels numbers from config file.
      try
	{
	  BOOST_FOREACH(ptree::value_type &v, pt.get_child("Config.LevelsGround"))
	    {
	      fG.push_back(boost::lexical_cast<int>(v.second.data()));
	    }
	}
      catch(...)
	{
         //If no levels were specified, calculate levels ourselfs. 
   
	for(int i=abs(J-I); i<=abs(J+I); i+=2)
	  fG.push_back(i);
	}   
      if(freqGround.size()!=fG.size())
	throw std::logic_error("Level count does not equal frequency count for ground state");
     

      try{

      BOOST_FOREACH(ptree::value_type &v,pt.get_child("Config.LevelsExited"))
	{
	  fE.push_back(boost::lexical_cast<int>(v.second.data()));
	}
      }
      catch(...)
	{

      
	for(int i=abs(Jp-I); i<=abs(Jp+I); i+=2)
	  fE.push_back(i);
	}
      if(freqExited.size()!=fE.size())
	throw std::logic_error("Level count does not equal frequency count for exited state");

      sizeG=0;
      sizeE=0;
		
      for(std::size_t i=0; i<fG.size(); i++)
        sizeG+=fG[i]+1;
      for(std::size_t i=0; i<fE.size(); i++)
        sizeE+=fE[i]+1;
      
      sizeM=sizeG*sizeG+sizeE*sizeE;
      
      
    }
    else
    {
      std::cout << "Unable to Open file " << config << std::endl; throw -1;
    }

    DoplerSpeed=0;
    SetB(0);
    LaserIntens=1;
    FlowSpeed=sqrt(M_PI*MOL_R*Temp/MolMass/2);
  //  GenerateSymbols();
    
    Gst g(fG,FgT);
    Est e(fE,FeT);
    cLaserFreq=std::abs(TransitionFrequency(g,e)+TrFr);
}

dbl OBEGenerator::GetFlowSpeed()const
{
  return sqrt(M_PI*MOL_R*Temp/MolMass/2);
}

vec< dbl > OBEGenerator::CalculateFluorescence(const vec< cdouble > &rhoV)const
{
  vec<dbl> ret;
  ret.reserve(fluro.size());
  for(auto &vec: fluro)
  {
    cdouble sum(0,0);
    for(Est r(fE); r.notFinal(); ++r)
      {
	for(Est s(fE); s.notFinal(); ++s)
	{
	  for(Gst m(fG); m.notFinal(); ++m)
	  {    
	    sum+=TransitionAmplitude(m,r,vec)*TransitionAmplitude(s,m,vec)*rhoV[gI(r,s)];  
	  } 
	}
      }
      ret.push_back(sum.real()/RedMElem/RedMElem*Gamma);
  }
  return ret;
}


vec<cvec> OBEGenerator::CreateRow(const State & i, const State & j,
                         const vec<dbl> & LaserIntens,
                         vec<cvec> & row)
{
  int nReg=LaserIntens.size();
  
  
  for(int i=0;i<nReg;i++)
  {
    row.push_back(cvec(sizeM,cdouble(0,0)));
  }
  
  if(!i.isExited())
  {
    //First sum
    for(Est k(fE); k.notFinal(); ++k)
	{
	for(Est m(fE); m.notFinal(); ++m)
	{
	    
	    cdouble threej=TransitionAmplitude(i,k,e)*TransitionAmplitude(m,j,e);
	    for(int dal=0; dal<nReg; dal++)
	    {
	      cdouble o(1,0);
	      cdouble x((Gamma+LaserWidth)/2, (cLaserFreq-DoplerShift()-TransitionFrequency(m,i)));
	      x=o/x;
	      cdouble y((Gamma+LaserWidth)/2, -(cLaserFreq-DoplerShift()-TransitionFrequency(k,j)));
	      y=o/y;
	      x=x+y;
	      row[dal][gI(k,m)]+=x*threej*LaserIntens[dal]/HBAR/HBAR;
	    }
	}
    }//END First sum
    //Second and Third sum
    for(Est k(fE); k.notFinal(); ++k)
    {
	for(Gst m(fG); m.notFinal(); ++m)
	{
	   
	    cdouble threej1=TransitionAmplitude(i,k,e)*TransitionAmplitude(k,m,e);
	    cdouble threej2=TransitionAmplitude(m,k,e)*TransitionAmplitude(k,j,e);
	    for(int dal=0; dal<nReg; dal++)
	    { 
	      cdouble o(1,0);
	      cdouble x((Gamma+LaserWidth)/2, -(cLaserFreq-DoplerShift()-TransitionFrequency(k,j)));
	      cdouble y((Gamma+LaserWidth)/2, (cLaserFreq-DoplerShift()-TransitionFrequency(k,i)));
	     
	      x=o/x;
	      y=o/y;
	      
	      row[dal][gI(m,j)]-=x*threej1*LaserIntens[dal]/HBAR/HBAR;
	      row[dal][gI(i,m)]-=y*threej2*LaserIntens[dal]/HBAR/HBAR;
	    }
	}
    }//End Second and Third sum


    
    //Spontanius decey
    for(Est k(fE); k.notFinal(); ++k)
    {
	for(Est m(fE); m.notFinal(); ++m)
	{
	    cdouble tmpV=cdouble(GammaSpont(i,k,m,j),0);
	    for(int dal=0; dal<nReg; dal++)
	    {
	      row[dal][gI(k,m)]+= tmpV;
	    }
	}
    }
    //Groud state spliting
    for(int dal=0;dal<nReg;dal++)
    {
      row[dal][gI(i,j)]-=cdouble(0,TransitionFrequency(i,j));

//       //Collisition relaxation
//       row[dal][gI(i,j)]-=cdouble(ColProb,0.0);

    }
    
  }
  else
  {

    for(Gst k(fG); k.notFinal(); ++k)
    {
	for(Gst m(fG); m.notFinal(); ++m)
	{


	    cdouble threej=TransitionAmplitude(i,k,e)*TransitionAmplitude(m,j,e);

	    for(int dal=0; dal<nReg; dal++)
	    {
	      cdouble o(1,0);
	      cdouble x((Gamma+LaserWidth)/2, -(cLaserFreq-DoplerShift()-TransitionFrequency(i,m)));
	      x=o/x;
	      cdouble y((Gamma+LaserWidth)/2,(cLaserFreq-DoplerShift()-TransitionFrequency(j,k)));
	      y=o/y;

	      row[dal][gI(k,m)]+=(x+y)*threej*LaserIntens[dal]/HBAR/HBAR;
	    }
	}
    }
    //Otra, Tresa Summa
    for(Gst k(fG); k.notFinal(); ++k)
    {
	for(Est m(fE); m.notFinal(); ++m)
	{
	    
	    cdouble threej1=TransitionAmplitude(i,k,e)*TransitionAmplitude(k,m,e);
	    cdouble threej2=TransitionAmplitude(m,k,e)*TransitionAmplitude(k,j,e);
	    for(int dal=0; dal<nReg; dal++)
	    {
	      cdouble o(1,0);
	      cdouble x((Gamma+LaserWidth)/2,(cLaserFreq-DoplerShift()-TransitionFrequency(j,k)));
	      cdouble y((Gamma+LaserWidth)/2,-(cLaserFreq-DoplerShift()-TransitionFrequency(i,k)));
	      x=o/x;
	      y=o/y;

	      row[dal][gI(m,j)]-=x*threej1*LaserIntens[dal]/HBAR/HBAR;
	      row[dal][gI(i,m)]-=y*threej2*LaserIntens[dal]/HBAR/HBAR;
	    }
	}
    }

    int selfI=gI(i,j);
    for(int dal=0; dal<nReg; dal++)
    {

	row[dal][selfI]-=cdouble(0,TransitionFrequency(i,j));
// 	//Molekulu sadursmes
// 	row[dal][selfI]-=cdouble(ColProb,0);
	//Spontana emsija
	row[dal][selfI]-=cdouble(Gamma,0);

    }//End Dal
  }
  

  return row;

}
dbl OBEGenerator::DoplerShift()const
{
  return DoplerSpeed*cLaserFreq/LSPEED;
}

#define MH MBOHR/HBAR
dbl OBEGenerator::TransitionFrequency(const State & a, const State & b)const
{
  dbl tmp;
  if(a.isExited() && b.isExited())
  {
    tmp=freqExited[a.nF()]+a.mF()/2.0*gLande(a)*B*MH-(freqExited[b.nF()]+b.mF()/2.0*gLande(b)*B*MH);
  }
  else if (a.isExited() && !b.isExited())
  {
    tmp=cTransFreq + freqExited[a.nF()]+a.mF()/2.0*gLande(a)*B*MH-(freqGround[b.nF()]+b.mF()/2.0*gLande(b)*B*MH);
  }
  else if (!a.isExited() && b.isExited())
  {
    tmp=cTransFreq + freqExited[b.nF()]+b.mF()/2.0*gLande(b)*B*MH-(freqGround[a.nF()]+a.mF()/2.0*gLande(a)*B*MH);
  }
  else
  {
    tmp=freqGround[a.nF()]+a.mF()/2.0*gLande(a)*B*MH-(freqGround[b.nF()]+b.mF()/2.0*gLande(b)*B*MH);
  }

  return tmp;
}

cdouble OBEGenerator::TransitionAmplitude(const State & a,const State & b,const cvec & v)const
{
  if(a.isExited())
  {
    cdouble tmp(0,0);
    for(int q=-1;q<=1;q++)
    {
      tmp+=v[q+1]*pow(-1,q)*wigner3J(a.F(),2,b.F(),-a.mF(),-2*q,b.mF());
    }
    tmp*=pow(-1,(a.F()-a.mF()+Jp+I+b.F()+2+Jp-J)/2)*sqrt((a.F()+1)*(b.F()+1))*gsl_sf_coupling_6j(Jp,a.F(),I,b.F(),J,2)*RedMElem;
  
    return tmp;
  }
  else 
  {
    cdouble tmp(0,0);
    for(int q=-1;q<=1;q++)
    {
      tmp+=std::conj(v[q+1])*wigner3J(a.F(),2,b.F(),-a.mF(),2*q,b.mF());
    }
    tmp*=pow(-1,(a.F()-a.mF()+J+I+b.F()+2)/2)*sqrt((a.F()+1)*(b.F()+1))*gsl_sf_coupling_6j(J,a.F(),I,b.F(),Jp,2)*RedMElem;
    return tmp;
  }
}

dbl OBEGenerator::GammaSpont(const State & a, const State & b,const State & c, const State & d)const
{
dbl tmp=(Jp+1)*Gamma*
	    pow(-1,(a.F()-a.mF()+c.F()-c.mF()+2*Jp+2*I+b.F()+d.F()+4)/2)*
	    sqrt((a.F()+1)*(b.F()+1)*(c.F()+1)*(d.F()+1))*
	    gsl_sf_coupling_6j(J,a.F(),I,b.F(),Jp,2)*
	    gsl_sf_coupling_6j(Jp,c.F(),I,d.F(),J,2);
    dbl sum=0;
    for(int q=-1; q<2; q++)
    {
        sum+=pow(-1,q)*wigner3J(a.F(),2,b.F(),-a.mF(),2*q,b.mF())
             *wigner3J(c.F(),2,d.F(),-c.mF(),-2*q,d.mF());
    }

    return tmp*sum;
}

dbl OBEGenerator::gLande(const State & a)const
{
    if(a.F()==0)
      return 0;

    if(a.isExited())
    {
      return gJp*((a.F()*a.F()/4.0+a.F()/2.0)-(I*I/4.0+I/2.0)+(Jp*Jp/4.0+Jp/2.0))/a.F()/(a.F()/2.0+1)+
         gINuc*((a.F()*a.F()/4.0+a.F()/2.0)+(I*I/4.0+I/2.0)-(Jp*Jp/4.0+Jp/2.0))/a.F()/(a.F()/2.0+1);
    }
    else
    {
      return  gJ*((a.F()*a.F()/4.0+a.F()/2.0)-(I*I/4.0+I/2.0)+(Jp*Jp/4.0+Jp/2.0))/a.F()/(a.F()/2.0+1)+
         gINuc*((a.F()*a.F()/4.0+a.F()/2.0)+(I*I/4.0+I/2.0)-(Jp*Jp/4.0+Jp/2.0))/a.F()/(a.F()/2.0+1);
    }
}
dbl OBEGenerator::GetDoplerStdDev()const
{
   return sqrt(MOL_R*Temp/MolMass);
}

dbl OBEGenerator::GetSpeedProb(dbl speed)const
{
  return sqrt(MolMass/2/M_PI/MOL_R/Temp)*exp(-MolMass*speed*speed/2/MOL_R/Temp);
}

int OBEGenerator::gI(const State & a,const State & b) const
{
  int index=0;
  if(a.isExited())
  {
    int n1=a.nF(),n2=b.nF();
      
      index+=sizeG*sizeG;
    for(int i=0;i<n2;i++)
      index+=sizeE*(fE[i]+1);
    for(int i=-fE[n2];i<b.mF();i+=2)
      index+=sizeE;
    
    for(int i=0;i<n1;i++)
      index+=fE[i]+1;
    
    for(int i=-fE[n1];i<a.mF();i+=2)
      index++;
  }
  else 
  {
   int n1=a.nF(),n2=b.nF();
    for(int i=0;i<n2;i++)
      index+=sizeG*(fG[i]+1);
    for(int i=-fG[n2];i<b.mF();i+=2)
      index+=sizeG;
    
    for(int i=0;i<n1;i++)
      index+=fG[i]+1;
    
    for(int i=-fG[n1];i<a.mF();i+=2)
      index++;
  }
  return index;
}

//Set laser Polarization
void OBEGenerator::SetPolar(dbl x, dbl y, dbl z)
{
  e.push_back(cdouble(-1/sqrt(2)*x,1/sqrt(2)*y));
  e.push_back(cdouble(z,0));
  e.push_back(cdouble(1/sqrt(2)*x,1/sqrt(2)*y));
}
//Add fluorescence polarization which needs to be calculated
void OBEGenerator::SetFluro(dbl x,dbl y, dbl z)
{
  cvec tmp;
  tmp.push_back(cdouble(-1/sqrt(2)*x,1/sqrt(2)*y));
  tmp.push_back(cdouble(z,0));
  tmp.push_back(cdouble(1/sqrt(2)*x,1/sqrt(2)*y));
  fluro.push_back(tmp);
}

dbl OBEGenerator::wigner3J(int F1,int F2,int F3,int mf1,int mf2,int mf3)const
{
    return gsl_sf_coupling_3j(F1,F2,F3,mf1,mf2,mf3);
//  return w3J[(F1-minF)/2][(F3-minF)/2][(mf1+maxF)/2][(mf3+maxF)/2][(mf2/2+1)];
}

/*
void OBEGenerator::GenerateSymbols()
{
  minF= fG[0]>fE[0]? fE[0]: fG[0];
  maxF=fG[fG.size()-1]>fE[fE.size()-1]? fG[fG.size()-1]: fE[fE.size()-1];
  int FC=(maxF-minF)/2+1;

  w3J.resize(FC);
  for( auto & a: w3J)
  {
    a.resize(FC);
    for( auto & b:a)
    {
      b.resize(2*maxF+1);
      for(auto & c:b)
      {
	c.resize(2*maxF+1);
	for(auto & cc:c)
	  cc.resize(3);
      }
    }
  }
  
  for(int i=minF;i<=maxF;i+=2)
  {
    for(int j=minF;j<=maxF;j+=2)
    {
      for(int m1=-i;m1<=i;m1+=2)
      {
	for(int m2=-j;m2<=j;m2+=2)
	{
	  for(int q=-1;q<=1;q++)
	    w3J[(i-minF)/2][(j-minF)/2][(m1+maxF)/2][(m2+maxF)/2][q+1]=gsl_sf_coupling_3j(i,2,j,m1,2*q,m2);
	}
      }
    }
  }
}
*/

std::string OBEGenerator::PrintFluroVec(int i)
{
    std::stringstream ret;
  ret <<"[";
  for(std::size_t k=0;k<fluro.at(i).size();k++)
    {
      ret << fluro[i][k].real();
      if(fluro[i][k].imag()<0)
	ret << "-i" << -fluro[i][k].imag();
      else if(fluro[i][k].imag()>0)
	ret << "+i" << fluro[i][k].imag();
      if(k!=2)
	ret << ";";
    }
  ret << "]";
  return ret.str();  
}
