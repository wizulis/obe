#include "obenumeric.h"


vec< vec< dbl > > OBEnumeric::GenerateMatrixAndSolve()
{
 
  for(std::size_t i=0;i<std::size_t(nReg+1);i++)
  {
    if(i==matA.size())
      matA.push_back(std::unique_ptr<ARMAmatrix>(new ARMAmatrix));
    matA[i]->Reset();
  }

    
    for(Gst i(systemGenerator->GetFG());i.notFinal();++i)
    {
      for(Gst j(systemGenerator->GetFG());j.notFinal();++j)
      {
    vec<cvec> r;
	systemGenerator->CreateRow(i,j,laserIntens,r);
	
	for(int m=0;m<nReg+1;m++)
	{
	  matA[m]->AddRow(r[m]);
	}
      }
    }    
    for(Est i(systemGenerator->GetFE());i.notFinal();++i)
    {
      for(Est j(systemGenerator->GetFE());j.notFinal();++j)
      {
    vec<cvec> r;
	systemGenerator->CreateRow(i,j,laserIntens,r);
	
	for(int m=0;m<nReg+1;m++)
	{
	  matA[m]->AddRow(r[m]);
	}
      }
    }
    vec<vec<dbl>> ret;
    using arma::cx_vec;
    using arma::cx_mat;
    
    cx_vec rho(systemGenerator->GetSizeM());
    rho.zeros();
    
    for(Gst i(systemGenerator->GetFG());i.notFinal();++i)
      rho(systemGenerator->gI(i,i))=1.0/systemGenerator->GetSizeG();
    
    vec<cx_vec> RhoPrev;
    RhoPrev.push_back(rho);
    
    cx_vec rhoP(systemGenerator->GetSizeM());
    cx_mat I=arma::eye<cx_mat>(systemGenerator->GetSizeM(),systemGenerator->GetSizeM());
    cx_vec tmpVec(systemGenerator->GetSizeM());
    
    cvec tmpR(systemGenerator->GetSizeM());
    
    for(int i=0;i<nReg;i++)
    {
      dbl dt=LaserRadStep/systemGenerator->GetFlowSpeed();
      
      tmpVec.zeros();
      tmpVec=(I+dt*matA[0]->GetMatrix())*RhoPrev[0];
      
      for(int j=1;j<=i;j++)
      {
	tmpVec+=dt*matA[j]->GetMatrix()*RhoPrev[j];
      }
      
      rhoP= arma::inv(I-dt*matA[i+1]->GetMatrix())*tmpVec;
   
      RhoPrev.push_back(rhoP);
      
    
      for(std::size_t j=0;j<tmpR.size();j++)
	tmpR[j]=rhoP(j);
      
   
//        PrintRho(tmpR);
//        std::cout << "-----------------------" << std::endl;
      
      vec<dbl> fl = systemGenerator->CalculateFluorescence(tmpR);
      

      ret.push_back(fl);
      rho=rhoP;
      
//       getchar();
    }
//     std::cout << "DONE" <<std::endl;
    
    return ret;
}

void OBEnumeric::GenerateIntensityDistribution()
{
  laserIntens.clear();
  laserRad.clear();

  double sigma = FWHM/2.35482;
  
  double y=RadPoints[(nFull-nReg)/2];
 

  if(nFull==1)
  {
    RadP p;
    p.x1=FWHM/2;
    p.x2=-FWHM/2;
    p.vol=1;
    p.y1=FWHM/2;
    p.y2=FWHM/2;
    p.r=FWHM/4;
    
    laserRad.push_back(p);
    laserIntens.push_back((-2*PIntens*sigma*sigma*exp(-FWHM*FWHM/8/sigma/sigma)-(-2*PIntens*sigma*sigma))/pow(FWHM/2,2));
  }
  else
  {
    for(int i=0;i<nReg/2;i++)
    {
      RadP p;
      
      p.r=RadPoints[nFull/2-1-i];
      p.y1=y-LaserRadStep/2;
      p.y2=y+LaserRadStep/2;


      p.vol=GetVol(p.r+LaserRadStep/2,p.y1,p.y2);
     
      
      if(i+1!=nReg/2)
      {
	p.vol-=GetVol(p.r-LaserRadStep/2,p.y1,p.y2);
	if(p.vol!=p.vol)
	{
	  std::cout << p.r << "\t" << nReg << "\t" << p.y1 << "\t" << p.y2 << std::endl;
	}
	p.x1=sqrt(pow(p.r-LaserRadStep/2,2)-y*y);  
      }
      else
      {
	p.x1=0;
      }
      p.x2=sqrt(pow(p.r+LaserRadStep/2,2)-y*y);

      laserRad.push_back(p);
    }

    for(int i=nReg/2-1;i>=0;i--)
    {
      RadP p;
      
      p.r=RadPoints[nFull/2-1-i];
      p.y1=y-LaserRadStep/2;
      p.y2=y+LaserRadStep/2;
     
      p.x2=-sqrt(pow(p.r+LaserRadStep/2,2)-y*y);

           
      p.vol=GetVol(p.r+LaserRadStep/2,p.y1,p.y2);
      
      if(i+1!=nReg/2)
      {
	p.x1=-sqrt(pow(p.r-LaserRadStep/2,2)-y*y);      
	p.vol-=GetVol(p.r-LaserRadStep/2,p.y1,p.y2);
      }
      else
      {
	p.x1=0;
      }
      
      p.x2=sqrt(pow(p.r+LaserRadStep/2,2)-y*y);

      laserRad.push_back(p);
    }
    
    laserIntens.push_back(0);
    for(int i=0;i<nReg;i++)
    { 
      laserIntens.push_back(PIntens*exp(-pow(laserRad[i].r,2)/2/sigma/sigma));
    }
    
  }
}
   
