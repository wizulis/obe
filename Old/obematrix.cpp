#include "obematrix.h"
#include "umfpack.h"

void UMFPACKmatrix::AddRow(cvec & tmp)
{
  if(pC==Ap.size())
    Ap.push_back(count);
  else
    Ap[pC]=count;


  pC++;

  for(std::size_t i=0;i<tmp.size();i++)
  {
    if(tmp[i]!=cdouble(0,0))
    {
      if(A.size()==aC)
	A.push_back(tmp[i]);
      else
	A[aC]=tmp[i];
      aC++;
      if(Ai.size()==iC)
	Ai.push_back(i);
      else
	Ai[iC]=i;
      iC++;
      count++;
    }
  }

}

void UMFPACKmatrix::AddBVal(cdouble v)
{
  if(BVal.size()==bC)
    BVal.push_back(v);
  else
    BVal[bC]=v;
  bC++;
}

void UMFPACKmatrix::Solve(cvec & res)
{
  if(pC==Ap.size())
    Ap.push_back(count);
  else
    Ap[pC]=count;
  
  if(BVal.size()!=Ap.size()-1)
    {
    throw std::runtime_error("UMFPACK matrix size and B vector size are not equal");
    } 


  int status;  
  status= umfpack_zi_symbolic (BVal.size(), BVal.size(), &Ap[0], &Ai[0],(double*) &A[0],0, &Symbolic, 0,0) ;
  if(status!=UMFPACK_OK)
  {
    std::stringstream tmp;
    tmp << "umfpack_zi_symbolic failed with Error Code " << status;
    tmp << "\n" << Ap.size() << "\t" << Ai.size() << "\t" << BVal.size();
    throw std::runtime_error(tmp.str());
  } 
  
  status= umfpack_zi_numeric (&Ap[0], &Ai[0], (double*)&A[0],0, Symbolic, &Numeric,0, 0) ;
  if(status!=UMFPACK_OK)
  {
    std::stringstream tmp;
    tmp << "umfpack_zi_numeric failed with Error Code " << status;
    throw std::runtime_error(tmp.str());
  } 


  status = umfpack_zi_solve (UMFPACK_Aat, &Ap[0], &Ai[0], (double*)&A[0], 0,(double*)&(res[0]),0 ,(double*)&BVal[0],0, Numeric, 0, 0);
  if(status!=UMFPACK_OK)
  {
    std::stringstream tmp;
    tmp << "umfpack_zi_solve failed with Error Code " << status;
    throw std::runtime_error(tmp.str());
  } 
  umfpack_zi_free_symbolic(&Symbolic);
  umfpack_zi_free_numeric(&Numeric);
}




void ARMAmatrix::AddRow(cvec & r )
{
  if(A.n_rows!=r.size())
  {
    if(row!=0)
      throw std::runtime_error("ARMA MATRIX A-row size and r-row Size are not Equal");
    
    A.resize(r.size(),r.size());
    BVal.resize(r.size());
  }
  for (std::size_t i=0;i<r.size();i++)
     A(row,i)=r[i];
    

   row++;
}
    
 
void ARMAmatrix::AddBVal(cdouble val)
{
  BVal(bC)=val;
  bC++;
}

void ARMAmatrix::Solve(cvec & res)
{
  if(row!=bC)
    throw std::runtime_error("Added Row and B-val sizes does not match");
  if(A.n_rows!=res.size())
    throw std::runtime_error("Res size does not match A row size");
    
  int size=res.size();
  arma::cx_mat L, U, P;
  arma::cx_vec y(size);
  lu(L, U, P, A);
   
   
   BVal=P*BVal;
  
   for( int i=0;i<size;i++)
   {
    y(i)=BVal(i)/L(i,i);
    
    cdouble sum(0,0);
    
    for(int j=0;j<i;j++)
      sum+=L(i,j)*y(j);
   
    y(i)-=sum/L(i,i);
   }
   
   for(int i=size-1;i>=0;i--)
   {
      res[i]=y(i)/U(i,i);
      cdouble sum(0,0);
      for(int j=i+1;j<size;j++)
	sum+=U(i,j)*res[j];
      res[i]-=sum/U(i,i);
   }

  
}

