#include "obenonlinear.h"
#include <armadillo>
#include <boost/property_tree/xml_parser.hpp>
#include <boost/property_tree/ptree.hpp>
#include <gsl/gsl_sf_coupling.h>
#include <algorithm>
#include <iostream>
#include <fstream>

#include <iomanip>
#include <gsl/gsl_const_mksa.h>
#define ELECTRON_CHARGE GSL_CONST_MKSA_ELECTRON_CHARGE
#define BOHR_RADIUS GSL_CONST_MKSA_BOHR_RADIUS
#define HBAR GSL_CONST_MKSA_PLANCKS_CONSTANT_HBAR
#define HPLANK GSL_CONST_MKSA_PLANCKS_CONSTANT_H
#define LSPEED GSL_CONST_MKSA_SPEED_OF_LIGHT
#define MOL_R GSL_CONST_MKSA_MOLAR_GAS
#define MBOHR GSL_CONST_MKSA_BOHR_MAGNETON
bool comp(arma::vec a, arma::vec b)
{
  for(int op_=0;op_<a.n_rows;op_++)
  {
    if(abs((a(op_)-b(op_))/abs(a(op_)+b(op_)))>1e-10)
      return false;
  }
  return true;
}
	
using boost::property_tree::ptree;
OBEnonLinear::OBEnonLinear(std::string config, OBEmatrix* mm, int dal): OBEsystem(config, mm, dal)
{
  ptree pt;
  read_xml(config, pt);
  L=pt.get<int>("Config.L");
  Lp=pt.get<int>("Config.Lp");
  Ex=std::unique_ptr<EigenCalc>(new EigenCalc(fE,freqExited,Jp,I,Lp,gINuc,gJp));
  Gr=std::unique_ptr<EigenCalc>(new EigenCalc(fG,freqGround,J,I,L,gINuc,gJ));
  SetB(0);
}

void OBEnonLinear::test()
{
  std::ofstream out("Izvads.txt");
  cvector vec={0,1,0};
  for(double B=0;B<=5000;B+=10)
    {
      SetB(B);
      out << GetB();
      
      for(Gst g(fG);g.notFinal();++g)
	{
	  for(Est e(fE);e.notFinal();++e)
	    {
	      if(e.mF()==g.mF())
	      {
		out <<"\t"<< (J3S(g,e,vec)*J3S(e,g,vec)).real()/RedMElem/RedMElem;
	      }
	    }
	  //++g;
	}
      out << std::endl;
    }
  
}

double OBEnonLinear::TransitionFreq(State a, State b)
{
    if(a.isExited() && b.isExited())
    {
      return (Ex->GetEnergy(a.nF(),a.mF())-Gr->GetEnergy(b.nF(),b.mF()));
    }
    else if (a.isExited() && !b.isExited())
    {
      return (cTransFreq+Ex->GetEnergy(a.nF(),a.mF())-Gr->GetEnergy(b.nF(),b.mF()));
    }
    else if (!a.isExited() && b.isExited())
    {
      return (cTransFreq+Ex->GetEnergy(b.nF(),b.mF())-Gr->GetEnergy(a.nF(),a.mF()));
    }
    else 
    {
      return (Gr->GetEnergy(a.nF(),a.mF())-Gr->GetEnergy(b.nF(),b.mF()));
    }
}

void OBEnonLinear::CalculateEnergies()
{
  Ex->SetB(B);
  Gr->SetB(B);
}

cdouble OBEnonLinear::J3S(State a, State b, cvector v)
{
  cdouble ret(0,0);
  if(a.isExited())
  {
    for(Est c(fE);c.notFinal();++c)
    {
      for(Gst d(fG);d.notFinal();++d)
      {
	if(c.mF()!=a.mF() || d.mF()!=b.mF())
	  continue;
      double tmp=Ex->GetMixing(a.mF(),a.nF(),c.nF())*Gr->GetMixing(b.mF(),b.nF(),d.nF());  
      if(tmp!=0)
	ret+=tmp*OBEsystem::J3S(c, d, v);
      }
    }
  }
  else
  {
    for(Gst c(fG);c.notFinal();++c)
    {
      for(Est d(fE);d.notFinal();++d)
      {
	if(c.mF()!=a.mF() || d.mF()!=b.mF())
	  continue;
	double tmp=Gr->GetMixing(a.mF(),a.nF(),c.nF())*Ex->GetMixing(b.mF(),b.nF(),d.nF());  
	if(tmp!=0)
	ret+=tmp*OBEsystem::J3S(c, d, v);
      }
    }
  }
 return ret;
}

double OBEnonLinear::GammaSpont(State& a, State& b, State& c, State& d)
{
  double ret=0;
  for(Gst aa(fG,fG[0],a.mF());aa.nF()<aa.Fcount();aa.incF())
    {
      for(Est bb(fE,fE[0],b.mF());bb.nF() < bb.Fcount();bb.incF())
	{
	  for(Est cc(fE,fE[0],c.mF());cc.nF()<cc.Fcount();cc.incF())
	    {
	      for(Gst dd(fG,fG[0],d.mF());dd.nF() < dd.Fcount();dd.incF())
		{
		  double tmp=Gr->GetMixing(a.mF(),a.nF(),aa.nF())*
		    Ex->GetMixing(b.mF(),b.nF(),bb.nF())*
		    Ex->GetMixing(c.mF(),c.nF(),cc.nF())*
		    Gr->GetMixing(d.mF(),d.nF(),dd.nF());
		  if(tmp!=0)
		    ret+=tmp*OBEsystem::GammaSpont(aa, bb, cc, dd);
		}
	    }
	}
    }
  return ret;
}
