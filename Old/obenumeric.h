#ifndef OBENUMERIC_H
#define OBENUMERIC_H
#include "obesystem.h"
class OBEnumeric: public OBEsystem
{
  vec<std::unique_ptr<ARMAmatrix>> matA;
public: 
  OBEnumeric(std::shared_ptr< OBEGenerator > g):OBEsystem(g,OBEmatrix::MatTypes::ARMA)
  {}
  vec<vec<dbl>> GenerateMatrixAndSolve();
protected:
  void 	GenerateIntensityDistribution();
};

#endif // OBENUMERIC_H
