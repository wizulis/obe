#ifndef OBENONLINEAR_H
#define OBENONLINEAR_H
#include <memory>
#include "obesystem.h"
#include "eigencalc.h"


template<typename T>
struct OffsetContainer
{
  int off;
  int st;
  std::vector<T> data;
  
  OffsetContainer(int numElem,int offset,int step):off(offset),st(step)
  {
    data.resize(numElem);
  }
  
  OffsetContainer(){}
  
  void Setup(int numElem,int offset,int step)
  {
    off=offset;
    st=step; 
    if(data.size()!=0)
      data.clear();
    data.resize(numElem);
  }
  
  T &  operator()(int i)
  {
    if((i-off)/st<data.size() && data.size()!=0)
      return data[(i-off)/st];
    else 
      throw "Access out of Bounds";
  }
  
};

class OBEnonLinear : public OBEsystem
{
  
protected:
  std::unique_ptr<EigenCalc> Ex,Gr;
  double GammaSpont(State& a, State& b, State& c, State& d);
  
  cdouble J3S(State a,State b,cvector v);
  void CalculateEnergies();
  
  double TransitionFreq(State a,State b);
  int L;
  int Lp;
public:
  OBEnonLinear(std::string config, OBEmatrix* mm, int dal = 1);
  
  void test();
  void SetB(double t) {OBEsystem::SetB(t);CalculateEnergies();}
};

#endif // OBENONLINEAR_H
