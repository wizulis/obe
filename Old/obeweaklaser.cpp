#include "obeweaklaser.h"


#include <boost/property_tree/xml_parser.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/foreach.hpp>
#include <boost/lexical_cast.hpp>

OBEWeakLaser::OBEWeakLaser(const std::string &config){
    InitConfig(config);
}


void OBEWeakLaser::InitConfig(const std::string &config){
    if(ConfigName==config)
        return;

    using boost::property_tree::ptree;
    ConfigName=config;
    int FgT,FeT;
    dbl TrFr;

    std::ifstream file(config.c_str());

    if(file.is_open())
    {
      ptree pt2;
      read_xml(config, pt2);
      ptree pt=pt2.get_child("Config.SecondLaser");
      J=pt.get<int>("Config.J");
      Jp=pt.get<int>("Config.Jp");
      gJ=pt.get<dbl>("Config.gJ");
      gJp=pt.get<dbl>("Config.gJp");

      try //If I is not defined
      {
    I=pt.get<int>("Config.I");
    gINuc=pt.get<dbl>("Config.gI");

      }
      catch (...)
      {
    std::cout << "Setting Nuclear spin to 0" << std::endl;
    I=0;
    gINuc=0;
      }

      Gamma=pt.get<dbl>("Config.Gamma")*1e6*2*M_PI;
      cTransFreq=pt.get<dbl>("Config.TransitionFrequency")*1e12*2*M_PI;

      FgT=pt.get<int>("Config.Transition.Fg");
      FeT=pt.get<int>("Config.Transition.Fe");

      TrFr=pt.get<dbl>("Config.Transition.Detuning")*1e6*2*M_PI;
      LaserWidth=pt.get<dbl>("Config.LaserWidth")*1e6*2*M_PI;

      RedMElem=pt.get<dbl>("Config.ReducedMatrixElement")*ELECTRON_CHARGE*BOHR_RADIUS;

      FWHM=pt.get<dbl>("Config.FWHM");

      MolMass=pt.get<dbl>("Config.AtomMass")*1e-3;
      Temp=pt.get<dbl>("Config.Temperature");

      SetPolar(pt.get<dbl>("Config.LaserPolarization.x"),
           pt.get<dbl>("Config.LaserPolarization.y"),
           pt.get<dbl>("Config.LaserPolarization.z"));

      try
      {
    ColProb = pt.get<dbl>("Config.ColisionProbability")*1e6*2*M_PI;
      }
      catch (...)
      {
    std::cout << "Setting Colision Probability to 0" << std::endl;
    ColProb=0;
      }


      BOOST_FOREACH(ptree::value_type &v, pt.get_child("Config.Fluorescence"))
        SetFluro(v.second.get<dbl>("x"),
         v.second.get<dbl>("y"),
         v.second.get<dbl>("z"));



      BOOST_FOREACH(ptree::value_type &v, pt.get_child("Config.ExitedFreqShift"))
        freqExited.push_back(boost::lexical_cast<dbl>(v.second.data())*1e6*2*M_PI);

      BOOST_FOREACH(ptree::value_type &v, pt.get_child("Config.GroundFreqShift"))
        freqGround.push_back(boost::lexical_cast<dbl>(v.second.data())*1e6*2*M_PI);

      //Try to fill ground levels numbers from config file.
      try
    {
      BOOST_FOREACH(ptree::value_type &v, pt.get_child("Config.LevelsGround"))
        {
          fG.push_back(boost::lexical_cast<int>(v.second.data()));
        }
    }
      catch(...)
    {
         //If no levels were specified, calculate levels ourselfs.

    for(int i=abs(J-I); i<=abs(J+I); i+=2)
      fG.push_back(i);
    }
      if(freqGround.size()!=fG.size())
    throw std::logic_error("Level count does not equal frequency count for ground state");


      try{

      BOOST_FOREACH(ptree::value_type &v,pt.get_child("Config.LevelsExited"))
    {
      fE.push_back(boost::lexical_cast<int>(v.second.data()));
    }
      }
      catch(...)
    {


    for(int i=abs(Jp-I); i<=abs(Jp+I); i+=2)
      fE.push_back(i);
    }
      if(freqExited.size()!=fE.size())
    throw std::logic_error("Level count does not equal frequency count for exited state");

      sizeG=0;
      sizeE=0;

      for(std::size_t i=0; i<fG.size(); i++)
        sizeG+=fG[i]+1;
      for(std::size_t i=0; i<fE.size(); i++)
        sizeE+=fE[i]+1;
      sizeM=sizeG*sizeG+sizeE*sizeE;


    }
    else
    {
      std::cout << "Unable to Open file " << config << std::endl; throw -1;
    }

    DoplerSpeed=0;
    SetB(0);
    LaserIntens=1;
    FlowSpeed=sqrt(M_PI*MOL_R*Temp/MolMass/2);
    //GenerateSymbols();

    Gst g(fG,FgT);
    Est e(fE,FeT);
    cLaserFreq=std::abs(TransitionFrequency(g,e)+TrFr);

    Est i(fE);
    offset=gI(i,i);
}

void OBEWeakLaser::CreateRow(const Est &i, const Est &j, const dbl laserIntens, const cvec &rhoV, cvec &row,cdouble & bval)const {


    bval=cdouble(0,0);
    for(Gst k(fG); k.notFinal(); ++k){
        for(Gst m(fG); m.notFinal(); ++m){

            cdouble threej=TransitionAmplitude(i,k,e)*TransitionAmplitude(m,j,e);
            cdouble o(1,0);

            cdouble x((Gamma+LaserWidth)/2, (cLaserFreq-DoplerShift()-TransitionFrequency(m,i)));
            cdouble y((Gamma+LaserWidth)/2, -(cLaserFreq-DoplerShift()-TransitionFrequency(k,j)));
            x=o/x;
            y=o/y;
            x=x+y;
            //gI from standart implentation works here becouse the base level is the same
            bval-=x*threej*laserIntens/HBAR/HBAR*rhoV[gI(k,m)];

        }
    }//END First sum


    //Second and Third sum
    for(Gst k(fG); k.notFinal(); ++k){
        for(Est m(fE); m.notFinal(); ++m){

            cdouble threej1=TransitionAmplitude(i,k,e)*TransitionAmplitude(k,m,e);
            cdouble threej2=TransitionAmplitude(m,k,e)*TransitionAmplitude(k,j,e);

            cdouble o(1,0);
            cdouble x((Gamma+LaserWidth)/2, -(cLaserFreq-DoplerShift()-TransitionFrequency(k,j)));
            cdouble y((Gamma+LaserWidth)/2, (cLaserFreq-DoplerShift()-TransitionFrequency(k,i)));

            x=o/x;
            y=o/y;

            row[gInd(m,j)]-=x*threej1*laserIntens/HBAR/HBAR;
            row[gInd(i,m)]-=y*threej2*laserIntens/HBAR/HBAR;

        }
    }//End Second and Third sum

    row[gInd(i,j)]-=cdouble(0,TransitionFrequency(i,j));
    row[gInd(i,j)]-=Gamma;
}


vec<dbl> OBEWeakLaser::CalculateFluorescence(const vec<cdouble> &rhoV) const{


    vec<dbl> ret;
    ret.reserve(fluro.size());
    for(auto &vec: fluro)
    {
      cdouble sum(0,0);
      for(Est r(fE); r.notFinal(); ++r)
      {
          for(Est s(fE); s.notFinal(); ++s)
          {
            for(Gst m(fG); m.notFinal(); ++m)
            {
              sum+=TransitionAmplitude(m,r,vec)*TransitionAmplitude(s,m,vec)*rhoV[gInd(r,s)];
            }
          }
      }
      ret.push_back(sum.real()/RedMElem/RedMElem*Gamma);
    }
    return ret;

}

int OBEWeakLaser::gInd(const Est &a, const Est &b) const{
    return gI(a,b)-offset;
}
