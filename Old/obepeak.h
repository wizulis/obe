#ifndef OBEPEAK_H
#define OBEPEAK_H

#include "obesystem.h"



class OBEpeak: public OBEsystem
{
protected:
  dbl cpWidth;
  dbl cpMult;
public:
  
  OBEpeak(std::shared_ptr< OBEGenerator > , OBEmatrix::MatTypes matType);
  void InitConfig(std::string);
  
  void GenerateIntensityDistribution();
  dbl AvgIntensity();
  void SetN(int );
};


#endif