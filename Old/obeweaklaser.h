#ifndef OBEWEAKLASER_H_INCL
#define OBEWEAKLASER_H_INCL

#include "obegenerator.h"
#include <functional>
class OBEWeakLaser:public OBEGenerator{

    int offset;
   public:
    OBEWeakLaser(){}
    OBEWeakLaser(const std::string & config);
    virtual void InitConfig(const std::string & config) override;
    void CreateRow(const Est & i, const Est & j,const dbl laserIntens,const cvec & rhoV,cvec &row,cdouble & bval)const ;
    vec<dbl> CalculateFluorescence(const vec<cdouble> &rhoV) const override;
    virtual ~OBEWeakLaser(){}

    int gInd(const Est &,const Est&)const;
};


#endif
