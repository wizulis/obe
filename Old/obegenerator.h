#ifndef OBEGENERATOR_H
#define OBEGENERATOR_H
#include "indexClass.h"
#include <string>
#include <map>
#include <complex>
#include <vector>
#include <gsl/gsl_const_mksa.h>
#include "assigment.hpp"

namespace OBE {


    class OBEGenerator
    {
    public:
      OBEGenerator(){}
      OBEGenerator(const Assigment & asg);

      void 	SetLaserIntensity(dbl x){LaserIntens=x;}
      void 	SetFluro(dbl x,dbl y,dbl z);
      void 	SetPolar(dbl x,dbl y,dbl z);
      void 	SetB(dbl x){B=x*1e-4;}
      int  GetFluroCount(){return fluro.size();}
      dbl  GetB(){return B*1e4;}
      virtual void SetAssignment(const Assigment &asg){}
      virtual void 	SetDoplerSpeed(dbl val){DoplerSpeed=val;}
      dbl 	GetDoplerSpeed(dbl val)const {return DoplerSpeed;}
      dbl 	GetDoplerStdDev() const;
      dbl 	GetFlowSpeed() const;
      virtual int 	gI(const State &a,const State &b) const ;
      dbl 	GetSpeedProb(dbl speed) const ;

      int 	GetSizeM()const {return sizeM;}
      int 	GetSizeG()const {return sizeG;}
      int 	GetSizeE()const {return sizeE;}
      const vec<int> & GetFG()const {return fG;}
      const vec<int> & GetFE()const {return fE;}

      virtual vec<dbl> CalculateFluorescence(const cvec& rhoV) const ;

      std::string PrintFluroVec(int);

      virtual vec<cvec> CreateRow(const State & i, const State & j,const vec<dbl> & li,vec<cvec> &row);
    protected:

      std::string ConfigName;
      cvec e; 				// Laser Polarziation
      vec<cvec> fluro; 		//Fluorescence polarization

      dbl 	cLaserFreq;		//Laser Frequency
      dbl 	LaserWidth;		//FWHM of Laser spectral profile
      dbl 	LaserIntens;		// Laser beam intensity

      /*int maxF;
      int minF;
      */
      int sizeM,sizeG,sizeE;



     vec <int> fG,fE;
      vec <dbl> freqGround,freqExited;

      int 		J; 					// J*2
      int 		Jp; 					// Jp*2
      int 		I;					// 2*I

      dbl	gJ;
      dbl 	gJp;
      dbl	gINuc;

      dbl 	Gamma;					// parejas atrums

      dbl 	cTransFreq;				// centrala parejas frekvence
      dbl  	RedMElem; 				// |<J||d||J'>|
      dbl  	FWHM;					// FWHM Lazera staram (Lazera stara diametrs)
      dbl	B;


      dbl 	Temp;                                   // Temperatura
      dbl	MolMass;                                // Atommasa au vienibas
      dbl 	ColProb;				//Sadrusmju varbutiba;
      dbl 	DoplerSpeed;				//Doplera atrums
      dbl 	FlowSpeed;				//Plumsas starp apgabaliem atrums




      virtual dbl 		TransitionFrequency(const State &a,const State &b)const;
      virtual cdouble 	TransitionAmplitude(const State &a,const State & b, const cvec &v)const;
      virtual dbl 		GammaSpont(const State &a,const State &b,const State &c,const State &d)const;


      void 		GenerateSymbols();
      dbl 	DoplerShift()const;


      dbl 	wigner3J(int,int,int,int,int,int)const;
      dbl 	gLande(const State &a)const;
      dbl 	speedProb(dbl speed)const;


    };
}
#endif // OBEGENERATOR_H
