#include "obetwolaser.h"
#include "obematrix.h"




void OBETwoLaser::InitConfig(const std::string &config){

    OBEGenerator::InitConfig(config);
    weakProbe.InitConfig(config);

   // weakIntens=1.0;
}

void OBETwoLaser::SetDoplerSpeed(dbl val){
    OBEGenerator::SetDoplerSpeed(val);
    weakProbe.SetDoplerSpeed(val);
}

vec<dbl> OBETwoLaser::CalculateFluorescence(const cvec &rhoV) const{

    UMFPACKmatrix mat;

    cdouble bv;
    cvec row;
    for(Est i(weakProbe.GetFE());i.notFinal();++i){
        for(Est j(weakProbe.GetFE());j.notFinal();++j){
            row.clear();

            //It should call default constructor on each of elements which is 0,0;
            row.resize(pow(weakProbe.GetSizeE(),2));
            bv=cdouble(0,0);

            weakProbe.CreateRow(i,j,weakIntens,rhoV,row,bv);
            mat.AddRow(row);
            mat.AddBVal(bv);
        }
    }
    cvec rhoNew(pow(weakProbe.GetSizeE(),2));

    mat.Solve(rhoNew);

    return weakProbe.CalculateFluorescence(rhoNew);
}

 void OBETwoLaser::SetAssignment(std::shared_ptr<OBEassigment> &asg){

    weakProbe.SetB(GetB());
    weakIntens=pow(2*M_PI*asg->GetSecR()*1e6*HBAR/RedMElem,2);
}
