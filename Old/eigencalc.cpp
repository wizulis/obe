#include "eigencalc.h"
#include <armadillo>
#include <gsl/gsl_sf_coupling.h>
#include <gsl/gsl_const.h>
#include <cassert>
#include <boost/concept_check.hpp>

#define HBAR GSL_CONST_MKSA_PLANCKS_CONSTANT_HBAR
#define MBOHR GSL_CONST_MKSA_BOHR_MAGNETON
EigenCalc::EigenCalc(std::vector< int > Fvec,std::vector<double> fr,int Jval,int Ival,int Lval, double gIval, double gJval):
Fv(Fvec),freq(fr),
J(Jval),I(Ival),L(Lval),gI(gIval),gJ(gJval)
{

  B=0;
}

void EigenCalc::SetB(double val)
{
  B=val;
  Calculate();
}

double EigenCalc::GetB()
{
  return B;
}

void EigenCalc::Calculate()
{
  assert(Fv.size()!=0);
 
  assert(Fv.size()==freq.size());
  
  EnergyValues.clear();
  EnergyValues.resize(Fv[Fv.size()-1]+1);
  MixingValues.clear();
  MixingValues.resize(Fv[Fv.size()-1]+1);
  
  
  int EInd=0;
  for(int mF=-Fv[Fv.size()-1];mF<=Fv[Fv.size()-1];mF+=2)
  {/*
    std::cout << "<--->" << std::endl << mF << std::endl << "----" << std::endl;*/
    std::vector<int> Ftmp;
    
    for(int i=0;i<Fv.size();i++)
      if(Fv[i]>=std::abs(mF))
      	Ftmp.push_back(i);
      
    arma::mat HMat(Ftmp.size(),Ftmp.size());
    HMat.zeros();
    for(int i=0;i<Ftmp.size();i++)
    {
      HMat(i,i)=freq[Ftmp[i]];
    }
    
    for(int i=0;i<Ftmp.size();i++)
    {
      for(int j=0;j<Ftmp.size();j++)
      {
	HMat(i,j)+=MBOHR/HBAR*B*sqrt((Fv[Ftmp[i]]+1)*(Fv[Ftmp[j]]+1))
		    *pow(-1,(J+I+Fv[Ftmp[i]]+Fv[Ftmp[j]]-mF+2)/2)
		    *gsl_sf_coupling_3j(Fv[Ftmp[i]],2,Fv[Ftmp[j]],-mF,0,mF)
		    *(
		      gJ*sqrt(J/2.0*(J/2.0+1)*(J+1))
		      *gsl_sf_coupling_6j(J,Fv[Ftmp[i]],I,Fv[Ftmp[j]],J,2)
		      
		      +gI*sqrt(I/2.0*(I/2.0+1)*(I+1))
		      *gsl_sf_coupling_6j(I,Fv[Ftmp[i]],L,Fv[Ftmp[j]],I,2)
		    );
      }
    }

    arma::vec eigE;
    arma::mat eigVec;
    if(!arma::eig_sym(eigE,eigVec,HMat))
    {
      throw std::runtime_error("Error, calculating eigenvalues for one of mF");
    }
    std::vector<double> tmpEnergies;
    
    for(int i=0;i<Ftmp.size();i++)
      tmpEnergies.push_back(eigE(i));
    
    std::sort(tmpEnergies.begin(),tmpEnergies.end());
    
    EnergyValues[EInd].resize(Fv.size());
    MixingValues[EInd].resize(Fv.size());
    
    for(int i=0;i<Fv.size();i++)
    {
      EnergyValues[EInd][i]=0;
      MixingValues[EInd][i].resize(Fv.size());
      for(int j=0;j<Fv.size();j++)
      {
	MixingValues[EInd][i][j]=0;
      }
      
    }
    
    for(int i=0;i<Ftmp.size();i++)
      EnergyValues[EInd][Ftmp[i]]=tmpEnergies[i];
    
    arma::mat tmpMat = HMat*eigVec;
    
    for(int i=0;i<Ftmp.size();i++)
    {
      bool found = false;
      std::vector<double> eV; 
      for(int j=0;j<Ftmp.size();j++)
      {
	for(int m=0;m<Ftmp.size();m++)
	{
	  if(std::abs(tmpMat.col(j)(m)/tmpEnergies[i]-eigVec.col(j)(m))>1e-10)
	    break;
	  if(m+1==Ftmp.size())
	  {

	    found=true;
	  }
	}
	if(found)
	{
	  for(int m=0;m<Ftmp.size();m++)
	    eV.push_back(eigVec.col(j)(m));
	  break;
	}
      }
      assert(eV.size()==Ftmp.size());
      
      for(int j=0;j<Ftmp.size();j++)
	MixingValues[EInd][Ftmp[i]][Ftmp[j]]=eV[j];
    }

    EInd++;
  }
}
double EigenCalc::GetEnergy(int nF, int mF)
{
  return EnergyValues[(mF+Fv[Fv.size()-1])/2][nF];
}
double EigenCalc::GetMixing(int mF, int Fan, int Fbn)
{

  return MixingValues[(mF+Fv[Fv.size()-1])/2][Fan][Fbn];
}


