#ifndef OBECALC_H
#define OBECALC_H
#include <functional>
#include <memory>
#include <thread>
#include <vector>
#include <atomic>
#include <algorithm>
#include <array>
#include <iostream>

#include "assigment.hpp"

template<class sys>
class OBEcalc
{
  
public:
  OBEcalc(std::vector<std::shared_ptr<OBEassigment>> asig,
          std::function<std::shared_ptr<sys>(void)> CalcF,
          int thrC=1):
  asVec(asig)
  {
    oFactory=CalcF;

    objs.resize(isRunning.size());
    threadV.resize(isRunning.size());
    
    for(int i=0;i<isRunning.size();i++)
    {
      isRunning[i]=false;
      objs[i]=oFactory();
      threadV[i]=std::thread();
    }
    tC=thrC;
    ToCheck.store(true);
    asCheck=std::thread(&OBEcalc::CheckStartThread,this);
  }
  
  void CheckStartThread()
  {
      while(asVec.size()!=0)
      {

          if(!ToCheck.load())
            {
 	      for(auto i:runV)
 	      {
		if(i->IsCompleted())
 		{
 		  complV.push_back(i);
		  i->SaveResult();
 		}
 	      }
// 	      runV.erase(std::remove_if(runV.begin(),
//                     runV.end(),
//                     [complV](std::shared_ptr<OBEassigment> a){return complV.find(a)!=complV.end();}),
//              runV.end());
            std::this_thread::sleep_for(std::chrono::milliseconds(500));
            continue;
            }
        ToCheck.store(false);
        int tCn=tC;
        int run=0;
        for(int i=0;i<isRunning.size();i++)
            if(isRunning[i])
                run++;

        if(run<tC)
        {
            while(run<tCn && asVec.size()>0)
            {
                int i;
	  
                //Get ID of not running instance
                for(i=0;i<isRunning.size();i++)
                    if(!isRunning[i])
                        break;
	    
                //Start thread with assigment
                //std::cout << "Starting thread " << i << "\t" << asVec.size() << std::endl;

                if(threadV[i].joinable())
                    threadV[i].join();
                threadV[i]=std::thread([](
                    std::shared_ptr<sys> ob,std::shared_ptr<OBEassigment> as,bool & isR,std::atomic<bool> &tuC)
                    {
                        ob->CalculateAssigment(as);
                        isR=false;
                        tuC.store(true);
                    },objs[i],asVec[asVec.size()-1],std::ref(isRunning[i]),std::ref(ToCheck));
                isRunning[i]=true;
                //Move running assiment to runV
                runV.push_back(asVec[asVec.size()-1]);
                asVec.pop_back();
                run++;
            }
        }

      
        }
      for(std::thread & x: threadV)
          if(x.joinable())
            x.join();
      for(auto i:runV)
      {
        if(i->IsCompleted())
        {
              complV.push_back(i);
        }
      }
      
      runV.erase(std::remove_if(runV.begin(),
                    runV.end(),
                    [](std::shared_ptr<OBEassigment> a){return a->IsCompleted();}),
             runV.end());
      std::cout << "Completed " << complV.size() << " assigments" << std::endl;
      
      for(auto k: complV)
      {

		if(k->IsMaster())
	  if(!k->SaveResult())
	    std::cout << "Failed To save result!!!!!!" << std::endl;
	
      }
	
    }
  void WaitEnd()
  {
      asCheck.join();
  }
  int SetThreadCount(int i)
  {
    if(i<0)
      return;
    if(i>isRunning.size())
      tC=isRunning.size();
    else
      tC=i;
    return tC;
  }
  
private:
  std::atomic<bool> ToCheck;
  std::thread asCheck;
  int tC;
  std::function<std::shared_ptr<sys>(void)> oFactory;
  std::vector<std::shared_ptr<OBEassigment>> asVec;
  std::vector<std::shared_ptr<OBEassigment>> runV;
  std::vector<std::shared_ptr<OBEassigment>> complV;
  std::array<bool,16> isRunning;//Hardcoded limit - change here!
  std::vector<std::shared_ptr<sys>> objs;
  std::vector<std::thread> threadV;
  
};

std::vector<std::shared_ptr<OBEassigment>> SplitDoplerIntegral(std::shared_ptr<OBEassigment> asg,int thrC)
{
    std::vector<std::shared_ptr<OBEassigment>> ret(thrC,std::shared_ptr<OBEassigment>(new OBEassigment(*asg)));
    
    dbl f=asg->GetDF();
    dbl t=asg->GetDT();
    
//     std::cout<< "Splitting " << f << "\t" << t;

    dbl st=(t-f)/thrC;
    int steps=asg->GetDS()/thrC;

    for(int i=0;i<thrC;i++)
    {
//       std::cout << "\t" << f+i*st <<"\t" << f+(i+1)*st << "\t" << steps;
        ret[i]->SetDopplerP(f+i*st,f+(i+1)*st,steps);
    }
//     std::cout << "\n";
    for(int i=1;i<thrC;i++)
      ret[0]->AddLink(ret[i]);
    
    return ret;
}

#endif // OBECALC_H
