#ifndef OBETWOLASER_H_INCL
#define OBETWOLASER_H_INCL

#include "obegenerator.h"
#include "obeweaklaser.h"

class OBETwoLaser: public OBEGenerator{

public:
    OBETwoLaser(){}

    virtual void InitConfig(const std::string & config) override;

    virtual vec<dbl> CalculateFluorescence(const cvec &rhoV) const override;
    virtual void SetAssignment(std::shared_ptr<OBEassigment> &);
    virtual void SetDoplerSpeed(dbl val);
private:
    OBEWeakLaser weakProbe;
    dbl weakIntens;
};


#endif
