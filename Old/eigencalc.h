#ifndef EIGENCALC_H
#define EIGENCALC_H
#include <vector>

class EigenCalc
{ 
  std::vector<int> Fv;
  int J;
  int I;
  int L;
  double gI;
  double gJ;
  double B;
  std::vector<std::vector<std::vector<double> > > MixingValues;
  std::vector<std::vector<double> > EnergyValues;
  std::vector<double> freq;
  void Calculate();
public:
  EigenCalc(std::vector<int> Fvec,std::vector<double> fr,int Jval,int Ival,int Lval,double gIval,double gJval);
  void SetB(double val);
  double GetB();

  double GetEnergy(int F,int mF);
  double GetMixing(int mF,int Fa,int Fb);
};

#endif // EIGENCALC_H
