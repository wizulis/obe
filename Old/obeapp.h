#ifndef OBEAPP_H
#define OBEAPP_H

#include "obesystem.h"
#include "obenonlinear.h"
#include <mutex>
#include <vector>

#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>

enum SolverType {UMF,ARM};
enum SystemType {LINEAR,NONLINEAR};

struct Assigment
{
  Assigment(){}
  Assigment(int num,double Bval,double R,std::string outF):
  N(num),B(Bval),Rabi(R),OutputFile(outF){}
  int N;
  double B;
  double Rabi;
  std::string OutputFile;
};


class OBEapp
{
public:
  OBEapp();
  
  void CreateSocket(int p);
  void AddAssigment(Assigment s);
  void Calculate(int threads, std::string Config,SystemType sysT,SolverType solv);
  void SingleAssigmentCalculate(int threads,std::string Config,SystemType sysT,SolverType solv);
 
private:
    void ListenToCommands();
    void Calc(OBEsystem * sy,std::string outFile);
    std::vector<std::map<double,double>> Calc2(OBEsystem * sy,std::pair<int,int> val);
    int port_;
    bool isRunning;
    std::mutex assiMut;
    std::mutex resultMut;
    std::mutex fileMut;
    std::vector<Assigment> assigments;
    std::vector<OBEsystem*> sys;
    std::vector<OBEmatrix*> matr;
    std::vector<std::vector<double>> Results;
};


namespace boost {
namespace serialization {

template<class Archive>
void serialize(Archive & ar, Assigment & g, const unsigned int version)
{
    ar & g.N;
    ar & g.B;
    ar & g.Rabi;
    ar & g.OutputFile;
}
} // namespace serialization
} // namespace boost
#endif // OBEAPP_H
