/*
 wraperis matricas glabasanai un atrisinasanai utt.
 Nepieciesams, lai varetu vienkarsak mainit izmantojamo datu pierakstu un atrisinasanas metodi.
 Ta ka matrica tiek aizpildita pievienojot kolonnas, var izmantot ari UMFPACK algoritmiem.
 */
#ifndef OBEMATRIX_H
#define OBEMATRIX_H
#include <vector>
#include <complex>
#include <armadillo>
#include "obegenerator.h"

class OBEmatrix
{
  
  int size;

public:
   enum class MatTypes {UMF,ARMA}; 
  virtual void AddRow(cvec &)=0;
  virtual void AddBVal(cdouble )=0;
  virtual void Solve(cvec &)=0;
  virtual void Reset()=0;
  virtual MatTypes GetType()=0;
  virtual ~OBEmatrix(){};
};

class UMFPACKmatrix: public OBEmatrix
{
  cvec BVal;
  //Sistemas pieraksts UMFPACK
  cvec  A;
  vec<int>  Ai;
  vec<int>  Ap;
  std::size_t iC,pC,aC,bC;

  std::size_t count;
  void *Numeric, *Symbolic;
  
public:
  
  UMFPACKmatrix()
  {
    Reset();
  }
  MatTypes GetType(){return MatTypes::UMF;}
  void AddRow(cvec &);
  void AddBVal(cdouble);
  void Solve(cvec &);
  void Reset()
  {
    count=0;
    iC=0;
    pC=0;
    aC=0;
    bC=0;
    A.clear();
    Ai.clear();
    Ap.clear();
  }
 
};



class ARMAmatrix: public OBEmatrix
{
public:
  arma::cx_mat A;
  int row;
  int bC;
  arma::cx_vec BVal;
public:
  arma::cx_mat & GetMatrix(){return A;}
  ARMAmatrix()
  {
  row=0;
  bC=0;
  }
  MatTypes GetType(){return MatTypes::ARMA;}
  void AddRow(cvec &);
  void AddBVal(cdouble);
  void Solve(cvec &);
  void Reset(){A.zeros();row=0;bC=0;}
};
#endif // OBEMATRIX_H
