#include "obepeak.h"
#include <boost/property_tree/xml_parser.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/foreach.hpp>
#include <boost/lexical_cast.hpp>
void OBEpeak::GenerateIntensityDistribution()
{
  laserIntens.clear();
  laserRad.clear();

  double sigma = FWHM/2.35482;
  
  double y=0;
  if(nReg==1)
  {
    RadP p;
    p.r=RadPoints[0];
    p.y1=0;
    p.y2=0;
    p.x1=0;
    p.x2=0;
    p.vol=1;
    laserRad.push_back(p);
  }
  for(int i=0;i<nReg/2;i++)
    {
      RadP p;
      
      p.r=RadPoints[nFull/2-1-i];
      p.y1=y-LaserRadStep/2;
      p.y2=y+LaserRadStep/2;


      p.vol=GetVol(p.r+LaserRadStep/2,p.y1,p.y2);
     
      
      if(i+1!=nReg/2)
      {
	p.vol-=GetVol(p.r-LaserRadStep/2,p.y1,p.y2);
	if(p.vol!=p.vol)
	{
	  std::cout << p.r << "\t" << nReg << "\t" << p.y1 << "\t" << p.y2 << std::endl;
	}
	p.x1=sqrt(pow(p.r-LaserRadStep/2,2)-y*y);  
      }
      else
      {
	p.x1=0;
      }
      p.x2=sqrt(pow(p.r+LaserRadStep/2,2)-y*y);

      laserRad.push_back(p);
    }

    for(int i=nReg/2-1;i>=0;i--)
    {
      RadP p;
      
      p.r=RadPoints[nFull/2-1-i];
      p.y1=y-LaserRadStep/2;
      p.y2=y+LaserRadStep/2;
     
      p.x2=-sqrt(pow(p.r+LaserRadStep/2,2)-y*y);

           
      p.vol=GetVol(p.r+LaserRadStep/2,p.y1,p.y2);
      
      if(i+1!=nReg/2)
      {
	p.x1=-sqrt(pow(p.r-LaserRadStep/2,2)-y*y);      
	p.vol-=GetVol(p.r-LaserRadStep/2,p.y1,p.y2);
      }
      else
      {
	p.x1=0;
      }
      
      p.x2=sqrt(pow(p.r+LaserRadStep/2,2)-y*y);

      laserRad.push_back(p);
    }
//     std::cout << "NReg " << nReg << ";" << std::endl;	
    assert(laserRad.size()==nReg);
    
    for(int i=0;i<nReg;i++)
    { 
      if(std::abs(laserRad[i].r) >cpWidth) 
	laserIntens.push_back(PIntens);
      else
	laserIntens.push_back(PIntens*cpMult);
      
     // std::cout << sigma << "\t" <<  PIntens << "\t" << laserRad[i].r << "\t" << laserIntens[i] << "\t" << RadPoints[i] << std::endl;
    }
    
  
}

void OBEpeak::SetN(int nf)
{
  if(nf==0)
   throw std::runtime_error("Error nf = 0 !!");
  nFull=nf;
  if(nFull%2!=0 && nFull!=1)
    {
      std::cout << "Nepieciesams para skaits dalijumu\n";
      throw std::runtime_error("Error nFull");
    }

  nReg=nFull;
  
  LaserRadStep=MaxRad*FWHM/nFull;
  RadPoints.clear();
  if(nFull>=2)
    for(int i=0;i<nFull/2;i++)
      RadPoints.push_back(LaserRadStep*(i+0.5));
  else
    RadPoints.push_back(FWHM/2);
  SetLocalN(nFull);
}



OBEpeak::OBEpeak(std::shared_ptr< OBEGenerator > gen, OBEmatrix::MatTypes matType): OBEsystem(gen, matType)
{
  cpWidth=1;
  cpMult=1;
}

void OBEpeak::InitConfig(std::string conf)
{
  OBEsystem::InitConfig(conf);
  boost::property_tree::ptree pt;
  boost::property_tree::read_xml(conf, pt);
  
  cpWidth=pt.get<dbl>("Config.CentralPeakWidth");
  cpMult=pt.get<dbl>("Config.CentralPeakMult");
  GenerateIntensityDistribution();
}
dbl OBEpeak::AvgIntensity()
{
  return PIntens;
}
