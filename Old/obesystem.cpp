#include <math.h>
#include <iostream>
#include <fstream>

#include <gsl/gsl_sf_coupling.h>
#include <gsl/gsl_integration.h>
#include <gsl/gsl_const_mksa.h>

#include <boost/property_tree/xml_parser.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/foreach.hpp>
#include <boost/lexical_cast.hpp>
#include <chrono>

#define ELECTRON_CHARGE GSL_CONST_MKSA_ELECTRON_CHARGE
#define BOHR_RADIUS GSL_CONST_MKSA_BOHR_RADIUS
#define HBAR GSL_CONST_MKSA_PLANCKS_CONSTANT_HBAR
#define HPLANK GSL_CONST_MKSA_PLANCKS_CONSTANT_H
#define LSPEED GSL_CONST_MKSA_SPEED_OF_LIGHT
#define MOL_R GSL_CONST_MKSA_MOLAR_GAS
#define MBOHR GSL_CONST_MKSA_BOHR_MAGNETON

#include "obesystem.h"

//#define NDEBUG

using boost::property_tree::ptree;

 
   
void OBEsystem::SetLocalN(int n)
{
  nReg=n;
 
  GenerateIntensityDistribution();
}

double GetVol(double R, double y1,double y2)
{
  double tmp1=R*R-y2*y2;
  if(tmp1<0)
    tmp1=0;
  
  double tmp2=R*R-y1*y1;
  if(tmp2<0)
    tmp2=0;
  
  double tmp3=y2/R;
  if(tmp3>1)
    tmp3=1;
  if(tmp3<-1)
    tmp3=-1;
  
  double tmp4=y1/R;
  if(tmp4>1)
    tmp4=1;
  if(tmp4<-1)
    tmp4=-1;
  
  return std::abs(y2*sqrt(tmp1)+R*R*asin(tmp3)-y1*sqrt(tmp2)-R*R*asin(tmp4));
}


//Generate laser beam profile regions and intensities in each region
   
void OBEsystem::GenerateIntensityDistribution()
{
  laserIntens.clear();
  laserRad.clear();

  double sigma = FWHM/2.35482;
  
  double y=RadPoints[(nFull-nReg)/2];
 

  if(nFull==1)
  {
    RadP p;
    p.x1=FWHM/2;
    p.x2=-FWHM/2;
    p.vol=1;
    p.y1=FWHM/2;
    p.y2=FWHM/2;
    p.r=FWHM/4;
    
    laserRad.push_back(p);
    laserIntens.push_back((-2*PIntens*sigma*sigma*exp(-FWHM*FWHM/8/sigma/sigma)-(-2*PIntens*sigma*sigma))/pow(FWHM/2,2));
  }
  else
  {
    for(int i=0;i<nReg/2;i++)
    {
      RadP p;
      
      p.r=RadPoints[nFull/2-1-i];
      p.y1=y-LaserRadStep/2;
      p.y2=y+LaserRadStep/2;


      p.vol=GetVol(p.r+LaserRadStep/2,p.y1,p.y2);
     
      
      if(i+1!=nReg/2)
      {
	p.vol-=GetVol(p.r-LaserRadStep/2,p.y1,p.y2);
	if(p.vol!=p.vol)
	{
	  std::cout << p.r << "\t" << nReg << "\t" << p.y1 << "\t" << p.y2 << std::endl;
	}
	p.x1=sqrt(pow(p.r-LaserRadStep/2,2)-y*y);  
      }
      else
      {
	p.x1=0;
      }
      p.x2=sqrt(pow(p.r+LaserRadStep/2,2)-y*y);

      laserRad.push_back(p);
    }

    for(int i=nReg/2-1;i>=0;i--)
    {
      RadP p;
      
      p.r=RadPoints[nFull/2-1-i];
      p.y1=y-LaserRadStep/2;
      p.y2=y+LaserRadStep/2;
     
      p.x2=-sqrt(pow(p.r+LaserRadStep/2,2)-y*y);

           
      p.vol=GetVol(p.r+LaserRadStep/2,p.y1,p.y2);
      
      if(i+1!=nReg/2)
      {
	p.x1=-sqrt(pow(p.r-LaserRadStep/2,2)-y*y);      
	p.vol-=GetVol(p.r-LaserRadStep/2,p.y1,p.y2);
      }
      else
      {
	p.x1=0;
      }
      
      p.x2=sqrt(pow(p.r+LaserRadStep/2,2)-y*y);

      laserRad.push_back(p);
    }
    
    assert(laserRad.size()==(std::size_t)nReg);
    
    for(int i=0;i<nReg;i++)
    { 
      laserIntens.push_back(PIntens*exp(-pow(laserRad[i].r,2)/2/sigma/sigma));

     // std::cout << sigma << "\t" <<  PIntens << "\t" << laserRad[i].r << "\t" << laserIntens[i] << "\t" << RadPoints[i] << std::endl;
    }
    
  }
}
   
OBEsystem::OBEsystem(std::shared_ptr<OBEGenerator> generP,OBEmatrix::MatTypes matType):systemGenerator(generP),matT(matType)
{
  nFull=1;
  nReg=1;
  MaxRad=3;
  PIntens=1;
  FWHM=1;
  RadPoints.push_back(1);
  SetLocalN(nFull);
}
   
void OBEsystem::InitConfig(std::string config)
{
  systemGenerator->InitConfig(config);
  if(ConfigName==config)
      return;
  ConfigName=config;
 
  boost::property_tree::ptree pt;
  boost::property_tree::read_xml(config, pt);
  
  
  RedMElem=pt.get<dbl>("Config.ReducedMatrixElement")*ELECTRON_CHARGE*BOHR_RADIUS;

  FWHM=pt.get<dbl>("Config.FWHM");

  TRC=pt.get<dbl>("Config.TransRateCoef");
  MaxRad=pt.get<dbl>("Config.MaxRadiuss");
  PIntens=1;
  FlowSpeed=systemGenerator->GetFlowSpeed();
  SetN(nFull);
  GenerateIntensityDistribution(); 
  SetRabiFreq(1);
  
}


void OBEsystem::SetN(int nf)
{
  if(nf==0)
   throw std::runtime_error("Error nf = 0 !!");
  nFull=nf;
  if(nFull%2!=0 && nFull!=1)
    {
      std::cout << "Nepieciesams para skaits dalijumu\n";
      throw std::runtime_error("Error number of 'splits' needs to be even");
    }

  nReg=nFull;
  
  LaserRadStep=MaxRad*FWHM/2.35482*2.0/nFull;
  RadPoints.clear();
  if(nFull>=2)
    for(int i=0;i<nFull/2;i++)
      RadPoints.push_back(LaserRadStep*(i+0.5));
  else
    RadPoints.push_back(FWHM/2);
  SetLocalN(nFull);
}

vec<vec<dbl>> OBEsystem::GenerateMatrixAndSolve()
{
  //std::cout << "Starting Gen Mat and solve" << std::endl;
  for(std::size_t i=0;i<(std::size_t)nReg;i++)
    {
      if(i==mat.size())
	{
	  if(matT==OBEmatrix::MatTypes::ARMA)
	    {
	      mat.push_back(std::unique_ptr<OBEmatrix>(new ARMAmatrix));
	    }
	  else
	    {
	      mat.push_back(std::unique_ptr<OBEmatrix>(new UMFPACKmatrix));
	    }
	}
      else
	{
	  mat[i]->Reset();
	  if(matT!=mat[i]->GetType())
	    {
	      if(matT==OBEmatrix::MatTypes::ARMA)
		{
		  mat.push_back(std::unique_ptr<OBEmatrix>(new ARMAmatrix));
		}
	      else
		{
		  mat.push_back(std::unique_ptr<OBEmatrix>(new UMFPACKmatrix));
		}
	    }
	}
    }
    
    for(Gst i(systemGenerator->GetFG());i.notFinal();++i)
    {
      for(Gst j(systemGenerator->GetFG());j.notFinal();++j)
      {
        vec<cvec> r;
        systemGenerator->CreateRow(i,j,laserIntens,r);
	
        for(int m=0;m<nReg;m++)
        {
          //dbl tmpV=systemGenerator->GetFlowSpeed()/std::abs(laserRad[m].x1-laserRad[m].x2)*TRC;
          dbl tmpV=systemGenerator->GetFlowSpeed()/std::abs(LaserRadStep)*TRC;
          r[m][systemGenerator->gI(i,j)]-=cdouble(tmpV,0);

          mat[m]->AddRow(r[m]);

          if(m==0)
          {
            if(i==j)
              mat[0]->AddBVal(cdouble(-tmpV/systemGenerator->GetSizeG(),0));
            else
              mat[0]->AddBVal(cdouble(0,0));
	  } 
	}
      }
    }    
    for(Est i(systemGenerator->GetFE());i.notFinal();++i)
    {
      for(Est j(systemGenerator->GetFE());j.notFinal();++j)
      {
        vec<cvec> r;
        systemGenerator->CreateRow(i,j,laserIntens,r);

        for(int m=0;m<nReg;m++)
        {
         // dbl tmpV=systemGenerator->GetFlowSpeed()/std::abs(laserRad[m].x1-laserRad[m].x2)*TRC;
          dbl tmpV=systemGenerator->GetFlowSpeed()/std::abs(LaserRadStep)*TRC;
          r[m][systemGenerator->gI(i,j)]-=cdouble(tmpV,0);

          mat[m]->AddRow(r[m]);
          if(m==0)
          {
            mat[0]->AddBVal(cdouble(0,0));
          }
        }
      }
    }
    

    vec<vec<dbl>> ret;
    
    
    cvec rhoT(systemGenerator->GetSizeM());
    
    mat[0]->Solve(rhoT);
    
//     PrintRho(rhoT);
//     getchar();
    
    vec<dbl> fl = systemGenerator->CalculateFluorescence(rhoT);
    ret.push_back(fl);
    
    for(int i=1;i<nReg;i++)
    {
      //dbl tmpV=systemGenerator->GetFlowSpeed()/std::abs(laserRad[i].x1-laserRad[i].x2)*TRC;
      dbl tmpV=systemGenerator->GetFlowSpeed()/std::abs(LaserRadStep)*TRC;
      for(int j=0;j<systemGenerator->GetSizeM();j++)
      {
	mat[i]->AddBVal(-tmpV*rhoT[j]);
      }
      mat[i]->Solve(rhoT);
      
//       PrintRho(rhoT);
//       getchar();
    
      fl = systemGenerator->CalculateFluorescence(rhoT);
//       std::cout << fl[0] << std::endl;
      ret.push_back(fl);
    }
//     getchar();
    
    return ret;
}
   
vec<vec<dbl>> OBEsystem::Calculate(dbl from,dbl to,int steps)
{
  vec<vec<dbl>> ret(nReg,vec<dbl>(systemGenerator->GetFluroCount()));
  // std::cout << " Stating calc loop" << std::endl;
  for(dbl v=from+(to-from)/steps*0.5;v<to;v+=(to-from)/steps)
  {
 //std::cout << " In calc loop" << std::endl;
    
    systemGenerator->SetDoplerSpeed(v);
    auto tmp=GenerateMatrixAndSolve();
    dbl prob=SpeedProb(v)*(to-from)/steps;
    for(std::size_t i=0;i<ret.size();i++)
    {
      for(std::size_t j=0;j<ret[i].size();j++)
	ret[i][j]+=prob*tmp[i][j];
    }
  }
  return ret;
}
   
std::map<dbl,vec<dbl>> OBEsystem::CalculateMT(dbl from,dbl to, int steps)
{
  SetLocalN(nFull);

  std::map<dbl,vec<dbl>> ret;
  for (auto z: RadPoints)
    ret[z]=vec<dbl>(systemGenerator->GetFluroCount());
  
  int Reg= (nFull==1?1:2);
  
  for(;Reg<=nFull;Reg+=2)
  {
    SetLocalN(Reg);
    auto tmpR=Calculate(from,to,steps);
    for(std::size_t i=0;i<tmpR.size();i++)
    {
      for(std::size_t j=0;j<tmpR[i].size();j++)
	ret[laserRad[i].r][j]+=laserRad[i].vol*tmpR[i][j];
    }
  }
  for(auto & i:ret)
  {
    for(std::size_t j=0;j<i.second.size();j++)
      i.second[j]/=i.first*M_PI/LaserRadStep;
  }
  return ret;
}


   
dbl OBEsystem::GetMaxSpeed()
{
  return systemGenerator->GetDoplerStdDev()*2;
}
   
dbl OBEsystem::SpeedProb(dbl speed)
{
  return systemGenerator->GetSpeedProb(speed);
}



//Calculate AVG intensity in beam center.
   
dbl OBEsystem::AvgIntensity()
{

  if(nFull==1)
  {
    return laserIntens[0];
  }
  else 
  {
    return 0.721348*PIntens;
    dbl sigma=FWHM/2.35482;
    return 8*PIntens*sigma*sigma/FWHM*(1-exp(-FWHM*FWHM/8/(sigma*sigma)));
  }
  
}

//Set laser beam intesity from Rabi frequency and regenerate laser beam profile regions and intensities
   
void OBEsystem::SetRabiFreq(dbl Rab)//Rabi Frequency in 1/s not rad/s
{

  GenerateIntensityDistribution();
  dbl tmp=PIntens/AvgIntensity();
  PIntens=pow(2*M_PI*Rab*1e6*HBAR/RedMElem,2)*tmp;
  GenerateIntensityDistribution();  
}

   
dbl OBEsystem::GetRabiFreq()//Rabi Frequency in 1/s
{
  return sqrt(AvgIntensity())*RedMElem/HBAR*1e-6/2/M_PI;
}
  
dbl OBEsystem::GetB()
{
  return systemGenerator->GetB();
}
   
void OBEsystem::SetB(dbl val)
{
  systemGenerator->SetB(val);
}


   
void OBEsystem::test()
{
  
}
   
void OBEsystem::PrintRho(cvec & rho)
{
  dbl pop=0;
  for(Gst gi(systemGenerator->GetFG());gi.notFinal();++gi)
  {
  for(Gst gj(systemGenerator->GetFG());gj.notFinal();++gj)
    std::cout << rho[systemGenerator->gI(gi,gj)].real()<< "\t";
  pop+= rho[systemGenerator->gI(gi,gi)].real();
  std::cout << std::endl;
  }    
  std::cout << std::endl;
  for(Est gi(systemGenerator->GetFE());gi.notFinal();++gi)
  {
  for(Est gj(systemGenerator->GetFE());gj.notFinal();++gj)
    std::cout << rho[systemGenerator->gI(gi,gj)].real()<< "\t";
  pop+= rho[systemGenerator->gI(gi,gi)].real();
  std::cout << std::endl;
  }
  std::cout << "Full Population: " << pop << std::endl << std::endl;
}
   
dbl OBEsystem::GetDoplerStdDev()
{
  return systemGenerator->GetDoplerStdDev();
}
   
std::string OBEsystem::PrintFluroVec(int i)
{
  return systemGenerator->PrintFluroVec(i);
}

void OBEsystem::CalculateAssigment(std::shared_ptr< OBEassigment > asig)
{
  std::chrono::system_clock::time_point now = std::chrono::system_clock::now();

    
  if(GetConfig()!=asig->GetFile())
    InitConfig(asig->GetFile());
  
  systemGenerator->SetAssignment(asig);
  SetN(asig->GetReg());
  
  if(GetRabiFreq()!=asig->GetRabi())
    SetRabiFreq(asig->GetRabi());
  if(GetB()!=asig->GetB())
    SetB(asig->GetB());
  
  int flSize=0;
  if(asig->GetMultiTraj())
    {
      auto tmp=CalculateMT(asig->GetDF()*GetDoplerStdDev(),asig->GetDT()*GetDoplerStdDev(),asig->GetDS());
      asig->SetResult(tmp);
      flSize=tmp[laserRad[0].r].size();
    }
  else
  {
    auto tmp=Calculate(asig->GetDF()*GetDoplerStdDev(),asig->GetDT()*GetDoplerStdDev(),asig->GetDS());
    std::map<dbl,vec<dbl>> ttmp;
    for(int i=0;i<nReg;i++)
    {
      if(i<nReg/2)
	ttmp[-laserRad[i].r]=tmp[i];
      else
	ttmp[laserRad[i].r]=tmp[i];    
    }
    asig->SetResult(ttmp);
    flSize=tmp[0].size();
  }
  vec<std::string> fluroNames;
  for(int i=0;i<flSize;i++)
    fluroNames.push_back(PrintFluroVec(i));
  asig->SetNames(fluroNames);

  std::chrono::system_clock::time_point now2 =std::chrono::system_clock::now();
  std::cout << "Completed in "  << std::chrono::duration_cast<std::chrono::seconds>(now2-now).count() << std::endl;
}
