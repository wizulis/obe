
#include "assigment.hpp"
#include "scheduler.hpp"

void init();//defined in init.cpp
#include "etcgenerator.hpp"

int main(int argc, char * argv[]){
 
  using OBE::vec;
  using OBE::dbl;
    init();
    OBE::Scheduler sched(4);
    const auto BVec=vec<dbl>{0,0.5,1,2,4,6,8,10,12,14,16,18,20,25,30,35,40,50,60};
    using std::sqrt;
    vec<dbl> rr{18};
    for(OBE::dbl rab: rr){
        for(const std::string & def_id:
	      std::vector<std::string>{
                  "5317540edc2c2c59192d9fa1",
            /*	      	"53175408dc2c2c59192d9fa0",
		"53175411dc2c2c59192d9fa2",
		"531753fddc2c2c59192d9f9f" 
	      *///			  	"53175427dc2c2c59192d9fa4"//)  "531753fddc2c2c59192d9f9f"
	     	//	"53175469dc2c2c59192d9fa7",
		//	  "53175427dc2c2c59192d9fa4",
	      //			  "531754b6dc2c2c59192d9fa9",
        //	  	"531754c7dc2c2c59192d9faa",
			  //				"531754dfdc2c2c59192d9fac"

 	  }){
	  for(auto t:OBE::vec<std::string>{"T90"})
    for(OBE::dbl B:BVec){

                std::shared_ptr<OBE::Assigment> asg(new OBE::Assigment());

                OBE::vec<std::string> mods={"N20",t,"MR5","TC1","CC2"};
                asg->Marker="Cs_ETC_09_07";
                asg->SetModification(mods);
                asg->SetExperiment(def_id).SetB(B).SetDopplerP(-1,1,2.0/200).SetRabi(rab).SetOffset(0);
                sched.AddAssigment(asg);
            }
        }
    }
    return 0;
}
