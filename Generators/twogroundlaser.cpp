#include "twogroundlaser.hpp"
#include "equationmatrix.hpp"
#include "assigment.hpp"
#include "secondlasergenerator.hpp"
#include <iomanip>

namespace OBE {

    TwoWeakGroundLaser::TwoWeakGroundLaser(const AssigmentBase * asg, const DBContainer & p)
        : StateMixingGenerator(asg,p){

        subAsg.trans=Transition("Cs","D1");

        auto obj=p.get<DBContainer>("sub_level");
        subAsg.model.temperature=0;
        subAsg.model.regions=1;
        subAsg.loadTransition(obj);
        subAsg.loadModelCoeficients(obj);
        subAsg.loadPolarizations(obj);
        subAsg.generator=subAsg.LoadModel<BasicGenerator>(p,"SecondLaserGenerator");
        subAsg.trans.Energy_calc=subAsg.LoadModel<FrequencyCalculator>(p,"NonLinearZeeman");
        subAsg.generator->SetLaserIntensities({1000});
        subAsg.generator->TransitRateVector({subAsg.model.fwhm});

    }

    vec<dbl> TwoWeakGroundLaser::CalculateFluorescence(const Rho & r) const{


        UMFPACK_Matrix mat;
        subAsg.generator->SetDopplerSpeed(dopler_speed);

        auto second=subAsg.generator->GetRho();

        second->AcquireMemory();
        for(const State i:second->ref(Rho::LEVEL::EXITED))
            for(const State j:second->ref(Rho::LEVEL::EXITED))
                mat.AddRow(subAsg.generator->CreateRow(i,j,*second)[0]);

        cvec bvec;
        subAsg.generator->FillRHVec(r,bvec);

        mat.Solve(second->DataHandler(),bvec);
        vec<dbl> ret=subAsg.generator->CalculateFluorescence(*second);
        return ret;
    }


    TwoGroundLaser::TwoGroundLaser(const AssigmentBase * asg,const DBContainer & p)
        : StateMixingGenerator(asg,p){

        auto obj=p.get<DBContainer>("sub_level");

        std::string trans=obj.get<std::string>("trans");
        subAsg.trans=Transition(asg->trans.element,trans);

        subAsg.loadTransition(obj);
        subAsg.loadModelCoeficients(obj);
        subAsg.loadPolarizations(obj);

        subAsg.generator=subAsg.LoadModel<BasicGenerator>(p,"SecondLaserGenerator");
        subAsg.trans.Energy_calc=subAsg.LoadModel<FrequencyCalculator>(p,"NonLinearZeeman");
        subAsg.trans.Energy_calc->UpdateLevels();
        subAsg.trans.Energy_calc->SetB(asg->GetParam(Assigment::PARAMS::MAGNETIC_FIELD));
        subAsg.generator->TransitRateVector({subAsg.model.fwhm});
    }

    vec<dbl> TwoGroundLaser::CalculateFluorescence(const Rho & r) const{
        return subAsg.generator->CalculateFluorescence(r);
    }
    void TwoGroundLaser::ApplySecondTransitRelax(int ind,vec<cvec> & data)const{
        for(int i=1;i<data.size();i+=subAsg.model.regions+1){
            for(int j=0;j<subAsg.model.regions;j++)
                data[j+i][ind]-=subAsg.generator->TransitRate(j);
        }
    }
  void TwoGroundLaser::RemoveFirstTransitRelax(int ind,vec<cvec> & data) const{
    for(int i=1;i<data.size();i+=subAsg.model.regions+1){
        for(int j=0;j<subAsg.model.regions;j++)
          data[i+j][ind]+=TransitRate(i);
    }
  }
    vec<cvec> TwoGroundLaser::CreateRow(const State & i, const State & j, const Rho & r) const{

        assert(i.lvl==j.lvl);

        //For Exited state we can use both
        if(i.lvl==Exited() && j.lvl==Exited()){
           auto ret=StateMixingGenerator::CreateExitedRow(i,j,r);
           ApplySecondTransitRelax(r.index(i,j),ret);
           RemoveFirstTransitRelax(r.index(i,j),ret);
           return ret;
        }else if(i.lvl!=Ground() && j.lvl!=Ground()){
            auto row=subAsg.generator->CreateRow(i,j,r);

            vec<cvec> ret;
            ret.reserve(subAsg.model.regions*asg->model.regions);
            for(int i=0;i<asg->model.regions;i++){
                for(int j=0;j<row.size();j++){
                    ret.push_back(row[j]);
                }
            }
	    
            return ret;
        }



        auto data=StateMixingGenerator::CreateRow(i,j,r);
        assert(data.size()>0);
        RemoveFirstTransitRelax(r.index(i,j),data);

        auto data2=subAsg.generator->CreateRow(i,j,r);
        assert(data2.size()>0);
        assert(data2[0].size()==data[0].size());

        int reg_count=data.size();
        int r_size=data[0].size();

        for(int i=1;i<reg_count;i+=subAsg.model.regions+1){
            for(int k=0;k<subAsg.model.regions;k++)
                for(int j=0;j<r_size;j++){
                    data[i+k][j]+=data2[k][j];
                }
        }

        return data;
    }

    std::unique_ptr<Rho> TwoGroundLaser::GetRho() const{
        return std::unique_ptr<Rho>(new Rho2L(asg->trans,subAsg.trans));
    }
    void TwoGroundLaser::SetDopplerSpeed(const dbl val){
        StateMixingGenerator::SetDopplerSpeed(val);
        subAsg.generator->SetDopplerSpeed(val);
    }

    void TwoGroundLaser::UpdateCache(const Rho &r){
        StateMixingGenerator::UpdateCache(r);
        subAsg.trans.Energy_calc->SetB(asg->GetParam(Assigment::PARAMS::MAGNETIC_FIELD));
        subAsg.generator->UpdateCache(r);
    }
    vec<dbl> TwoGroundLaser::GetRadiusVec()const{

        if(subAsg.model.regions==1){
            return {subAsg.model.fwhm};
        }
        dbl sigma=subAsg.model.fwhm/SIGMA_COEF;
        dbl mr=sigma*subAsg.model.max_radius;
        dbl dr=2*mr/subAsg.model.regions;

        vec<dbl> rads;
        for(dbl val=-mr+dr/2;val < mr;val+= dr)
            rads.push_back(val);
        return rads;
    }
    void TwoGroundLaser::SetSecondLaserIntensity(dbl rabi){
        if(subAsg.model.regions==1){
            subAsg.generator->SetLaserIntensities({std::pow(rabi*1e6*HBAR/subAsg.trans.Reduced_matrix_elem,2)});
            subAsg.generator->TransitRateVector({subAsg.model.fwhm});
        }else{

            auto rads=GetRadiusVec();
            auto dr=std::abs(rads[0]-rads[1]);
            subAsg.generator->TransitRateVector(vec<dbl>(subAsg.model.regions,dr));

            vec<dbl> intens;
            dbl sigma=subAsg.model.fwhm/SIGMA_COEF;
            auto peak=std::pow(rabi*1e6*HBAR/subAsg.trans.Reduced_matrix_elem,2)/AVERAGE_GAUSS;
            for(auto &r:rads)
                intens.push_back(peak*std::exp(-r*r/2/sigma/sigma));
            subAsg.generator->SetLaserIntensities(intens);
        }
    }


    void TwoGroundLaser::SetLaserIntensities(const vec<dbl> & intensities){
        laser_intensity.clear();
        laser_intensity.reserve(intensities.size()*(1+subAsg.model.regions));
        for(auto i:intensities){
            for(int j=0;j<subAsg.model.regions+1;j++)
                laser_intensity.push_back(i);
        }
    }

    void TwoGroundLaser::SetTransitSpeed(const dbl speed){
        StateMixingGenerator::SetTransitSpeed(speed);
        subAsg.generator->SetTransitSpeed(speed);
    }

    const vec<dbl> & TwoGroundLaser::TransitRateVector(const vec<dbl> &regLen){
        vec<dbl> tr=StateMixingGenerator::TransitRateVector(regLen);
        transit_rate.clear();
        transit_rate.reserve(tr.size()*(1+subAsg.model.regions));
        for(auto i:tr){
            for(int j=0;j<subAsg.model.regions+1;j++)
                transit_rate.push_back(i);
        }
        return transit_rate;
    }

    void TwoGroundLaser::Fill2LaserRHVec(const Rho & rho_ext, const Rho & rho_unper, cvec & bvec, int regions) const {
      subAsg.generator->FillRHVec(rho_unper,bvec,0);
    }
}
