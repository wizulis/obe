#ifndef FULLEQUATIONGENERATOR_HPP
#define FULLEQUATIONGENERATOR_HPP
#include "basicgenerator.hpp"
#include "statemixinggenerator.hpp"
#include "etcgenerator.hpp"

namespace OBE{
    template<typename T>
    class FullEquationGenerator: public T
    {
        using State=Rho::State;
    public:

        FullEquationGenerator(const AssigmentBase * assig,const DBContainer &p);

        virtual vec<cvec> CreateRow(const State &,const State &,const Rho &)const;

        std::unique_ptr<Rho> GetRho() const override;
        bool IsOCSupported()const override {return true;}
        void FillRHVec(const Rho &,cvec & ,int region) const override ;

    protected:
        virtual vec<cvec> CreateGroundRow(const State &,const State &,const Rho &)const;
        virtual vec<cvec> CreateEGRow(const State &,const State &,const Rho &)const;
        virtual vec<cvec> CreateGERow(const State &,const State &,const Rho &)const;
        virtual vec<cvec> CreateExitedRow(const State &,const State &,const Rho &)const;
    };

    using FullEqBasic=FullEquationGenerator<BasicGenerator>;
    using FullEqMixing=FullEquationGenerator<StateMixingGenerator>;
    using FullEqETC=FullEquationGenerator<ETCGenerator>;
}
#endif // FULLEQUATIONGENERATOR_HPP
