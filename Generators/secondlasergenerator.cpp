#include "secondlasergenerator.hpp"
#include "assigmentbase.hpp"
#include "assigment.hpp"
namespace OBE{

    SecondLaserGenerator::SecondLaserGenerator(const AssigmentBase * asg,const DBContainer & p)
        : StateMixingGenerator(asg,p) {

    }

//    vec<dbl> SecondWeakLaserGenerator::CalculateFluorescence(const Rho & rho) const {

//        const int offset=pow(rho.LevelSize(Rho::LEVEL::GROUND),2);
//        vec<dbl> ret;
//        ret.reserve(asg->fluroescence_polar_list.size());
//        auto & rr=rho.DataHandler();
//        for(const auto &vec: asg->fluroescence_polar_list){
//            cdbl sum(0,0);
//            for(State r:rho.ref(Rho::LEVEL::EXITED))
//                for(State s:rho.ref(Rho::LEVEL::EXITED))
//                    for(auto m:rho.ref(Rho::LEVEL::GROUND))
//                        sum+=TransitionAmplitude(m,r,vec)*TransitionAmplitude(s,m,vec)*rr[-offset+rho.index(r,s)];

//            ret.push_back(sum.real()/pow(asg->trans.Reduced_matrix_elem,2)*asg->trans.Spont_rate);
//        }
//        if(ret[0]==0)
//            throw std::runtime_error("CALC");
//      //  std::cout << ret    <<std::endl;
//        return ret;
//    }


//    void SecondLaserGenerator::FillRHVec(const Rho & r,cvec & bvec,int region) const{

//        Rho mrho(asg->trans);
//        std::size_t offset=pow(mrho.LevelSize(Rho::LEVEL::GROUND),2);
//        std::size_t vecSize=pow(mrho.LevelSize(Rho::LEVEL::EXITED),2);

//        if(bvec.size()!=vecSize)
//            bvec.resize(vecSize,0);

//        std::fill(bvec.begin(),bvec.end(),0);


//        for(State i: mrho.ref(Rho::LEVEL::EXITED)){
//            for(State j: mrho.ref(Rho::LEVEL::EXITED)){
//                for(State k: r.ref(Rho::LEVEL::GROUND)){
//                    for(State m: r.ref(Rho::LEVEL::GROUND)){

//                        cdbl transAmp=TransitionAmplitude(i,k,asg->laser_polarization)
//                                *TransitionAmplitude(m,j,asg->laser_polarization);

//                        cdbl o(1,0);
//                        cdbl x((asg->trans.Spont_rate+asg->model.laser_spectral_width)/2,
//                               -(asg->model.laser_freq-DoplerShift()-TransitionFrequency(i,m)));
//                        cdbl y(x.real(),
//                               (asg->model.laser_freq-DoplerShift()-TransitionFrequency(j,k)));

//                         x=o/x;
//                         y=o/y;

//                         bvec[-offset+mrho.index(i,j)]-=r(k,m)*(x+y)*transAmp*laser_intensity[0]/HBAR/HBAR;

//                    }
//                }
//            }
//        }
//    }

//    vec<cvec> SecondLaserGenerator::CreateExitedRow(const State &i, const State & j, const Rho &rho) const {
//        const int lvlSize=pow(rho.LevelSize(Rho::LEVEL::EXITED),2);
//        const int offset=pow(rho.LevelSize(Rho::LEVEL::GROUND),2);

//        vec<cvec> row{cvec(lvlSize,0)};

///*        //First sum
//        for(State k: rho.ref(Rho::LEVEL::GROUND)){
//            for(State m: rho.ref(Rho::LEVEL::GROUND)){

//                cdbl transAmp=TransitionAmplitude(i,k,asg->laser_polarization)
//                        *TransitionAmplitude(m,j,asg->laser_polarization);

//                for(std::size_t dal=0; dal<row.size(); dal++){
//                    cdbl o(1,0);
//                    cdbl x((asg->trans.Spont_rate+asg->model.laser_spectral_width)/2,
//                           -(asg->model.laser_freq-DoplerShift()-TransitionFrequency(i,m)));
//                    cdbl y(x.real(),
//                           (asg->model.laser_freq-DoplerShift()-TransitionFrequency(j,k)));

//                    x=o/x;
//                    y=o/y;

//                    row[dal][rho.index(k,m)]+=(x+y)*transAmp*laser_intensity[dal]/HBAR/HBAR;
//                }
//            }
//        }//END First sum
//*/
//        //Second and Third sum
//        for(State k: rho.ref(Rho::LEVEL::GROUND)){
//            for(State m: rho.ref(Rho::LEVEL::EXITED)){

//                cdbl transAmp1=TransitionAmplitude(i,k,asg->laser_polarization)
//                        *TransitionAmplitude(k,m,asg->laser_polarization);

//                cdbl transAmp2=TransitionAmplitude(m,k,asg->laser_polarization)
//                        *TransitionAmplitude(k,j,asg->laser_polarization);

//                for(std::size_t dal=0; dal<row.size(); dal++){
//                    cdbl o(1,0);
//                    cdbl x((asg->trans.Spont_rate+asg->model.laser_spectral_width)/2,
//                           (asg->model.laser_freq-DoplerShift()-TransitionFrequency(j,k)));
//                    cdbl y(x.real(),
//                           -(asg->model.laser_freq-DoplerShift()-TransitionFrequency(i,k)));

//                    x=o/x;
//                    y=o/y;

//                    row[dal][-offset+rho.index(m,j)]-=x*transAmp1*laser_intensity[dal]/HBAR/HBAR;
//                    row[dal][-offset+rho.index(i,m)]-=y*transAmp2*laser_intensity[dal]/HBAR/HBAR;
//                }
//            }
//        }//End Second and Third sum


//        //State spliting,transit rate and colision decay
//        int idx=-offset+rho.index(i,j);
//        for(std::size_t dal=0;dal<row.size();dal++){

//            cdbl & elem=row[dal][idx];

//            elem-=cdbl(0,TransitionFrequency(i,j));
//            //Collisition relaxation
//            elem-=ColisionRate();
//            //Trans rate decay
//            elem-=TransitRate(dal);
//            //Spontanious decay
//            elem-=asg->trans.Spont_rate;
//        }

//        return row;

//    }


    vec<cvec> SecondLaserGenerator::CreateGroundRow(const State &i, const State &j, const Rho &r) const {
        auto ret=StateMixingGenerator::CreateGroundRow(i,j,r);
        int ind=r.index(i,j);
        int count=0;
        for(auto & m:ret){
            m[ind]+=cdbl(ColisionRate(),TransitionFrequency(i,j));
            count++;
        }
        return ret;
    }

}
