#ifndef ETCGENERATOR_HPP
#define ETCGENERATOR_HPP
#include "statemixinggenerator.hpp"

namespace OBE {
    class ETCGenerator : public StateMixingGenerator{
        dbl cell_length;
    public:

        dbl cell_coef;
      dbl ang_coef;
        ETCGenerator(const AssigmentBase * asg, const DBContainer & p);

        virtual dbl ColisionRate()const override;
    };
}
#endif // ETCGENERATOR_HPP
