#ifndef STATEMIXINGGENERATOR_HPP
#define STATEMIXINGGENERATOR_HPP

#include "matrix.hpp"
#include "typedefinitions.hpp"
#include "basicgenerator.hpp"
#include <map>


namespace OBE {
    class StateMixingGenerator : public BasicGenerator{

        std::map<PolarizationVector,std::pair<TMatrix<cdbl>,TMatrix<cdbl>>> cache;
        vec<dbl> gamma_cache;
        int size_ex,size_gr;
    public:
        StateMixingGenerator(const AssigmentBase * asg_p,const DBContainer & p);

        virtual cdbl TransitionAmplitude(const State &i, const State &j, const PolarizationVector &vec) const override;
        virtual dbl Gamma(const State &, const State &, const State &, const State &) const override;

        virtual void UpdateCache(const Rho &) override;
    private:
        int Index(const State & i) const;

        void PrecalculateGamma(const Rho & r);
        virtual dbl RawGamma(const State &, const State &, const State &, const State &,const Rho & r)const;
        void  PrecalculateTransAmp(const PolarizationVector & vec,const Rho & r);
        cdbl  RawTransAmp(const State & i,const State &j,const PolarizationVector & vec,const Rho & r);

    };
}
#endif // STATEMIXINGGENERATOR_HPP
