#ifndef TWOGROUNDLASER_HPP
#define TWOGROUNDLASER_HPP
#include "assigmentbase.hpp"
#include "statemixinggenerator.hpp"
#include "polarizationvector.hpp"
#include "transition.hpp"
namespace OBE {
    class TwoWeakGroundLaser : public StateMixingGenerator{

        AssigmentBase subAsg;
    public:
        TwoWeakGroundLaser(const AssigmentBase * asg,const DBContainer & p);
        virtual vec<dbl> CalculateFluorescence(const Rho & ) const override;
    };

    class TwoGroundLaser : public StateMixingGenerator{


        AssigmentBase subAsg;
    public:
        TwoGroundLaser(const AssigmentBase * asg,const DBContainer & p);



        virtual vec<dbl> CalculateFluorescence(const Rho & ) const override;
        AssigmentBase & GetSubAsg(){return subAsg;}
        vec<dbl> GetRadiusVec()const;
        virtual vec<cvec> CreateRow(const State &,const State &,const Rho &)const override;
        std::unique_ptr<Rho> GetRho() const override;
        void SetDopplerSpeed(const dbl val);
        virtual void UpdateCache(const Rho &) override;
        void SetSecondLaserIntensity(dbl intensity);
        void SetLaserIntensities(const vec<dbl> & laser) override;
        void SetTransitSpeed(const dbl) override;

        virtual const vec<dbl> & TransitRateVector(const vec<dbl> & regLen=vec<dbl>()) override;
        virtual void Fill2LaserRHVec(const Rho &,const Rho &,cvec &,int)const;
    protected:
        void ApplySecondTransitRelax(int ind,vec<cvec> & data)const;
      void RemoveFirstTransitRelax(int ind,vec<cvec> & data) const;
    };
}

#endif // TWOGROUNDLASER_HPP
