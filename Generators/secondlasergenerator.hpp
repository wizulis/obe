#ifndef SECONDLASERGENERATOR_HPP
#define SECONDLASERGENERATOR_HPP
#include "statemixinggenerator.hpp"


namespace OBE {
    class SecondLaserGenerator : public StateMixingGenerator {
    public:
        SecondLaserGenerator(const AssigmentBase * asg,const DBContainer & p);
        virtual Rho::LEVEL Exited()const override {return Rho::LEVEL::EXITED_2;}
    protected:
        vec<cvec> CreateGroundRow(const State &, const State &, const Rho &) const override;
    };
}

#endif // SECONDLASERGENERATOR_HPP
