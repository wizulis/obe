#include "assigmentbase.hpp"
#include "fullequationgenerator.hpp"

namespace OBE{

    template<class T>
    FullEquationGenerator<T>::FullEquationGenerator(const AssigmentBase * asg_p,const DBContainer &p):T(asg_p,p)
    {
    }

    template<class T>
    vec<cvec> FullEquationGenerator<T>::CreateRow(const State & i, const State & j,const Rho & rho) const{
        if(i.lvl==Rho::LEVEL::GROUND && j.lvl==Rho::LEVEL::GROUND)
            return CreateGroundRow(i,j,rho);
        else if(i.lvl==Rho::LEVEL::EXITED && j.lvl==Rho::LEVEL::EXITED)
            return CreateExitedRow(i,j,rho);
        else if(i.lvl==Rho::LEVEL::EXITED){
            return CreateEGRow(i,j,rho);
        }else{
            return CreateGERow(i,j,rho);
        }

    }

    template<class T>
    std::unique_ptr<Rho> FullEquationGenerator<T>::GetRho() const {
        return std::unique_ptr<Rho>(new RhoOC(T::asg->trans));
    }
    template<class T>
    void FullEquationGenerator<T>::FillRHVec(const Rho &r ,cvec & v,int region) const{
        T::FillRHVec(r,v,region);
        for(auto i:r.ref(Rho::LEVEL::GROUND)){
            for(auto j:r.ref(Rho::LEVEL::EXITED)){
                v[r.index(i,j)]+=r(i,j)*T::TransitRate(region)/2.0;
                v[r.index(j,i)]+=r(j,i)*T::TransitRate(region)/2.0;
            }
        }

    }

    template<class T>
    vec<cvec> FullEquationGenerator<T>::CreateGroundRow(const State &i,const State &j,const Rho &r)const{
        const int regs=T::laser_intensity.size();
        vec<cvec> row(regs,cvec(r.MatrixSize(),{0,0}));
    /*    const_cast<FullEquationGenerator<T> *>(this)->laser_intensity=vec<dbl>(row.size(),0);
        const_cast<FullEquationGenerator<T> *>(this)->dopler_speed=0;
        const_cast<AssigmentBase*>(T::asg)->model.trans_rate_coef=0;
        reinterpret_cast<ETCGenerator *>(const_cast<FullEquationGenerator<T>*>(this))->cell_coef=0;
*/
        for(State k:r.ref(Rho::LEVEL::EXITED)){

            auto amp1=T::TransitionAmplitude(i,k,T::asg->laser_polarization);
            auto amp2=T::TransitionAmplitude(k,j,T::asg->laser_polarization);
            int ind1=r.index(k,j);
            int ind2=r.index(i,k);
            for(int dal=0;dal<regs;dal++){
                row[dal][ind1]+=sqrt(T::laser_intensity[dal])/HBAR*cdbl{0,1}*amp1;
                row[dal][ind2]-=sqrt(T::laser_intensity[dal])/HBAR*cdbl{0,1}*amp2;
            }
        }
        int ind=r.index(i,j);
        for(int dal=0;dal<regs;dal++){
            row[dal][ind]-=cdbl{T::ColisionRate()+T::TransitRate(dal),T::TransitionFrequency(i,j)};
        }

        //Spontanius decay
        for(State k: r.ref(Rho::LEVEL::EXITED)){
            for(State m: r.ref(Rho::LEVEL::EXITED)){

                cdbl tmpV=cdbl(T::Gamma(i,k,m,j),0);
                ind=r.index(k,m);
                for(std::size_t dal=0; dal<regs; dal++)
                    row[dal][ind]+=tmpV;
            }
        }
        return row;

    }

    template<class T>
    vec<cvec> FullEquationGenerator<T>::CreateEGRow(const State & i,const State & j,const Rho & r)const{
        const int regs=T::laser_intensity.size();
        vec<cvec> row(regs,cvec(r.MatrixSize(),{0,0}));

        for(State k:r.ref(Rho::LEVEL::GROUND)){
            auto amp1=T::TransitionAmplitude(i,k,T::asg->laser_polarization);
            int ind1=r.index(k,j);
            for(int dal=0;dal<regs;dal++){
                row[dal][ind1]+=sqrt(T::laser_intensity[dal])/HBAR*cdbl{0,1}*amp1;
            }
        }

        for(State k:r.ref(Rho::LEVEL::EXITED)){
            auto amp2=T::TransitionAmplitude(k,j,T::asg->laser_polarization);
            int ind2=r.index(i,k);
            for(int dal=0;dal<regs;dal++){
                row[dal][ind2]-=sqrt(T::laser_intensity[dal])/HBAR*cdbl{0,1}*amp2;
            }
        }


        int ind=r.index(i,j);
        auto trf=cdbl{-(T::asg->trans.Spont_rate+T::ColisionRate())/2.0,T::asg->model.laser_freq-T::DoplerShift()-T::TransitionFrequency(i,j)};
        for(int dal=0;dal<regs;dal++){
            row[dal][ind]+=trf-T::TransitRate(dal)/2.0;
        }

        return row;

    }

    template<class T>
    vec<cvec> FullEquationGenerator<T>::CreateGERow(const State & i,const State & j,const Rho &r)const{
        const int regs=T::laser_intensity.size();
        vec<cvec> row(regs,cvec(r.MatrixSize(),{0,0}));

        for(State k:r.ref(Rho::LEVEL::EXITED)){
            auto amp1=T::TransitionAmplitude(i,k,T::asg->laser_polarization);
            int ind1=r.index(k,j);
            for(int dal=0;dal<regs;dal++){
                row[dal][ind1]+=sqrt(T::laser_intensity[dal])/HBAR*cdbl{0,1}*amp1;
            }
        }

        for(State k:r.ref(Rho::LEVEL::GROUND)){
            auto amp2=T::TransitionAmplitude(k,j,T::asg->laser_polarization);
            int ind2=r.index(i,k);
            for(int dal=0;dal<regs;dal++){
                row[dal][ind2]-=sqrt(T::laser_intensity[dal])/HBAR*cdbl{0,1}*amp2;
            }
        }


        int ind=r.index(i,j);
        auto trf=cdbl{-(T::asg->trans.Spont_rate+T::ColisionRate())/2.0,-(T::asg->model.laser_freq-T::DoplerShift()+T::TransitionFrequency(i,j))};
        for(int dal=0;dal<regs;dal++){
            row[dal][ind]+=trf-T::TransitRate(dal)/2.0;
        }

        return row;


    }
    template<class T>
    vec<cvec> FullEquationGenerator<T>::CreateExitedRow(const State & i,const State & j ,const Rho & r)const{
        const int regs=T::laser_intensity.size();
        vec<cvec> row(regs,cvec(r.MatrixSize(),{0,0}));

        for(State k:r.ref(Rho::LEVEL::GROUND)){
            auto amp1=T::TransitionAmplitude(i,k,T::asg->laser_polarization);
            auto amp2=T::TransitionAmplitude(k,j,T::asg->laser_polarization);
            int ind1=r.index(k,j);
            int ind2=r.index(i,k);
            for(int dal=0;dal<regs;dal++){
                row[dal][ind1]+=sqrt(T::laser_intensity[dal])/HBAR*cdbl{0,1}*amp1;
                row[dal][ind2]-=sqrt(T::laser_intensity[dal])/HBAR*cdbl{0,1}*amp2;
            }
        }


        int ind=r.index(i,j);
        auto trf=cdbl{-(T::asg->trans.Spont_rate+T::ColisionRate()),-T::TransitionFrequency(i,j)};
        for(int dal=0;dal<regs;dal++){
            row[dal][ind]+=trf-T::TransitRate(dal);
        }

        return row;
    }

    template class FullEquationGenerator<BasicGenerator>;
    template class FullEquationGenerator<StateMixingGenerator>;
    template class FullEquationGenerator<ETCGenerator>;
}

