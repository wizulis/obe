#include "etcgenerator.hpp"
#include "assigment.hpp"
namespace OBE{

    ETCGenerator::ETCGenerator(const AssigmentBase * asg_ptr, const DBContainer &p):StateMixingGenerator(asg_ptr,p){

        if(!p.fill(cell_length,"cell_length"))
            throw std::runtime_error("Cell length not sepecified or specified incorrectly");
        cell_length*=1e-3;

        if(!p.fill(cell_coef,"cell_coef"))
            cell_coef=2*M_PI;

    }

    dbl ETCGenerator::ColisionRate()const{
      return asg->model.colision_prob
	//	+6.28*sqrt(M_PI*MOL_R*asg->model.temperature/asg->trans.Atom_mass/2)*ang_coef/cell_length
        +cell_coef*std::abs(dopler_speed)/cell_length;
    }

}
