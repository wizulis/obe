#include "statemixinggenerator.hpp"
#include "assigment.hpp"
#include "transition.hpp"
#include "frequencycalculator.hpp"

namespace OBE {

    StateMixingGenerator::StateMixingGenerator(const AssigmentBase *asg_p,  const DBContainer &p):BasicGenerator(asg_p,p),size_ex(0),size_gr(0){

        for(const int angMom:asg->trans.Angular_moment[Rho::LEVEL::EXITED])
            size_ex+=angMom+1;
        for(const int angMom:asg->trans.Angular_moment[Rho::LEVEL::GROUND])
            size_gr+=angMom+1;

        gamma_cache.resize(pow(size_ex,2)*pow(size_gr,2),0);
     }


    int StateMixingGenerator::Index(const State &i) const{
        int ind=0;
        for(const int angMom:i.parent.AngMom(i.lvl)){
            if(angMom<i.F)
                ind+=angMom+1;
            else if(angMom==i.F){
                ind+=(i.mF+angMom)/2;
                return ind;
            }
        }
    }

    cdbl StateMixingGenerator::TransitionAmplitude(const State &i, const State &j, const PolarizationVector &vec) const{


        auto itr=cache.find(vec);

        const State & ex = i.lvl==Exited()? i:j;
        const State & gr = i.lvl==Exited()? j:i;

        int index_ex=Index(ex),index_gr=Index(gr);

        if(i.lvl==Ground()){
            return itr->second.first(index_gr,index_ex);
        }else{
            return itr->second.second(index_gr,index_ex);
        }


    }

    dbl StateMixingGenerator::Gamma(const State &a, const State &b, const State &c, const State &d) const{

        if((b.lvl!=c.lvl || b.lvl!=Exited())|| (a.lvl!=d.lvl || a.lvl!=Ground()))
            throw std::runtime_error("Gamma error");

        int ind_a=Index(a),ind_b=Index(b),ind_c=Index(c),ind_d=Index(d);

        int ind=ind_a*(size_ex*size_ex*size_gr);
        ind+=ind_b*(size_ex*size_gr);
        ind+=ind_c*size_gr;
        ind+=ind_d;

        return gamma_cache[ind];
    }

    void StateMixingGenerator::UpdateCache(const Rho & r){
        cache.clear();
        PrecalculateTransAmp(asg->laser_polarization,r);
        for(const auto & vec:asg->fluroescence_polar_list){
            PrecalculateTransAmp(vec,r);
        }
        PrecalculateGamma(r);
    }

    void StateMixingGenerator::PrecalculateGamma(const Rho & r){

        memset(gamma_cache.data(),0,sizeof(dbl)*gamma_cache.size());

        for(auto & a:r.ref(Ground())){
            for(auto & b:r.ref(Exited())){
                for(auto & c:r.ref(Exited())){
                    for(auto & d:r.ref(Ground())){
                        int ind_a=Index(a),ind_b=Index(b),ind_c=Index(c),ind_d=Index(d);

                        int ind=ind_a*(size_ex*size_ex*size_gr);
                        ind+=ind_b*(size_ex*size_gr);
                        ind+=ind_c*size_gr;
                        ind+=ind_d;

                        gamma_cache[ind]=RawGamma(a,b,c,d,r);
                    }
                }
            }
        }
    }

    void StateMixingGenerator::PrecalculateTransAmp(const PolarizationVector & vec,const Rho & r){

        std::pair<TMatrix<cdbl>,TMatrix<cdbl>> & mat_pair=cache[vec];
        mat_pair.first.Resize(size_gr,size_ex,{0,0});
        mat_pair.second.Resize(size_gr,size_ex,{0,0});


        int g_i=0;
        int e_i=0;

        for(const State g:r.ref(Ground())){
            e_i=0;
            for(const State e:r.ref(Exited())){
                mat_pair.first(g_i,e_i)=RawTransAmp(g,e,vec,r);
                mat_pair.second(g_i,e_i)=RawTransAmp(e,g,vec,r);
                e_i++;
            }
            g_i++;
        }
    }
    cdbl StateMixingGenerator::RawTransAmp(const State & i,const State &j,const PolarizationVector & vec,const Rho & r){
        cdbl ret(0,0);
        for(const State a:r.ref(i.lvl)){
            if(a.mF!=i.mF)
                continue;

            auto val=asg->trans.Energy_calc->MixingCoeficient(i,a);
            if(val==0)
                continue;

            for(const State b:r.ref(j.lvl)){
                if(b.mF!=j.mF)
                    continue;

                auto val2=asg->trans.Energy_calc->MixingCoeficient(j,b);
                if(val2==0.0)
                    continue;

                ret+=val*val2*BasicGenerator::TransitionAmplitude(a,b,vec);
            }
        }
        return ret;

    }


    dbl StateMixingGenerator::RawGamma(const State &a, const State &b, const State &c, const State &d,const Rho & r) const{

        dbl ret(0);


        for(const State aa:r.ref(a.lvl)){
            dbl val_a=asg->trans.Energy_calc->MixingCoeficient(a,aa);
            if(val_a==0)
                continue;
            for(const State bb:r.ref(b.lvl)){
                dbl val_b=asg->trans.Energy_calc->MixingCoeficient(b,bb);
                if(val_b==0)
                    continue;
                for(const State cc:r.ref(c.lvl)){
                    dbl val_c=asg->trans.Energy_calc->MixingCoeficient(c,cc);
                    if(val_c==0)
                        continue;
                    for(const State dd:r.ref(d.lvl)){
                        dbl val_d=asg->trans.Energy_calc->MixingCoeficient(d,dd);
                        if(val_d==0)
                            continue;
                        ret+=val_a*val_b*val_c*val_d*BasicGenerator::Gamma(aa,bb,cc,dd);
                    }
                }
            }
        }
        return ret;
    }
}
