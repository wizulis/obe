#include "assigment.hpp"
#include "scheduler.hpp"

void init();//defined in init.cpp



int main(int argc, char * argv[]){
 
    init();
    OBE::Scheduler sched(8);
    OBE::vec<OBE::dbl> rv={48};
     
    //    auto exp= "53690047313945443138fe78";
     auto exp = "53175427dc2c2c59192d9fa4";
     //auto exp="537e0d6e7c96ff4b5e83b46f";
     auto BVec=OBE::vec<OBE::dbl>{};for(double B=0;B<60;B+=2)BVec.push_back(B);
    //auto BVec=OBE::vec<OBE::dbl>{0,0.1,0.5,1,2,3,4,6,8,10,12,16,20};

    for(auto rab:rv){
      for(auto fw:OBE::vec<std::string>{"T100","T120","T130","T140","T165"}){
      for(OBE::dbl B:BVec){
//	if(fw=="T100" && B < 20)
//		continue;

        std::shared_ptr<OBE::Assigment> asg(new OBE::Assigment());
	asg->SetModification(OBE::vec<std::string>{"N10",fw,"CC1","TC1","MR5"});
        asg->SetExperiment(exp).SetB(B).SetDopplerP(-1,1,2.0/200).SetRabi(rab).SetOffset(0);
        asg->Marker="tmp_data3";
        sched.AddAssigment(asg);
    }
}
}

    return 0;
}
