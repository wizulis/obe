#ifndef ASSIGMENTDEAMON_HPP
#define ASSIGMENTDEAMON_HPP

#include <deque>
#include <list>
#include <thread>
#include <mutex>
#include <condition_variable>
#include <atomic>

namespace OBE{
    class Assigment;

    namespace Deamon{
        class AssigmentDeamon
        {


            friend class ThreadObj;

            class ThreadObj{

                AssigmentDeamon & deamon;
                std::atomic_bool running;
                std::thread thread_obj;
            public:

                ThreadObj(AssigmentDeamon & parent);
                ThreadObj(ThreadObj &&);
                void run();
                void stop();
                ~ThreadObj();
            protected:
                void ThreadFunction();
            };

            std::deque<std::pair<long,std::shared_ptr<Assigment>>> assigments;
            std::list<std::shared_ptr<Assigment>> toSave;
            std::list<ThreadObj> threads;
            std::condition_variable cond;
            std::mutex mut;
            std::mutex save_mut;

            std::thread saveThread;

            long maxInt=0;
            std::atomic_bool running;

        public:

            AssigmentDeamon();

            long AddAssigment( std::shared_ptr<Assigment>);
            void RemoveAssigment(long);
            void NeedsSaving( std::shared_ptr<Assigment>);
            void run(int);
            void stop();
        protected:
            void SaveThreadFunc();
        };
    }
}
#endif // ASSIGMENTDEAMON_HPP
