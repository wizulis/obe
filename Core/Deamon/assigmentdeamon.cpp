#include "assigmentdeamon.hpp"
#include "assigment.hpp"


namespace OBE{
    namespace Deamon{

        AssigmentDeamon::ThreadObj::ThreadObj(AssigmentDeamon & parent):deamon(parent){
            running.store(false);
        }

        AssigmentDeamon::ThreadObj::ThreadObj(ThreadObj && obj)
            : deamon(obj.deamon),
              running(false),
              thread_obj(std::move(obj.thread_obj)){

        }

        void AssigmentDeamon::ThreadObj::run(){
            if(running.load())
                return;
            running.store(true);
            thread_obj=std::thread(&AssigmentDeamon::ThreadObj::ThreadFunction,this);

        }

        void AssigmentDeamon::ThreadObj::stop(){
            running.store(false);
        }

        AssigmentDeamon::ThreadObj::~ThreadObj(){
            if(thread_obj.joinable())
                thread_obj.join();
        }

        void AssigmentDeamon::ThreadObj::ThreadFunction(){
            while(running){
                std::shared_ptr<Assigment> asg;

                {
                    std::unique_lock<std::mutex> lock(deamon.mut);
                    if(deamon.assigments.size()>0){
                        asg=deamon.assigments.front().second;
                        deamon.assigments.pop_front();
                    }
                    else{
                        while(running.load() & deamon.assigments.size()==0)
                            deamon.cond.wait(lock,[&](){return !running.load();});
                        if(!running.load())
                            return;


                        asg=deamon.assigments.front().second;
                        deamon.assigments.pop_front();

                    }
                }

                if(!running.load())
                    return;

                if(!asg->Run()){
                    deamon.NeedsSaving(asg);
                }
            }
        }

        AssigmentDeamon::AssigmentDeamon(){
            running.store(false);
         }

        long AssigmentDeamon::AddAssigment(std::shared_ptr<Assigment> asg){
            std::unique_lock<std::mutex> lock(mut);
            bool notify_at_end=false;
            if(assigments.size()==0)
                notify_at_end=true;

            long ret=++maxInt;
            assigments.push_back(std::pair<long,std::shared_ptr<Assigment>>(ret,asg));
            if(notify_at_end)
                cond.notify_one();
            return ret;
        }
        void AssigmentDeamon::RemoveAssigment(long id){
            std::unique_lock<std::mutex> lock(mut);
            for(auto itr=assigments.begin();itr!=assigments.end();++itr){
                if(itr->first==id){
                    assigments.erase(itr);
                    break;
                }
            }
        }

        void AssigmentDeamon::NeedsSaving( std::shared_ptr<Assigment> asg){
            std::unique_lock<std::mutex> lock(save_mut);
            toSave.push_back(asg);
        }

        void AssigmentDeamon::run(int threads){
            running.store(true);
            saveThread=std::thread(&AssigmentDeamon::SaveThreadFunc,this);

            for(int i=0;i<threads;i++){
                this->threads.push_back(ThreadObj(*this));
            }
            for(auto & obj:this->threads)
                obj.run();
        }
        void AssigmentDeamon::stop(){

            running.store(false);
            for(auto & obj:threads){
                obj.stop();
            }
            cond.notify_all();
            saveThread.join();
        }
        void AssigmentDeamon::SaveThreadFunc(){
            while(running){
                std::this_thread::sleep_for(std::chrono::seconds(5));
                std::unique_lock<std::mutex> lock(save_mut);
                if(toSave.size()>0){
                    auto it=toSave.begin();
                    while(it!=toSave.end()){
                        if((*it)->SendResults()){
                            it=toSave.erase(it);
                        }else{
                            break;
                        }
                    }
                }

            }
        }

    }
}

