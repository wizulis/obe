#include "matrix.hpp"
#include <gsl/gsl_eigen.h>

#include <iostream>

namespace OBE{




void Matrix::EigenValues(vec<dbl> &eigen_val, Matrix &eigen_vec)const{

    if(rowC!=colC)
        throw std::runtime_error("Matrix not square");

    vec<dbl> tmpData=data;
    gsl_matrix_view m = gsl_matrix_view_array (tmpData.data(), rowC, colC);
    gsl_vector *eval = gsl_vector_alloc (rowC);
    gsl_matrix *evec = gsl_matrix_alloc (rowC,colC);

    gsl_eigen_symmv_workspace * w = gsl_eigen_symmv_alloc (rowC);
    gsl_eigen_symmv (&m.matrix, eval, evec, w);

    gsl_eigen_symmv_free (w);

    gsl_eigen_symmv_sort (eval, evec,GSL_EIGEN_SORT_VAL_ASC);

    eigen_val.resize(rowC);
    for(int i=0;i<rowC;i++)
        eigen_val[i]=gsl_vector_get(eval,i);


    eigen_vec.Resize(rowC,colC);
    for(int i=0;i<rowC;i++){
        for(int j=0;j<colC;j++){
            eigen_vec(i,j)=gsl_matrix_get(evec,i,j);
        }

    }

    gsl_vector_free (eval);
    gsl_matrix_free (evec);
}



    void Matrix::print(){
        for(int i=0;i<rowC;i++){
            for(int j=0;j<colC;j++){
                std::cout <<"\t"<<(*this)(i,j);
            }
        std::cout << std::endl;
        }



    }
}
