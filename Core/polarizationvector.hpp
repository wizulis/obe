#ifndef POLARIZATIONVECTOR_HPP
#define POLARIZATIONVECTOR_HPP
#include "typedefinitions.hpp"
#include "dbcontainer.hpp"
//#include <mongo/client/dbclientcursor.h>


namespace OBE{


    class PolarizationVector;
    bool operator<(const PolarizationVector & a,const PolarizationVector & b);

    class PolarizationVector
    {

        std::string name;
        cdbl elems[3];
    public:


        PolarizationVector();
        PolarizationVector & operator=(const PolarizationVector & other);

        const std::string & GetName() const;
        static PolarizationVector FromDBContainer(const DBContainer & pol_vec);
        static PolarizationVector FromSpherical(const cdbl & qm1,const cdbl & q0,const cdbl & q1,const std::string & name=std::string());
        static PolarizationVector FromDecart(const cdbl & x,const cdbl & y,const cdbl & z,const std::string & name=std::string());

        void initFromSpherical(const cdbl & qm1,const cdbl & q0,const cdbl & q1);
        void initFromXYZ(const cdbl & x,const cdbl & y,const cdbl & z);

        cdbl & operator[](const int indx);
        const cdbl & operator[] (const int indx)const;
        friend  bool operator<(const PolarizationVector & a,const PolarizationVector & b);
    };
    inline bool operator<(const PolarizationVector & a,const PolarizationVector & b){
       return a.name < b.name;
    }
}
#endif // POLARIZATIONVECTOR_HPP
