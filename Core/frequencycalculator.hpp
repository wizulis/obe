#ifndef FREQUENCYCALCULATOR_HPP
#define FREQUENCYCALCULATOR_HPP

#include "typedefinitions.hpp"
#include "rho.hpp"

namespace OBE{

    class AssigmentBase;

    class FrequencyCalculator{

    protected:
        dbl B;
        const AssigmentBase * asg;
        Rho::LEVEL ex;
        Rho::LEVEL gr;

    public:

        FrequencyCalculator(const AssigmentBase * asg_ptr):asg(asg_ptr),ex(Rho::LEVEL::EXITED),gr(Rho::LEVEL::GROUND) {}

        virtual dbl GetEnergie(const Rho::State &)const=0;
        dbl Frequency(const Rho::State &,const Rho::State &) const;
        virtual dbl MixingCoeficient(const Rho::State &,const Rho::State &)const =0;
        virtual void SetB(const dbl val){ B=val;}

        dbl GetB()const { return B; }

        void UpdateLevels();
        virtual ~FrequencyCalculator(){}
    };
}

#endif // FREQUENCYCALCULATOR_HPP
