#ifndef ASSIGMENT_H
#define ASSIGMENT_H
#include <map>
#include <vector>
#include <iostream>
#include <fstream>
#include <stdexcept>
#include <memory>
#include "assigmentbase.hpp"
#include "results.hpp"
#include <boost/any.hpp>
#include <mongo/client/dbclient.h>


namespace OBE{

    class Assigment: public AssigmentBase {

public:
        bool isLoaded=false;
        std::string Marker;
        std::shared_ptr<Results> res;
        Assigment(){}

        Assigment & SetModification(vec<std::string> mods);
        Assigment & SetExperiment(std::string calc_def);

        Assigment & SetRabi(dbl R);
        Assigment & SetB(dbl B);
        Assigment & SetOffset(dbl off);
        Assigment & SetDopplerP(dbl dopplerFrom,dbl dopplerTo,dbl step);


        const std::shared_ptr<Results> & GetResult() const ;
        void SetResult(std::shared_ptr<Results> &a);


        bool Run();

        bool SendResults() const;
        void Load();

    protected:
        std::string calc_id;
        std::string calc_name;
        vec<std::string> mods;

    };

}
#endif // ASSIGMENT_H
