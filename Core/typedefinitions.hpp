#ifndef TYPEDEFINITIONS_HPP
#define TYPEDEFINITIONS_HPP

#include <complex>
#include <vector>
#include <gsl/gsl_const_mksa.h>
#include <boost/any.hpp>
#include <boost/optional.hpp>
#include <map>

namespace OBE{

    using dbl=double;
    using cdbl=std::complex<dbl>;



    template<typename T>
    class myVec:public std::vector<T>{
    public:
        myVec():std::vector<T>(){}
        myVec(int i):std::vector<T>(i){}
        myVec(int i,const T & val):std::vector<T>(i,val){}
        myVec(std::initializer_list<T> list):std::vector<T>(list){}
        T & operator[] (int pos){
            return std::vector<T>::at(pos);
        }
        constexpr const T & operator[](int pos)const{
            return std::vector<T>::at(pos);
        }

    };


    template<typename T>using vec=myVec<T>;
    //template<typename T>using vec=std::vector<T>;

    using cvec=vec<cdbl>;


    template<class T>
    std::ostream & operator<<(std::ostream & o, const vec<T> &  v){
        o <<"[";
        for(int i=0;i<v.size();i++){
            if(i!=0)
                o<<",";
            o <<v[i];
        }
        o <<"]";
        return o;
    }


    const dbl ELECTRON_CHARGE = GSL_CONST_MKSA_ELECTRON_CHARGE;
    const dbl BOHR_RADIUS = GSL_CONST_MKSA_BOHR_RADIUS;
    const dbl HBAR = GSL_CONST_MKSA_PLANCKS_CONSTANT_HBAR;
    const dbl HPLANK = GSL_CONST_MKSA_PLANCKS_CONSTANT_H;
    const dbl LSPEED = GSL_CONST_MKSA_SPEED_OF_LIGHT;
    const dbl MOL_R = GSL_CONST_MKSA_MOLAR_GAS;
    const dbl MBOHR = GSL_CONST_MKSA_BOHR_MAGNETON;
    const dbl SIGMA_COEF=2.35482;     // Simple math for FWHM->Sigma ;
    const dbl AVERAGE_GAUSS=0.721348; //Coeficient to move from average (within cylindrical gausian up to FWHM) to peak value

    const std::string MONGO_HOST="localhost:27017";
    const std::string MONGO_USER="alkali";
    const std::string MONGO_PASS="alkalipassword";

}
#endif // TYPEDEFINITIONS_HPP
