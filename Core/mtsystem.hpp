#ifndef MTSYSTEM_HPP
#define MTSYSTEM_HPP

#include "basicsystem.hpp"

namespace OBE {

    class AssigmentBase;

    class MTSystem: public BasicSystem{

    public:
            MTSystem(const AssigmentBase * parent,const DBContainer &p);


            void Calculate(const dbl doppler_from, const dbl doppler_to,const dbl doppler_step) override;


    protected:
            std::map<double,std::pair<double,double>> SetupLaserProfile(int reg,vec<dbl> rad_copy);

    };
}

#endif // MTSYSTEM_HPP
