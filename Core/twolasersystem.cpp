#include "twolasersystem.hpp"
#include "assigmentbase.hpp"
#include "twogroundlaser.hpp"

namespace OBE{
    TwoLaserSystem::TwoLaserSystem(const AssigmentBase *parent,const DBContainer & p):BasicSystem(parent,p)
    {

    }
    vec<std::unique_ptr<Equation_Matrix>> TwoLaserSystem::GenerateMatrix(int reg) const{

        TwoGroundLaser* generator=dynamic_cast<TwoGroundLaser*>(asg->generator.get());
        assert(generator!=nullptr);

        auto rho=asg->generator->GetRho();
        vec<std::unique_ptr<Equation_Matrix> > ret;

        int sub_regs=generator->GetSubAsg().model.regions;

        for(int i=0;i<reg*(sub_regs+1);i++)
            ret.push_back(std::unique_ptr<Equation_Matrix>(new UMFPACK_Matrix));


        vec<Rho::LEVEL> lvl_vec{Rho::LEVEL::GROUND,Rho::LEVEL::EXITED,Rho::LEVEL::EXITED_2};

        for(Rho::LEVEL lvl_i : lvl_vec ){
            for(Rho::State i:rho->ref(lvl_i)){
                for(Rho::LEVEL lvl_j : lvl_vec ){

                   if(lvl_i!=lvl_j && !asg->generator->IsOCSupported())
                        continue;
                   for(Rho::State j:rho->ref(lvl_j)){

                        auto tmp=asg->generator->CreateRow(i,j,*rho);
                        if(tmp.size()==reg*(sub_regs+1))
                            for(std::size_t i=0;i<reg*(sub_regs+1);i++)
                                ret[i]->AddRow(tmp[i]);
                        else{
                            for(std::size_t i=0;i<reg;i++)
                                for(std::size_t j=0;j<sub_regs;j++)
                                    ret[(sub_regs+1)*i+1+j]->AddRow(tmp[i*sub_regs+j]);
                        }
                    }
                }

            }
        }
        return ret;
    }

    std::map<dbl,vec<dbl> > TwoLaserSystem::Solve(vec<std::unique_ptr<Equation_Matrix> > &matrixVector, const vec<dbl> &xvec) const{

       TwoGroundLaser* generator=dynamic_cast<TwoGroundLaser*>(asg->generator.get());
       assert(generator!=nullptr);


       int sub_regs=generator->GetSubAsg().model.regions;

       std::map<dbl,vec<dbl> > ret;

       OBE::vec<std::unique_ptr<Rho>> rho_vec;

       auto rho=asg->generator->GetRho();
       rho->AcquireMemory();
       asg->generator->InitDensityMatrix(*rho);

       cvec BVec;
       int base_size=std::pow(rho->LevelSize(Rho::LEVEL::GROUND),2)+std::pow(rho->LevelSize(Rho::LEVEL::EXITED),2);

       for(int i=0;i<matrixVector.size();i+=sub_regs+1){
           if(i==0)
               asg->generator->FillRHVec(*rho,BVec,i);
           else
               asg->generator->FillRHVec(*rho_vec.back(),BVec,i);

           BVec.resize(base_size);
           matrixVector[i]->Solve(rho->DataHandler(),BVec);
           std::fill(rho->DataHandler().begin()+base_size,rho->DataHandler().end(),cdbl{0,0});
           rho_vec.push_back(std::move(rho));
           rho=asg->generator->GetRho();
           rho->AcquireMemory();
       }


//       asg->generator->InitDensityMatrix(*rho);
       vec<dbl> coef=generator->GetRadiusVec();

       auto & sub_gen=generator->GetSubAsg().generator;
       for(int i=0;i<xvec.size();i++){
           vec<dbl> fl;
           for(int j=0;j<sub_regs;j++){
               if(j==0)
                   sub_gen->FillRHVec(*rho_vec[i],BVec,j);
               else
                   sub_gen->FillRHVec(*rho,BVec,j);

               matrixVector[i*(sub_regs+1)+1+j]->Solve(rho->DataHandler(),BVec);
               auto fluro=asg->generator->CalculateFluorescence(*rho);
               if(fl.size()!= fluro.size())
                   fl.resize(fluro.size(),0);
               for(int m=0;m<fl.size();m++){
                    fl[m]+=fluro[m]*std::abs(coef[j]);
               }
           }

	   /*           auto sum=0.0;
           for(auto k: rho->Pop())
              for(auto m:k)
                  sum+=m;
           std::cout << sum << std::endl;
           getchar();*/
         //  std::cout << fl << std::endl;
           ret[xvec[i]]=fl;

       }

       return ret;
    }
}
