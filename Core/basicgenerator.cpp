#include "basicgenerator.hpp"
#include <gsl/gsl_sf_coupling.h>
#include "assigment.hpp"


namespace OBE {

  double BasicGenerator::ColisionRateE()const{
    return ColisionRate();
  }
    BasicGenerator::BasicGenerator(const AssigmentBase * assig, const DBContainer &p):asg(assig),transit_speed(-1){

    }

    void BasicGenerator::SetDopler(const vec<dbl> &dop){
        dopler_vec=dop;
    }

    dbl BasicGenerator::GetDopplerStdDev() const {
        return std::sqrt(MOL_R*asg->model.temperature/asg->trans.Atom_mass);
    }

    void BasicGenerator::SetDopplerSpeed(const dbl val){
        dopler_speed=val;
    }

    dbl BasicGenerator::DoplerShift()const {
        return dopler_speed*asg->model.laser_freq/LSPEED;
    }

    dbl BasicGenerator::GetSpeedProbability(const dbl speed) const {
        return std::sqrt(asg->trans.Atom_mass/2/M_PI/MOL_R/asg->model.temperature)*
                std::exp(-asg->trans.Atom_mass*speed*speed/2/MOL_R/asg->model.temperature);
    }
    dbl BasicGenerator::GetTransitProbability(const dbl speed) const {
        return asg->trans.Atom_mass/MOL_R/asg->model.temperature*speed*std::exp(-asg->trans.Atom_mass*speed*speed/2/MOL_R/asg->model.temperature);
    }

    void BasicGenerator::SetLaserIntensities(const vec<dbl> &laser){
        laser_intensity=laser;
    }

    vec<dbl> BasicGenerator::CalculateFluorescence(const Rho & rho) const {


        vec<dbl> ret;
        ret.reserve(asg->fluroescence_polar_list.size());
        for(const auto &vec: asg->fluroescence_polar_list){
            cdbl sum(0,0);
            for(State r:rho.ref(Exited()))
                for(State s:rho.ref(Exited()))
                    for(auto m:rho.ref(Ground()))
                        sum+=TransitionAmplitude(m,r,vec)*TransitionAmplitude(s,m,vec)*rho(r,s);

            ret.push_back(sum.real()/pow(asg->trans.Reduced_matrix_elem,2)*asg->trans.Spont_rate);
        }

        return ret;
    }

    vec<cvec> BasicGenerator::CreateRow(const State & i, const State & j,const Rho & rho) const{

        if(i.lvl==Ground() && j.lvl==Ground())
            return CreateGroundRow(i,j,rho);
        else if(i.lvl==Exited() && j.lvl==Exited())
            return CreateExitedRow(i,j,rho);
        else
            throw std::runtime_error("Optical coherences are not found in BasicGenerator");
    }

    dbl BasicGenerator::TransitionFrequency(const State &i, const State &j) const{

        return asg->trans.Energy_calc->Frequency(i,j);
    }

    cdbl BasicGenerator::TransitionAmplitude(const State &i, const State &j, const PolarizationVector &vec) const{

        int Jp=asg->trans.J_exited;
        int I=asg->trans.Nuclear_spin;
        int J=asg->trans.J_ground;


        if(i.lvl==Exited())
        {
            cdbl tmp(0,0);
            for(int q=-1;q<=1;q++)
                tmp+=vec[q]*gsl_sf_coupling_3j(i.F,2,j.F,-i.mF,2*q,j.mF);

            tmp*=pow(-1,(i.F-i.mF+Jp+I+j.F+2+Jp-J)/2)
                  *sqrt((i.F+1)*(j.F+1))
                  *gsl_sf_coupling_6j(Jp,i.F,I,j.F,J,2)
                  *asg->trans.Reduced_matrix_elem;

            return tmp;
        }
        else if(i.lvl==Ground())
        {
            cdbl tmp(0,0);
            for(int q=-1;q<=1;q++)
                tmp+=std::conj(vec[q])*pow(-1,q)*gsl_sf_coupling_3j(i.F,2,j.F,-i.mF,-2*q,j.mF);

            tmp*=pow(-1,(i.F-i.mF+J+I+j.F+2)/2)
                  *sqrt((i.F+1)*(j.F+1))
                  *gsl_sf_coupling_6j(J,i.F,I,j.F,Jp,2)
                  *asg->trans.Reduced_matrix_elem;
            return tmp;
        }else{
            throw std::runtime_error("Incorrect level");
        }
    }

    dbl BasicGenerator::Gamma(const State & a, const State & b, const State & c, const State & d) const{

        int Jp=asg->trans.J_exited;
        int I=asg->trans.Nuclear_spin;
        int J=asg->trans.J_ground;

        dbl tmp=(Jp+1)*asg->trans.Spont_rate*
                pow(-1,(a.F-a.mF+c.F-c.mF+2*Jp+2*I+b.F+d.F+4)/2)*
                sqrt((a.F+1)*(b.F+1)*(c.F+1)*(d.F+1))*
                gsl_sf_coupling_6j(J,a.F,I,b.F,Jp,2)*
                gsl_sf_coupling_6j(Jp,c.F,I,d.F,J,2);

        dbl sum=0;
        for(int q=-1; q<2; q++){
            sum+=pow(-1,q)*gsl_sf_coupling_3j(a.F,2,b.F,-a.mF,2*q,b.mF)
                 *gsl_sf_coupling_3j(c.F,2,d.F,-c.mF,-2*q,d.mF);
        }

        return tmp*sum;
    }

    dbl BasicGenerator::ColisionRate()const{
        return asg->model.colision_prob;
    }

    dbl BasicGenerator::TransitRate(int i)const{
        return transit_rate[i];
    }

    dbl BasicGenerator::TransitSpeed()const{
        if(transit_speed>0)
            return transit_speed;
        return  sqrt(M_PI*MOL_R*asg->model.temperature/asg->trans.Atom_mass/2);
    }

    void BasicGenerator::SetTransitSpeed(const dbl speed){
        for(auto & i:transit_rate)
            i*=speed/transit_speed;
        transit_speed=speed;
    }

    const vec<dbl> & BasicGenerator::TransitRateVector(const vec<dbl> & regionLength){
        if(regionLength.size()>0){

            transit_rate.clear();

            for(dbl regLen:regionLength){
                transit_rate.push_back(
                            asg->model.trans_rate_coef/regLen*TransitSpeed()
                            );
            }

        }
        return transit_rate;
    }


    void BasicGenerator::FillRHVec(const Rho & r,cvec & bvec,int region) const{

        std::size_t vecSize=r.MatrixSize();

        if(bvec.size()!=vecSize)
            bvec.resize(vecSize,0);
        std::fill(bvec.begin(),bvec.end(),0);

        int groundSize=r.LevelSize(Ground());

        for(State i:r.ref(Ground()))
            bvec[r.index(i,i)]-=ColisionRate()/groundSize;

        cvec rhoData=r.DataHandler();

        for(std::size_t i=0;i<vecSize;i++){
            bvec[i]-=rhoData[i]*TransitRate(region);

	}

    }
    void BasicGenerator::InitDensityMatrix(Rho &r) const{
        vec<cdbl> & ref=r.DataHandler();
        std::fill(ref.begin(),ref.end(),cdbl(0,0));


        cdbl initPop=1.0/r.LevelSize(Ground());
        for(State i:r.ref(Ground()))
            r(i,i)=initPop;
    }



    vec<cvec> BasicGenerator::CreateGroundRow(const State &i , const State & j,const Rho & rho) const{

        int lvlSize=rho.MatrixSize();

        vec<cvec> row(
                    laser_intensity.size(),
                    cvec(lvlSize,0)
                    );
        //First sum
        for(State k: rho.ref(Exited())){
            for(State m: rho.ref(Exited())){

                cdbl transAmp=TransitionAmplitude(i,k,asg->laser_polarization)
                        *TransitionAmplitude(m,j,asg->laser_polarization);

                for(std::size_t dal=0; dal<row.size(); dal++){
                    cdbl o(1,0);
                    cdbl x((asg->trans.Spont_rate+asg->model.laser_spectral_width+ColisionRateE())/2,
                           asg->model.laser_freq-DoplerShift()-TransitionFrequency(m,i));
                    cdbl y(x.real(),
                           -(asg->model.laser_freq-DoplerShift()-TransitionFrequency(k,j)));

                    x=o/x;
                    y=o/y;

                    x=x+y;

                    row[dal][rho.index(k,m)]+=x*transAmp*laser_intensity[dal]/HBAR/HBAR;
                }
            }
        }//END First sum

        //Second and Third sum
        for(State k: rho.ref(Exited())){
            for(State m: rho.ref(Ground())){

                cdbl transAmp1=TransitionAmplitude(i,k,asg->laser_polarization)
                        *TransitionAmplitude(k,m,asg->laser_polarization);

                cdbl transAmp2=TransitionAmplitude(m,k,asg->laser_polarization)
                        *TransitionAmplitude(k,j,asg->laser_polarization);

                for(std::size_t dal=0; dal<row.size(); dal++){
                    cdbl o(1,0);
                    cdbl x((asg->trans.Spont_rate+asg->model.laser_spectral_width+ColisionRateE())/2,
                           -(asg->model.laser_freq-DoplerShift()-TransitionFrequency(k,j)));
                    cdbl y(x.real(),
                           (asg->model.laser_freq-DoplerShift()-TransitionFrequency(k,i)));

                    x=o/x;
                    y=o/y;

                    row[dal][rho.index(m,j)]-=x*transAmp1*laser_intensity[dal]/HBAR/HBAR;
                    row[dal][rho.index(i,m)]-=y*transAmp2*laser_intensity[dal]/HBAR/HBAR;
                }
            }
        }//End Second and Third sum



        //Spontanius decay
        for(State k: rho.ref(Exited())){
            for(State m: rho.ref(Exited())){

                cdbl tmpV=cdbl(Gamma(i,k,m,j),0);
                for(std::size_t dal=0; dal<row.size(); dal++)
                    row[dal][rho.index(k,m)]+= tmpV;
            }
        }

        //Groud state spliting,transit rate and colision decay
        int idx=rho.index(i,j);
        for(std::size_t dal=0;dal<row.size();dal++){

            cdbl & elem=row[dal][idx];

            elem-=cdbl(0,TransitionFrequency(i,j));
            //Collisition relaxation
            elem-=ColisionRate();
            //Trans rate decay
            elem-=TransitRate(dal);
        }

        return row;
    }
    vec<cvec> BasicGenerator::CreateExitedRow(const State &i , const State & j,const Rho & rho) const{

        int lvlSize=rho.MatrixSize();

        vec<cvec> row(
                    laser_intensity.size(),
                    cvec(lvlSize,0)
                    );

        //First sum
        for(State k: rho.ref(Ground())){
            for(State m: rho.ref(Ground())){

                cdbl transAmp=TransitionAmplitude(i,k,asg->laser_polarization)
                        *TransitionAmplitude(m,j,asg->laser_polarization);

                for(std::size_t dal=0; dal<row.size(); dal++){
                    cdbl o(1,0);
                    cdbl x((asg->trans.Spont_rate+asg->model.laser_spectral_width+ColisionRateE())/2,
                           -(asg->model.laser_freq-DoplerShift()-TransitionFrequency(i,m)));
                    cdbl y(x.real(),
                           (asg->model.laser_freq-DoplerShift()-TransitionFrequency(j,k)));

                    x=o/x;
                    y=o/y;

                    row[dal][rho.index(k,m)]+=(x+y)*transAmp*laser_intensity[dal]/HBAR/HBAR;
                }
            }
        }//END First sum

        //Second and Third sum
        for(State k: rho.ref(Ground())){
            for(State m: rho.ref(Exited())){

                cdbl transAmp1=TransitionAmplitude(i,k,asg->laser_polarization)
                        *TransitionAmplitude(k,m,asg->laser_polarization);

                cdbl transAmp2=TransitionAmplitude(m,k,asg->laser_polarization)
                        *TransitionAmplitude(k,j,asg->laser_polarization);

                for(std::size_t dal=0; dal<row.size(); dal++){
                    cdbl o(1,0);
                    cdbl x((asg->trans.Spont_rate+asg->model.laser_spectral_width+ColisionRateE())/2,
                           (asg->model.laser_freq-DoplerShift()-TransitionFrequency(j,k)));
                    cdbl y(x.real(),
                           -(asg->model.laser_freq-DoplerShift()-TransitionFrequency(i,k)));

                    x=o/x;
                    y=o/y;

                    row[dal][rho.index(m,j)]-=x*transAmp1*laser_intensity[dal]/HBAR/HBAR;
                    row[dal][rho.index(i,m)]-=y*transAmp2*laser_intensity[dal]/HBAR/HBAR;
                }
            }
        }//End Second and Third sum


        //State spliting,transit rate and colision decay
        int idx=rho.index(i,j);
        for(std::size_t dal=0;dal<row.size();dal++){

            cdbl & elem=row[dal][idx];

            elem-=cdbl(0,TransitionFrequency(i,j));
            //Collisition relaxation
            elem-=ColisionRate();
            //Trans rate decay
            elem-=TransitRate(dal);
            //Spontanious decay
            elem-=asg->trans.Spont_rate;
        }

        return row;
    }

    std::unique_ptr<Rho> BasicGenerator::GetRho() const{
        return std::unique_ptr<Rho>(new Rho(asg->trans));
    }
}
