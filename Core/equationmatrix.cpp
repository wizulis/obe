#include "equationmatrix.hpp"
#include "suitesparse/umfpack.h"
#include <iostream>

namespace OBE{

    UMFPACK_Matrix::UMFPACK_Matrix():Equation_Matrix(0){Reset();}

    void UMFPACK_Matrix::AddRow(const cvec &tmp){
        //Store location of start of row
        if(rowCounter == Ap.size())
          Ap.push_back(count);
        else
          Ap[rowCounter]=count;

        rowCounter++;

        for(std::size_t i=0;i<tmp.size();i++){

            if( tmp[i] != cdbl(0,0)){
                //Store value
                if(A.size()==elementCounter)
                    A.push_back(tmp[i]);
                else
                    A[elementCounter]=tmp[i];

                elementCounter++;

                //Store location of column
                if(Ai.size()==columnCounter)
                    Ai.push_back(i);
                else
                    Ai[columnCounter]=i;

                columnCounter++;

                count++;
              }
        }
    }


    void UMFPACK_Matrix::Solve(cvec & X,const cvec & Bvec){


        if(Bvec.size()==Ap.size()){
            if(rowCounter==Ap.size())
              Ap.push_back(count);
            else
              Ap[rowCounter]=count;
        }

        if(Bvec.size()!=rowCounter)
          throw std::runtime_error("UMFPACK matrix size and B vector size are not equal");


        int status;

        status= umfpack_zi_symbolic (Bvec.size(), Bvec.size(), &Ap[0], &Ai[0],(double*) &A[0],0, &Symbolic, 0,0) ;

        if(status!=UMFPACK_OK)
        {
          std::stringstream tmp;
          tmp << "umfpack_zi_symbolic failed with Error Code " << status;
          tmp << "\n" << Ap.size() << "\t" << Ai.size() << "\t" << Bvec.size();
          throw std::runtime_error(tmp.str());
        }


        status= umfpack_zi_numeric (&Ap[0], &Ai[0], (double*)&A[0],0, Symbolic, &Numeric,0, 0) ;
        if(status!=UMFPACK_OK)
        {
          std::stringstream tmp;
          tmp << "umfpack_zi_numeric failed with Error Code " << status;
          throw std::runtime_error(tmp.str());
        }

        double Info [UMFPACK_INFO];
        //Solves row mayor format
        status = umfpack_zi_solve (UMFPACK_Aat, &Ap[0], &Ai[0], (double*)&A[0], 0,(double*)(X.data()),0 ,(double*)Bvec.data(),0, Numeric, 0, Info);
        if(status!=UMFPACK_OK)
        {
          std::stringstream tmp;
          tmp << "umfpack_zi_solve failed with Error Code " << status;
          throw std::runtime_error(tmp.str());
        }
//        std::cout << "Error Omega: " << Info[UMFPACK_OMEGA1] << " " << Info[UMFPACK_OMEGA2] << std::endl;
        umfpack_zi_free_symbolic(&Symbolic);
        umfpack_zi_free_numeric(&Numeric);
    }

    void UMFPACK_Matrix::Reset(){
        count=0;
        elementCounter=0;
        columnCounter=0;
        rowCounter=0;
        bvalCounter=0;
        A.clear();
        Ai.clear();
        Ap.clear();
    }
    void UMFPACK_Matrix::print(std::ostream & os){
        for(int i=0;i<Ap.size()-1;i++){
            auto colidx=0;
            for(int j=0;j<Ap.size()-1;j++){
                if(Ai[Ap[i]+colidx]==j){
                    os << A[Ap[i]] << "\t";
                    colidx+=1;
                }
                else
                    os << 0 << "\t";
            }
            os << "\n";
        }
    }

    SOR_Matrix::SOR_Matrix():Equation_Matrix(0){
          Ap.reserve(1000);
          A.reserve(30000);
          Ai.reserve(30000);
      }

      void SOR_Matrix::AddRow(const cvec & v){
          int start=A.size();

          for(int i=0;i<v.size();i++){
              if(v[i]!=0.0){
                  A.push_back(v[i]);
                  Ai.push_back(i);
              }
          }
          int end=A.size();
          Ap.push_back(std::make_pair(start,end));

      }

      void SOR_Matrix::Solve(cvec & x,const cvec & b){

          dbl error=0;
          cvec last=x;

          do{

              last=x;

              for(uint itr=0;itr<20;itr++){
                  for(int row=0;row<Ap.size()-1;row++){
                      cdbl val=0;
                      cdbl Aval=0;
                      auto ind=Ap[row];
                      for(int col=ind.first;col<ind.second;col++){
                          if(Ai[col]==row){
                              Aval=A[col];
                          }else{
                              val+=x[Ai[col]]*A[col];
                          }
                      }
                      x[row]=(1-2.0/3.0)*x[row]+2.0/3.0*(b[row]-val)/Aval;
                  }
              }
              error=0;
              for(int i=0;i<last.size();i++){
                  auto er=std::abs(last[i]-x[i])/std::abs(last[i]+x[i]);
                  if(er>error)
                      error=er;
              }
          }while(error>1e-8);
      }


      void SOR_Matrix::Reset(){
          Ai.clear();
          A.clear();
          Ap.clear();
      }
      void SOR_Matrix::print(std::ostream & os){

      }

      ARMA_Matrix::ARMA_Matrix(int size):Equation_Matrix(size),data(size,size,arma::fill::zeros),row_id(0){

      }

      void ARMA_Matrix::AddRow(const cvec &v){
        for(int i=0;i<v.size();i++)
            data(row_id,i)=v[i];
        row_id++;
      }

      void ARMA_Matrix::Solve(cvec & ret, const cvec & bvec){

          auto vec = arma::conv_to<arma::cx_vec>::from(static_cast<const std::vector<std::complex<double>>&>(bvec));

          arma::cx_vec sol=arma::solve(data,vec);
          ret.resize(sol.n_cols);
          for(int i=0;i<sol.n_cols;i++)
              ret[i]=sol(i);
      }
      void ARMA_Matrix::Reset(){
          row_id=0;
      }

      void ARMA_Matrix::print(std::ostream & os) {
          os << data;
      }


}
