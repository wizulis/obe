#include "polarizationvector.hpp"
#include <boost/algorithm/string.hpp>

namespace OBE {

    PolarizationVector::PolarizationVector():elems{cdbl(0),cdbl(0),cdbl(0)}{

    }

    PolarizationVector & PolarizationVector::operator = (const PolarizationVector & other){
        name=other.name;
        for(int i=0;i<3;i++)
            elems[i]=other.elems[i];
        return *this;
    }

    const std::string & PolarizationVector::GetName()const{
        return name;
    }

    PolarizationVector PolarizationVector::FromDBContainer(const DBContainer & laser_pol){

        auto tmpName=laser_pol.get<std::string>("name");

        auto x_opt=laser_pol.get_opt<std::string>("x");
        if(!x_opt){

            // Not x,y,z format - falling back to q_-1,q_0,q_1 format
            cdbl qm1={0,0},q0={1,0},q1{0,0};
/*            std::string polField_t;

            polField_t=laser_pol.getField("q_m1").toString(false,false);
            boost::erase_all(polField_t,"\"");
            std::istringstream(polField_t)>>qm1;

            polField_t=laser_pol.getField("q_0").toString(false,false);
            boost::erase_all(polField_t,"\"");
            std::istringstream(polField_t)>>q0;

            polField_t=laser_pol.getField("q_1").toString(false,false);
            boost::erase_all(polField_t,"\"");
            std::istringstream(polField_t)>>q1;

	    //	    	    std::cout << qm1 << q0 << q1 << std::endl;
*/
            return FromSpherical(qm1,q0,q1,tmpName);
        }else{
            cdbl x,y,z;
            std::stringstream(x_opt.get()) >> x;
            std::stringstream(laser_pol.get<std::string>("y")) >> y;
            std::stringstream(laser_pol.get<std::string>("z")) >> z;
            return FromDecart(x,y,z,tmpName);
        }

    }

    PolarizationVector PolarizationVector::FromSpherical(const cdbl &qm1, const cdbl &q0, const cdbl &q1,const std::string & nm){
        PolarizationVector ret;
        ret.initFromSpherical(qm1,q0,q1);
        ret.name=nm;
        return ret;
    }

    PolarizationVector PolarizationVector::FromDecart(const cdbl &x, const cdbl &y, const cdbl &z,const std::string & nm){
        PolarizationVector ret;
        ret.initFromXYZ(x,y,z);
        ret.name=nm;
        return ret;
    }

    void PolarizationVector::initFromSpherical(const cdbl &qm1, const cdbl &q0, const cdbl &q1){

        elems[0]=qm1;
        elems[1]=q0;
        elems[2]=q1;
	//	std::cout << qm1 << q0 << q1 << std::endl;
    }

    void PolarizationVector::initFromXYZ(const cdbl & x,const cdbl & y,const cdbl & z){

        elems[0]=1.0/sqrt(2)*x+cdbl(0,1.0/sqrt(2))*y;
        elems[1]= z;
        elems[2]= -1.0/sqrt(2)*x+cdbl(1.0/sqrt(2))*y;

    }


    cdbl & PolarizationVector::operator [](const int idx){
        return elems[idx+1];
    }

    const cdbl & PolarizationVector::operator [](const int idx)const{
        return elems[idx+1];
    }
}
