#ifndef TMATRIX_HPP
#define TMATRIX_HPP

#include <vector>
#include <stdexcept>
#include "typedefinitions.hpp"
namespace OBE {

    template<typename T>
    class TMatrix{

    protected:
        vec<T> data;
        int rowC;
        int colC;

    public:

        TMatrix():rowC(0),colC(0){

        }

        TMatrix(int rows, int columns):data(rows*columns,0),rowC(rows),colC(columns){

        }

        T & operator ()(int row,int col){
            if(row<0 || row>=rowC || col <0 || col>=colC)
                throw std::runtime_error("Invalid index");
            return data[row*colC+col];
        }

        const T & operator ()(int row, int col)const{
            if(row<0 || row>=rowC || col <0 || col>=colC)
                throw std::runtime_error("Invalid index");
            return data[row*colC+col];
        }

        void Resize(int rows, int columns,const T & val=T()){
            data.resize(rows*columns,val);
            rowC=rows;
            colC=columns;
        }
        std::size_t size(int i=0)const{
            switch(i){
            case 0:
                return rowC;
            case 1:
                return colC;
            }
            throw std::runtime_error("Wrong matrix dimension");
        }

    };
    class Matrix:public TMatrix<dbl>{
    public:
        Matrix(){}
        Matrix(int rows,int columns):TMatrix<dbl>(rows,columns){}

        void EigenValues(vec<dbl> & eigen_val,Matrix & eigen_vec)const;
        void print();
    };

    inline cvec operator *(const TMatrix<cdbl> & m,const cvec v){
        if(m.size(1)!=v.size())
            throw std::runtime_error("Matrix and vector dimensions are not equal");
        cvec ret(v.size(),{0,0});
        for(std::size_t r=0;r<m.size(0);r++){
            for(std::size_t c=0;c<v.size();c++){
                ret[r]+=m(r,c)*v[c];
            }
        }

    }
}
#endif
