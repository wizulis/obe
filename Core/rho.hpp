#ifndef RHO_HPP
#define RHO_HPP

#include "typedefinitions.hpp"
#include <map>


namespace OBE {
    class Transition;
    class Rho
    {

    protected:
        Rho(vec<int> && sizes, vec<vec<int>> && angMap):levelSize(sizes),angMomMap(angMap){}
    public:
       enum  LEVEL:int {GROUND=0,EXITED,EXITED_2,SIZE};

       class RhoRef;
       struct State;

       Rho(const Transition & trans);

       virtual void AcquireMemory();
       virtual int index(const State & a,const State & b)const;
       virtual int MatrixSize()const;

       RhoRef ref(LEVEL lvl)const;


       cdbl & operator() (const State & a, const State & b);
       const cdbl & operator() (const State & a,const State &b) const;
       Rho & operator=(const Rho &);
       cvec & DataHandler();
       const cvec & DataHandler() const;

       int LevelSize(Rho::LEVEL lvl) const;
       vec<int> AngMom(Rho::LEVEL lvl) const;


       struct State{

            State(const int initF,const int initMf,const LEVEL initLvl,const Rho & p);

            const int F;
            const int mF;
            const LEVEL lvl;
            const Rho & parent;
            struct Iter{

                Iter(const Rho & par,const LEVEL l);

                Iter & operator ++ ();

                bool operator == (const Iter & other) const;
                bool operator != (const Iter & other) const;

                const State operator * () const;

                void toEnd();
            private:
                vec<int>::const_iterator  angMomStart,angMomEnd;
                const Rho & parent;
                const LEVEL lvl;
                int F;
                int mF;
            };

        };

        class RhoRef{

            const Rho & parent;
            LEVEL lvl;

        public:

            RhoRef(const Rho & par,const LEVEL l);

            State::Iter begin();
            State::Iter end();
        };

        friend State::Iter;
        vec<vec<dbl>> Pop()const;
    protected:
        vec<cdbl> data;
        vec<int> levelSize;
        vec<vec<int>>  angMomMap;

    };

    class RhoOC:public Rho{
    protected:

    public:
        using Rho::Rho;
        int index(const Rho::State & a, const Rho::State & b) const override;

        int MatrixSize() const override;
    };

    class Rho2L: public Rho{
        static vec<int> GetSizes(const Transition &,const Transition&);
        static vec<vec<int>> GetAngMomMap(const Transition &,const Transition&);
    public:
        Rho2L(const Transition &,const Transition &);

    };

    std::ostream & operator <<(std::ostream & ost,const Rho & r);
    std::ostream & operator <<(std::ostream & ost,const Rho::State & i);
}
#endif // RHO_HPP
