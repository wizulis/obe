#ifndef DBCONTAINER_HPP
#define DBCONTAINER_HPP

#include <map>
#include <boost/any.hpp>
#include <boost/optional.hpp>

#include <mongo/client/dbclient.h>
#include "typedefinitions.hpp"
namespace OBE{
    class DBContainer
    {
        typedef std::map<std::string,boost::any> Load_t;
        Load_t data;
    public:

        explicit DBContainer(){}
        explicit DBContainer(const mongo::BSONObj & obj)
        {

            for(auto i=obj.begin();i.more();){
                mongo::BSONElement elem=i.next();
                auto name=std::string(elem.fieldName());

                if(elem.isNumber()){
                    double num=elem.number(),tmp_val;
                    if(!std::modf(num,&tmp_val))
                        data[name]=static_cast<int>(num);
                    else
                        data[name]=num;
                }else if(elem.isABSONObj()){
                    data[name]=DBContainer(elem.Obj());
                }else if(elem.type()==mongo::BSONType::jstOID){
                    data[name]=elem.toString(false,false);
                }else{
                    data[name]=elem.String();
                }
            }
            data.erase("_id");

        }
        void add(const mongo::BSONObj & params){
            add(DBContainer(params));
        }
        void add(const DBContainer & params){
            for(auto & i:params.data){
                data[i.first]=i.second;
            }
        }
        template<typename T> boost::optional<T> get_opt(const std::string & field)const;

        template<typename T> T get(const std::string & field)const{
            auto val=get_opt<T>(field);
            if(!val){
                std::stringstream str;
                str << "Unable to find value " << field;
                throw std::runtime_error(str.str());
            }
            return val.get();
        }

        template<typename T> bool fill(T & param, const std::string &field)const{
            if(auto val=get_opt<T>(field)){
                param=val.get();
                return true;
            }else{
                return false;
            }
        }

        Load_t::const_iterator begin()const{
            return data.cbegin();
        }

        Load_t::const_iterator end()const{
            return data.cend();
        }
        int erase(const std::string & key){
            return data.erase(key);
        }
    };

    template<> inline boost::optional<dbl> DBContainer::get_opt(const std::string & field) const {
        auto itr=data.find(field);
        if(itr==data.end())
            return boost::optional<dbl>{};
        if(typeid(dbl)!=itr->second.type() && typeid(int)!=itr->second.type()){
            std::stringstream str;
            str << "Incorrect type ( " << typeid(double).name() <<  " ) for " << field << " ( " << itr->second.type().name() << " )";
            throw std::runtime_error(str.str());
        }else if(typeid(dbl)==itr->second.type()){
            return boost::optional<dbl>{boost::any_cast<dbl>(itr->second)};
        }else {
            return boost::optional<dbl>{static_cast<dbl>(boost::any_cast<int>(itr->second))};
        }
    }

    template<> inline boost::optional<std::string> DBContainer::get_opt(const std::string & field) const {
        auto itr=data.find(field);
        if(itr==data.end())
            return boost::optional<std::string>{};
        if(typeid(dbl)!=itr->second.type() && typeid(int)!=itr->second.type() && typeid(std::string)!=itr->second.type()){
            std::stringstream str;
            str << "Incorrect type ( " << typeid(double).name() <<  " ) for " << field << " ( " << itr->second.type().name() << " )";
            throw std::runtime_error(str.str());
        }else if(typeid(dbl)==itr->second.type()){
            return boost::optional<std::string>{std::to_string(boost::any_cast<dbl>(itr->second))};
        }else if(typeid(int)==itr->second.type()){
            return boost::optional<std::string>{std::to_string(boost::any_cast<int>(itr->second))};
        }else{
            return boost::optional<std::string>{boost::any_cast<std::string>(itr->second)};
        }
    }
    template<typename T> boost::optional<T> DBContainer::get_opt(const std::string & field)const{
        auto itr=data.find(field);
        if(itr==data.end())
            return boost::optional<T>{};
        if(typeid(T)!=itr->second.type()){
            std::stringstream str;
            str << "Incorrect type ( " << typeid(T).name() <<  " ) for " << field << " ( " << itr->second.type().name() << " )";
            throw std::runtime_error(str.str());
        }else{
            return boost::optional<T>{boost::any_cast<T>(itr->second)};
        }
    }
}

#endif // DBCONTAINER_HPP
