#include "numericsystem.hpp"
#include <armadillo>
#include "assigment.hpp"
//#include

namespace OBE{
    vec<std::unique_ptr<Equation_Matrix> > NumericSystem::GenerateMatrix(int reg)const{


        auto rho=asg->generator->GetRho();
        vec<std::unique_ptr<Equation_Matrix> > ret;

        auto size=rho->MatrixSize();
        for(int i=0;i<reg;i++)
            ret.push_back(std::unique_ptr<Equation_Matrix>(new ARMA_Matrix(size)));


        vec<Rho::LEVEL> lvl_vec{asg->generator->Ground(),asg->generator->Exited()};


        for(Rho::LEVEL lvl_i : lvl_vec ){
            for(Rho::State i:rho->ref(lvl_i)){
                for(Rho::LEVEL lvl_j : lvl_vec ){

                   if(lvl_i!=lvl_j && !asg->generator->IsOCSupported())
                        continue;
                   for(Rho::State j:rho->ref(lvl_j)){
                        auto tmp=asg->generator->CreateRow(i,j,*rho);
                        for(std::size_t i=0;i<tmp.size();i++)
                            ret[i]->AddRow(tmp[i]);
                    }
                }

            }
        }
        return ret;
    }

    std::map<dbl,vec<dbl>> NumericSystem::Solve(vec<std::unique_ptr<Equation_Matrix> > & matrixVector, const vec<dbl> & xvec) const{



        auto time_step=1/asg->generator->TransitSpeed()*std::abs(xvec[1]-xvec[0]);

        std::map<dbl,vec<dbl> > ret;
        auto rho=asg->generator->GetRho();
        rho->AcquireMemory();
        asg->generator->InitDensityMatrix(*rho);

        std::size_t s=rho->DataHandler().size();

        arma::cx_vec rho_p=arma::conv_to<arma::cx_vec>::from(static_cast<std::vector<cdbl>&>(rho->DataHandler()));

        arma::cx_mat ident(s,s,arma::fill::eye),upper(s,s),lower(s,s);

        for(int i=0;i<matrixVector.size();i++){
            arma::cx_mat & last=dynamic_cast<ARMA_Matrix&>(*matrixVector[i==0?0:i-1]);
            arma::cx_mat & now=dynamic_cast<ARMA_Matrix&>(*matrixVector[i]);

            lower=arma::inv(ident-now*(time_step/2));
            upper=ident+last*(time_step/2);
            rho_p=(lower)*(upper*rho_p);
            //rho_p=rho_p+now*rho_p*time_step;
            for(int j=0;j<rho_p.n_rows;j++)
                rho->DataHandler()[j]=rho_p(j);
            //std::cout << "nc " << rho_p.n_rows << "\n\n" << *rho <<std::endl;
            auto f=asg->generator->CalculateFluorescence(*rho);
	    //std::cout << rho->Pop() << std::endl;
            ret[xvec[i]]=std::move(f);
            //getchar();
        }

        return ret;
    }
}
