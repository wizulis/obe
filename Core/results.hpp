#ifndef RESULTS_HPP
#define RESULTS_HPP

#include "typedefinitions.hpp"

namespace OBE {
    struct Results
    {
        dbl Rabi,B,Offset;
        int Regions;

        struct Point{
            dbl x,y,r,S;
            vec<dbl> fluorescence;

        };

        Results(){
            data.reserve(20000);
        }
        void AddPoint(dbl x,dbl y,dbl r, dbl S,const vec<dbl> & fluor);
        vec<Point> data;

        vec<dbl> GetFullFluorescence() const;
        vec<Point> GetFluorDistribution() const;
    };
}
#endif // RESULTS_HPP
