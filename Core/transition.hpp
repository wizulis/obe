#ifndef ELEMENT_HPP
#define ELEMENT_HPP
#include <mongo/client/dbclient.h>

#include "typedefinitions.hpp"
#include "frequencycalculator.hpp"


namespace OBE{
    struct Transition
    {
        std::string element;

        int Nuclear_spin;
        dbl Atom_mass;
        int J_ground;
        int J_exited;
        int L_ground;
        int L_exited;
        dbl Lande_J_exited;
        dbl Spont_rate;
        dbl Trans_freq;
        dbl Reduced_matrix_elem;
        dbl Lande_J;
        dbl Lande_nuclear;
        std::map<int,dbl> Ground_freq;
        std::map<int,dbl> Exited_freq;

        vec<vec<int>> Angular_moment;
        vec<int> Level_sizes;

        std::unique_ptr<FrequencyCalculator> Energy_calc;

        Transition()=default;

        Transition(const Transition &)=delete;
        Transition & operator=(Transition && other);
        Transition(const std::string & elem,const std::string & transition);

        void RemoveLevel(const std::string & lvl,int ang);
        dbl TransitionFrequency(const int Fg, const int Fe) const;
      //    private:
        void UpdateLevelSizes();

    };
}
#endif // ELEMENT_HPP
