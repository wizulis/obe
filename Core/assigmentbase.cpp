#include "assigmentbase.hpp"
std::map<std::string,std::function<std::unique_ptr<OBE::BasicGenerator>(const OBE::AssigmentBase*,const OBE::DBContainer &)> > OBE::AssigmentBase::generators;
std::map<std::string,std::function<std::unique_ptr<OBE::BasicSystem>(const OBE::AssigmentBase*,const OBE::DBContainer &)> > OBE::AssigmentBase::systems;
std::map<std::string,std::function<std::unique_ptr<OBE::FrequencyCalculator>(const OBE::AssigmentBase*,const OBE::DBContainer &)> > OBE::AssigmentBase::frequencies;

namespace OBE {



    template<> std::unique_ptr<BasicGenerator> AssigmentBase::LoadModel(const DBContainer &p,const std::string & name) const{
        return LoadModel(p,name,generators);
    }

    template<> std::unique_ptr<BasicSystem> AssigmentBase::LoadModel(const DBContainer &p,const std::string & name) const {
        return LoadModel(p,name,systems);
    }

    template<> std::unique_ptr<FrequencyCalculator> AssigmentBase::LoadModel(const DBContainer &p,const std::string & name) const {
        return LoadModel(p,name,frequencies);
    }

    template<class T> std::unique_ptr<T> AssigmentBase::LoadModel (
            const DBContainer & p,
            const std::string & name,
            const std::map< std::string,std::function<std::unique_ptr<T>(const AssigmentBase*,const DBContainer &)>> & lib
            ) const {

        auto itr=lib.find(name);
        if(itr==lib.end()){
            throw std::runtime_error(std::string("Unregistred type: ")+name);
        }
        return itr->second(this,p);

    }

    dbl  AssigmentBase::GetParam(PARAMS a) const{

        auto itr=param.find(a);

        if(itr!=param.end())
            return itr->second;

        throw std::runtime_error("Unable to find param");
    }

    void AssigmentBase::LoadModels(const DBContainer &p){

        if(!generator)
            generator=LoadModel<BasicGenerator>(p,modelObjects[MODELTYPES::GENERATOR]);
        if(!system)
            system=LoadModel<BasicSystem>(p,modelObjects[MODELTYPES::SYSTEM]);
        if(!trans.Energy_calc)
            trans.Energy_calc=LoadModel<FrequencyCalculator>(p,modelObjects[MODELTYPES::FREQUENCY]);
        trans.Energy_calc->UpdateLevels();
    }

    void AssigmentBase::loadTransition(const DBContainer & p){
        auto level_gr=p.get<int>("level_ground");
        auto level_ex=p.get<int>("level_exited");

        //We don't have an system generator that could calculte frequency for us - so we need to calculate it by hand
        model.laser_freq=trans.TransitionFrequency(level_gr,level_ex);

        auto throw_out=p.get_opt<DBContainer>("level_throwout");

        if(throw_out){
            auto throw_out_map=throw_out.get();
//            std::cout << throw_out_map << std::endl;
/*            for(auto & item:throw_out_arr){
                auto lvl=item["LEVEL"].String();

                int ang_mom;
                std::stringstream(item["ang_mom"].toString(false,false))>>ang_mom;
                trans.RemoveLevel(lvl,ang_mom);b
            }*/
        }
        trans.UpdateLevelSizes();

    }
    void AssigmentBase::loadModelCoeficients(const DBContainer &p){

        if(!p.fill(model.laser_spectral_width,"laser_width"))
            throw std::runtime_error("Laser spectral width not specified or specified incorrectly");
        model.laser_spectral_width*=1e6*2*M_PI;

        if(!p.fill(model.fwhm,"fwhm"))
            throw std::runtime_error("Laser FWHM not specified or specified incorrectly");
        model.fwhm*=1e-3;

        if(!p.fill(model.temperature,"temperature"))
            throw std::runtime_error("Temperature not specified or specified incorrectly");

        if(!p.fill(model.trans_rate_coef,"trans_rate_coef"))
            throw std::runtime_error("Temperature not specified or specified incorrectly");

        if(!p.fill(model.max_radius,"max_radius"))
            throw std::runtime_error("Max radius not specified or specified incorrectly");

        if(!p.fill(model.colision_prob,"colision_prob"))
            throw std::runtime_error("Colision probability not specified or specified incorrectly");
        model.colision_prob*=1e6*2*M_PI;

        if(!p.fill(model.regions,"regions"))
            throw std::runtime_error("Regions not specified or specified incorrectly");

    }

    void AssigmentBase::loadPolarizations(const DBContainer &p){

        auto map=p.get<DBContainer>("laser_polarization");
        laser_polarization=PolarizationVector::FromDBContainer(map);

        map=p.get<DBContainer>("fluroescence_polarization");

        for(auto & fluro_elem: map)
                fluroescence_polar_list.push_back(PolarizationVector::FromDBContainer(boost::any_cast<DBContainer>(fluro_elem.second)));
    }

    void AssigmentBase::loadModelInfo(const DBContainer &p){

        modelObjects[MODELTYPES::FREQUENCY]=p.get<std::string>("frequency");
        modelObjects[MODELTYPES::GENERATOR]=p.get<std::string>("equation");
        modelObjects[MODELTYPES::SYSTEM]=p.get<std::string>("system");
    }
}
