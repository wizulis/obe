#ifndef BASICGENERATOR_HPP
#define BASICGENERATOR_HPP
#include "typedefinitions.hpp"
#include "rho.hpp"
#include <mongo/client/dbclient.h>
#include "dbcontainer.hpp"
namespace OBE {

    class AssigmentBase;
    class PolarizationVector;

    class BasicGenerator{

    protected:
        const AssigmentBase * asg;
        using State = Rho::State;
        dbl dopler_speed;
        vec<dbl> laser_intensity;
        vec<dbl> transit_rate;
        vec<dbl> dopler_vec;
        dbl transit_speed;
    protected:

       double ColisionRateE() const;
    public:

        BasicGenerator(const AssigmentBase * assig,const DBContainer &p);

        void SetDopler(const vec<dbl> & dop);
        dbl GetDopplerStdDev() const;
        void SetDopplerSpeed(const dbl val);
        dbl DoplerShift() const;
        dbl GetSpeedProbability(const dbl speed) const;

        dbl GetTransitProbability(const dbl speed) const;
        virtual void SetTransitSpeed(const dbl speed);
        dbl TransitSpeed()const;


        virtual void SetLaserIntensities(const vec<dbl> & laser);

        virtual vec<dbl> CalculateFluorescence(const Rho & ) const;
        dbl TransitionFrequency(const State &i,const State & j)const;
        virtual vec<cvec> CreateRow(const State &,const State &,const Rho &)const;


        virtual dbl Gamma(const State &,const State &,const State &,const State &)const;
        virtual cdbl TransitionAmplitude(const State &i,const State &j,const PolarizationVector & vec)const;

        virtual dbl ColisionRate() const;
        virtual dbl TransitRate(int n) const;
        virtual const vec<dbl> & TransitRateVector(const vec<dbl> & regLen=vec<dbl>());

        virtual void FillRHVec(const Rho &,cvec & ,int region=0) const ;
        virtual void InitDensityMatrix(Rho &)const;
        virtual std::unique_ptr<Rho> GetRho() const;
        virtual bool IsOCSupported()const{return false;}
        virtual void UpdateCache(const Rho & r){}
        virtual ~BasicGenerator(){}

        virtual Rho::LEVEL Exited() const {return Rho::LEVEL::EXITED;}
        virtual Rho::LEVEL Ground() const {return Rho::LEVEL::GROUND;}
    protected:

        virtual vec<cvec> CreateGroundRow(const State &,const State &,const Rho &)const;
        virtual vec<cvec> CreateExitedRow(const State &,const State &,const Rho &)const;

    };
}
#endif // BASICGENERATOR_HPP
