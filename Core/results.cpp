#include "results.hpp"
#include <stdexcept>
#include <map>

namespace OBE {

    void Results::AddPoint(dbl x, dbl y,dbl r, dbl S, const vec<dbl> &fluor){

        data.push_back(Results::Point());

        Results::Point &p=data.back();
        p.x=x;
        p.y=y;
        p.S=S;
	p.r=r;
        p.fluorescence=fluor;
    }

    vec<dbl> Results::GetFullFluorescence()const {
        vec<dbl> ret;
        dbl full_area=0;
        for(const auto & p:data){
            if(ret.size()==0){
                ret.resize(p.fluorescence.size(),0);
            }
            if(ret.size()!=p.fluorescence.size())
                throw std::runtime_error("Results size mismatch");
            full_area+=p.S;
            for(std::size_t i=0;i<ret.size();i++){
                ret[i]+=p.S*p.fluorescence[i];
            }
        }
        for(dbl & r:ret)
            r/=full_area;
        return ret;
    }
    vec<Results::Point> Results::GetFluorDistribution() const {

        std::map<double,std::map<double,Point>> points;

        for(const Point & p:data){
            Point & p_n=points[p.x][p.y];
            if(p_n.fluorescence.size()==0){
	      p_n.x=p.x;
	      p_n.y=p.y;
	      p_n.r=p.r;
	      p_n.S=p.S;
	      p_n.fluorescence=p.fluorescence;
            }else{
                for(int i=0;i<p.fluorescence.size();i++)
                   p_n.fluorescence[i]+=p.fluorescence[i];
            }
        }

        vec<Point> ret;
        ret.reserve(100);

        for(auto & p1:points)
            for(auto & p2:p1.second)
                ret.push_back(p2.second);

        return ret;
    }

}
