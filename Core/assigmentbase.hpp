#ifndef ASSIGMENTBASE_HPP
#define ASSIGMENTBASE_HPP
#include <functional>

#include "typedefinitions.hpp"
#include "transition.hpp"
#include "polarizationvector.hpp"

#include "basicgenerator.hpp"
#include "basicsystem.hpp"
#include "dbcontainer.hpp"

namespace OBE {




    class AssigmentBase
    {

        static std::map<std::string,std::function<std::unique_ptr<BasicGenerator>(const AssigmentBase*,const DBContainer &)> > generators;
        static std::map<std::string,std::function<std::unique_ptr<BasicSystem>(const AssigmentBase*,const DBContainer &)> > systems;
        static std::map<std::string,std::function<std::unique_ptr<FrequencyCalculator>(const AssigmentBase*,const DBContainer &)> > frequencies;

    public:
        template<class T>
        static void  registerGenerator(const std::string & name){
            generators[name]=[](const AssigmentBase * parent,const DBContainer & data){
                return std::unique_ptr<BasicGenerator>(new T(parent,data));
            };
        }
        template<class T>
        static void  registerSystem(const std::string & name){
            systems[name]=[](const AssigmentBase * parent,const DBContainer & data){
                return std::unique_ptr<BasicSystem>(new T(parent,data));
            };
        }
        template<class T>
        static void  registerFrequencyCalculator(const std::string & name){
            frequencies[name]=[](const AssigmentBase * parent,const DBContainer & data){
                return std::unique_ptr<FrequencyCalculator>(new T(parent,data));
            };
        }

    public:


        template<class T> std::unique_ptr<T> LoadModel(const DBContainer & ,const std::string &) const ;
        template<class T> std::unique_ptr<T> LoadModel(const DBContainer &,const std::string &,
                                                       const std::map< std::string,
                                                       std::function<std::unique_ptr<T>(const AssigmentBase*,const DBContainer &)>> & ) const ;
    public:

        enum class PARAMS:int{RABI,MAGNETIC_FIELD,DOPLER_START,DOPLER_END,DOPLER_STEP,LASER_OFFSET};
        dbl GetParam(PARAMS a) const;
        std::map<PARAMS,dbl> param;

        struct ModelSettings{
            dbl laser_freq;
            dbl laser_spectral_width;
            dbl fwhm;
            dbl temperature;
            dbl max_radius;
            dbl trans_rate_coef;
            dbl colision_prob;
            int regions;
        } model;

        Transition trans;

        PolarizationVector laser_polarization;
        vec<PolarizationVector> fluroescence_polar_list;

        AssigmentBase():model{},trans{} {}
        enum class MODELTYPES {FREQUENCY,GENERATOR,SYSTEM};
    private:
        std::map<MODELTYPES,std::string> modelObjects;


    public:
        void loadTransition(const DBContainer & p);
        void loadModelCoeficients(const DBContainer & p);
        void loadPolarizations(const DBContainer & p);
        void loadModelInfo(const DBContainer & p);
        void LoadModels(const DBContainer & p);

        std::unique_ptr<BasicGenerator> generator;
        std::unique_ptr<BasicSystem> system;
    };
}
#endif // ASSIGMENTBASE_HPP
