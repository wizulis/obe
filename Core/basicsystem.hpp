#ifndef BASICSYSTEM_HPP
#define BASICSYSTEM_HPP
#include <memory>
#include <mongo/client/dbclient.h>

#include "typedefinitions.hpp"
#include "equationmatrix.hpp"
#include "results.hpp"
#include "dbcontainer.hpp"
namespace OBE {

    class AssigmentBase;

    class BasicSystem{

    protected:
            std::shared_ptr<Results> results;

            const AssigmentBase * asg;
    public:
            BasicSystem(const AssigmentBase * parent,const DBContainer &p);

            void Rebind(const AssigmentBase * parent);

            virtual void Calculate(const dbl doppler_from, const dbl doppler_to,const dbl doppler_step);

            std::shared_ptr<Results> GetResults();
            virtual ~BasicSystem(){}
    protected:
            virtual vec<dbl> CreateRadiusVector()const;
            virtual vec<dbl> CreateIntensityVector(const vec<dbl> &) const;
            virtual vec<std::unique_ptr<Equation_Matrix>> GenerateMatrix(int reg) const;
            virtual std::map<dbl,vec<dbl> > Solve(vec<std::unique_ptr<Equation_Matrix>> &,const vec<dbl> &) const;

    };
}
#endif // BASICSYSTEM_HPP
