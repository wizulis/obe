#ifndef MONGOCONNECTION_H
#define MONGOCONNECTION_H
#include "typedefinitions.hpp"
#include "mongo/client/dbclient.h"
#include <mutex>
#include <iostream>

namespace OBE{
    namespace MongoConnection{


        class Connector{


           class AuthHook: public mongo::DBConnectionHook{

               void onCreate(mongo::DBClientBase *conn)
               {
                   std::string err;
                   if(!conn->auth("admin",MONGO_USER,MONGO_PASS,err)){
                        std::cerr << "Unable to connecto to mongo "
                                    << err;
                        exit(-1);
                   }

               }
               void onHandedOut(mongo::DBClientBase *)
               {

               }
               void onDestroy(mongo::DBClientBase *)
               {

               }

           };

        public:

            mongo::ConnectionString cstr;
            Connector(){
                mongo::pool.addHook(new AuthHook());
                std::string er;
                cstr=mongo::ConnectionString::parse(MONGO_HOST,er);
            }


        };
        inline std::shared_ptr<mongo::ScopedDbConnection>   getConnection(){
            //We want this as static object to make initialization atomic
            static Connector c;

            return std::shared_ptr<mongo::ScopedDbConnection>(new mongo::ScopedDbConnection(c.cstr));
        }
    }

}
#endif // MONGOCONNECTION_H
