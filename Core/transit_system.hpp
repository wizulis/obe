#ifndef TRANSITSYSTEM_HPP
#define TRANSITSYSTEM_HPP

#include "basicsystem.hpp"
#include "assigment.hpp"

namespace OBE {

    class AssigmentBase;
    template<typename T>
    class TransitSystem: public T{

    public:
        TransitSystem(const AssigmentBase * parent, const DBContainer &p):T(parent,p){
        }


            virtual void Calculate(const dbl doppler_from, const dbl doppler_to,const dbl doppler_step) override {

            this->results=std::shared_ptr<Results>(new Results);

            this->asg->trans.Energy_calc->SetB(this->asg->GetParam(Assigment::PARAMS::MAGNETIC_FIELD));

            auto rad_v=this->CreateRadiusVector();
            this->asg->generator->SetLaserIntensities(this->CreateIntensityVector(rad_v));

            auto r=this->asg->generator->GetRho();
            this->asg->generator->UpdateCache(*r);


            if(this->asg->model.regions==1)
                this->asg->generator->TransitRateVector(vec<dbl>(1,this->asg->model.fwhm));
            else
                this->asg->generator->TransitRateVector(vec<dbl>(rad_v.size(),std::abs(rad_v[0]-rad_v[1])));

            dbl max_speed=2*this->asg->generator->TransitSpeed();
            for(dbl start=max_speed/20;start<max_speed;start+=max_speed/10){
                this->asg->generator->SetTransitSpeed(start);
                auto prob_trans=this->asg->generator->GetTransitProbability(start)*max_speed/10;


               dbl dopplerCoef=this->asg->generator->GetDopplerStdDev();
               dbl tmp_val=0,tmp_area=0;
               for(dbl val=(doppler_from+doppler_step/2.0);
                    val<doppler_to;
                    val+=doppler_step){


                    this->asg->generator->SetDopplerSpeed(val*dopplerCoef);

                    auto equations=this->GenerateMatrix(rad_v.size());

                    auto res = this->Solve(equations,rad_v);

                    dbl dx=2*this->asg->model.max_radius/this->asg->model.regions*this->asg->model.fwhm/SIGMA_COEF;
                    dbl prob=this->asg->generator->GetSpeedProbability(val*dopplerCoef)*doppler_step*dopplerCoef*prob_trans;

                    for(auto & i:res){
                      tmp_val+=std::abs(i.first)*dx*i.second[0]*prob/prob_trans;
                      tmp_area+=std::abs(i.first)*dx;
                      for(auto & j:i.second){

                          j*=prob;
                      }
                      this->results->AddPoint(i.first,0,std::abs(i.first),std::abs(M_PI*dx*i.first),i.second);
                    }
                }
            }
        }
    };
}

#endif // MTSYSTEM_HPP
