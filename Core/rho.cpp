#include "rho.hpp"
#include <iostream>

#include "transition.hpp"

namespace OBE {

/*! \class Rho

    \brief Container class for storing and accessing density matrix

    The container class stores density matrix elements and some extra information for fast acess
    The class is created for storing density matrix of multiple levels, but the levels are not interconected
    This is used for equations with only Zeeman but no optical coherences.

    To use this for optical coherences one should extend this class.
    TODO: exted this class

*/


/*!
 * \brief Rho::Rho
 * \param Transition reference for angular momentum and level_size parametr references
 */
Rho::Rho(const OBE::Transition & trans):angMomMap(trans.Angular_moment),levelSize(trans.Level_sizes){


}

void Rho::AcquireMemory(){


    data.resize(MatrixSize());

}


/*!
 * \brief Rho::ref returns proxy class for iterations over states of a LEVEL
 * \param lvl LEVEL to iterate over
 * \return initialized proxy class
 */

Rho::RhoRef Rho::ref(Rho::LEVEL lvl)const{
    return RhoRef(*this,lvl);
}


int Rho::index(const State &a, const State &b) const{
    if(a.lvl!=b.lvl)
        throw std::runtime_error("Rho does not support different level elements");
    int lvlIdx=static_cast<int>(a.lvl);

    int ind=0;

    //Offset index to correct LEVEL
    for(int i=0;i<lvlIdx;i++)
        ind+=std::pow(levelSize[i],2);


    for(const int i:angMomMap[a.lvl]){
        //first index moves through rows
        if(i < a.F)
            ind+=(i+1)*levelSize[lvlIdx];
        else if(i==a.F)
            for(int mI=-i ; mI < a.mF ; mI+=2)
                ind+=levelSize[lvlIdx];
        //second though columns
        if(i < b.F)
            ind+=(i+1);
        else if(i==b.F)
            for(int mI=-i ; mI < b.mF ; mI+=2)
                ind++;
    }
    return ind;
}
/*!
 * \brief Rho::operator () used to access elements of state vector
 * \param a
 * \param b
 * \return reference to density matrix element of (a,b)
 */

const cdbl & Rho::operator ()(const Rho::State & a,const Rho::State & b) const {
    return data[index(a,b)];
}

cdbl & Rho::operator ()(const Rho::State & a, const Rho::State & b){
    return data[index(a,b)];
}

Rho & Rho::operator =(const Rho & other){
    data=other.data;
    levelSize=other.levelSize;
    angMomMap=other.angMomMap;
}

/*!
 * \brief Rho::DataHandler
 * \return underlaying data structure
 */
cvec & Rho::DataHandler(){
    return data;
}

const cvec & Rho::DataHandler() const{
    return data;
}


int Rho::LevelSize(Rho::LEVEL lvl) const{
    // We could check if the lvl is within allowed bounds and return 0 if not,
    // But that would mean that there is a bug thats hiden because of this.
    return levelSize[static_cast<int>(lvl)];
}
vec<int> Rho::AngMom(Rho::LEVEL lvl) const{
    return angMomMap[lvl];
}

int Rho::MatrixSize()const {

    int matSize=0;
    for(auto &i:levelSize)
        matSize+=i*i;
    return matSize;
}

/*!
    \class Rho::State

    \brief Small struct to hold parameters of a level definition

    This struct is used in cojuction with Rho::State::Iter to iterate over states in density matrix
*/

/*!
 * \brief Rho::State::State constructor to initilize constant values
 * \param initF angular momentum
 * \param initMf angular momentum projection
 * \param initLvl referenced LEVEL
 */
Rho::State::State(const int initF, const int initMf, const LEVEL initLvl,const Rho &p):F(initF),mF(initMf),lvl(initLvl),parent(p){

}


/*!
    \class Rho::State::Iter

    \brief Small iterator class to allow iteration over range of States

    Used in conjuction with Rho::RhoRef allows iteration in range for loop
*/

/*!
 * \brief Rho::State::Iter::Iter
 * \param par parent to iterate over
 * \param l LEVEL of the parent to iterate over
 */

Rho::State::Iter::Iter(const Rho &par, LEVEL l):parent(par),lvl(l){

    angMomStart=parent.angMomMap[l].begin();
    angMomEnd=parent.angMomMap[l].end();
    if(angMomStart!=angMomEnd){
        F=*angMomStart;
        mF=-F;
    }

}

/*!
 * \brief Rho::State::Iter::operator ++
 * \return iterator in next position
 */

Rho::State::Iter & Rho::State::Iter::operator ++ (){
    if(angMomStart==angMomEnd){
        F=0;
        mF=0;
        return *this;
    }
    mF+=2;
    if(mF>F){
        ++angMomStart;
        if(angMomStart==angMomEnd){
            F=0;
            mF=0;
            return *this;
        }
        F=*angMomStart;
        mF=-F;
    }
    return *this;
}

/*!
 * \brief Rho::State::Iter::operator !=
 * \param other
 * \return
 */
bool Rho::State::Iter::operator !=(const Rho::State::Iter & other) const {
    return !(*this==other);
}

/*!
 * \brief Rho::State::Iter::operator ==
 * \param other
 * \return
 */

bool Rho::State::Iter::operator ==(const Rho::State::Iter & other) const {
    if(lvl==other.lvl && angMomStart==other.angMomStart && F==other.F && mF==other.mF)
        return true;
    return false;

}

/*!
 * \brief Rho::State::Iter::operator *
 * \return State object with iterators state
 */

const Rho::State Rho::State::Iter::operator *()const{
    return State(F,mF,lvl,parent);
}

/*!
 * \brief Rho::State::Iter::toEnd move iterator to end quickly
 */
void Rho::State::Iter::toEnd(){
    F=0;
    mF=0;
    angMomStart=angMomEnd;
}


/*!
    \class Rho::RhoRef

    \brief Proxy class for Rho::State::Iter for use with range loops

*/

 /*!
 * \brief Rho::RhoRef::RhoRef
 * \param par
 * \param l
 */

Rho::RhoRef::RhoRef(const Rho &par, const LEVEL l):parent(par),lvl(l){

}

/*!
 * \brief Rho::RhoRef::begin
 * \return  first iterator of referenced level
 */
Rho::State::Iter Rho::RhoRef::begin(){
    return Rho::State::Iter(parent,lvl);
}

/*!
 * \brief Rho::RhoRef::end
 * \return  iterator after the last valid object
 */

Rho::State::Iter Rho::RhoRef::end(){
    Rho::State::Iter endItr(parent,lvl);
    endItr.toEnd();
    return endItr;
}


vec<vec<dbl>> Rho::Pop()const {

    vec<vec<dbl>> ret;
    ret.push_back(vec<dbl>{});
    for(State i:this->ref(Rho::LEVEL::GROUND))
        ret[0].push_back((*this)(i,i).real());
    ret.push_back(vec<dbl>{});
    for(State i:this->ref(Rho::LEVEL::EXITED))
        ret[1].push_back((*this)(i,i).real());
    //ret.push_back(vec<dbl>{});
    //    for(State i:this->ref(Rho::LEVEL::EXITED_2))
    //    ret[2].push_back((*this)(i,i).real());

    return ret;
}
std::ostream & operator <<(std::ostream & ost,const Rho & r){
    using State=Rho::State;
    ost << "Ground\n";
    for(State i:r.ref(Rho::LEVEL::GROUND)){
        ost << "| ";
        for(State j:r.ref(Rho::LEVEL::GROUND)){
            ost << r(i,j) <<"\t";
        }
        ost << "|\n";
    }
    ost << "\nExited\n";

    for(State i:r.ref(Rho::LEVEL::EXITED)){
        ost << "| ";
        for(State j:r.ref(Rho::LEVEL::EXITED)){
            ost << r(i,j) <<"\t";
        }
        ost << "|\n";
    }
    return ost;
}



int RhoOC::index(const State &a, const State &b) const{

    int lvlIdx_a=static_cast<int>(a.lvl);
    int lvlIdx_b=static_cast<int>(b.lvl);

    int row_size=0;
    for(int i=0;i<levelSize.size();i++)
        row_size+=levelSize[i];
    int ind=0;

    //Offset index to correct LEVEL
    for(int i=0;i<lvlIdx_a;i++)
        ind+=levelSize[i]*row_size;
    for(int i=0;i<lvlIdx_b;i++)
        ind+=levelSize[i];


    for(const int i:angMomMap[a.lvl]){
        //first index moves through rows
        if(i < a.F)
            ind+=(i+1)*row_size;
        else if(i==a.F)
            for(int mI=-i ; mI < a.mF ; mI+=2)
                ind+=row_size;
    }

    for(const int i:angMomMap[b.lvl]){
        //second though columns
        if(i < b.F)
            ind+=(i+1);
        else if(i==b.F)
            for(int mI=-i ; mI < b.mF ; mI+=2)
                ind++;
    }
    return ind;
}

int RhoOC::MatrixSize()const {
    int size=0;
    for(auto i:levelSize)
        size+=i;
    return std::pow(size,2);
}

Rho2L::Rho2L(const Transition &trans1, const Transition &trans2):Rho(GetSizes(trans1,trans2),GetAngMomMap(trans1,trans2)){

}

    vec<int> Rho2L::GetSizes(const Transition & trans1, const Transition & trans2){
        auto ret=trans1.Level_sizes;
        ret.push_back(trans2.Level_sizes.back());
        return ret;
    }

    vec<vec<int>> Rho2L::GetAngMomMap(const Transition &trans1, const Transition &trans2){
        auto ret=trans1.Angular_moment;
        ret.push_back(trans2.Angular_moment.back());
        return ret;
    }

    std::ostream & operator <<(std::ostream & ost,const Rho::State & i){
        ost << "|" << i.lvl << "," << i.F << "," << i.mF << ">";
        return ost;
    }


}
