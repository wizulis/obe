#ifndef NUMERICSYSTEM_HPP
#define NUMERICSYSTEM_HPP
#include "basicsystem.hpp"

namespace OBE{
    class NumericSystem : public BasicSystem
    {
    public:

        NumericSystem(const AssigmentBase * parent,const DBContainer&p):BasicSystem(parent,p){}
        virtual vec<std::unique_ptr<Equation_Matrix>> GenerateMatrix(int reg) const override;
        virtual std::map<dbl,vec<dbl>> Solve(vec<std::unique_ptr<Equation_Matrix> > &, const vec<dbl> &) const override;
    };
}

#endif // NUMERICSYSTEM_HPP
