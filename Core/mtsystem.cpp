#include "mtsystem.hpp"
#include "assigment.hpp"

#include <chrono>
namespace OBE{

    MTSystem::MTSystem(const AssigmentBase * parent, const DBContainer &p):BasicSystem(parent,p){
    }


    void MTSystem::Calculate(const dbl doppler_from, const dbl doppler_to,const dbl doppler_step){

        results=std::shared_ptr<Results>(new Results);
        asg->trans.Energy_calc->SetB(asg->GetParam(Assigment::PARAMS::MAGNETIC_FIELD));

        auto rad_v=CreateRadiusVector();
        auto r=asg->generator->GetRho();
        asg->generator->UpdateCache(*r);

        dbl dr=2*asg->model.max_radius*asg->model.fwhm/SIGMA_COEF/asg->model.regions;


        for(int reg=asg->model.regions;reg>0;reg-=2){
            auto areas=SetupLaserProfile(reg,rad_v);
            dbl y=(asg->model.regions-reg+1.0)/2*dr;
            vec<dbl> xvec;

            for(auto p:areas){
                xvec.push_back(p.first);;
            }

            dbl dopplerCoef=asg->generator->GetDopplerStdDev();
            for(dbl val=(doppler_from+doppler_step/2.0);
                val<doppler_to;
                val+=doppler_step){


                asg->generator->SetDopplerSpeed(val*dopplerCoef);
                //auto start = std::chrono::system_clock::now();
                auto equations=GenerateMatrix(reg);
                auto res = Solve(equations,xvec);
            //    auto e = std::chrono::system_clock::now();
              //   std::cout << std::chrono::duration_cast<std::chrono::milliseconds>(e-start).count () << std::endl;

                dbl prob=asg->generator->GetSpeedProbability(val*dopplerCoef)*doppler_step*dopplerCoef;

                //std::cout << ">>\t";
                int idx=0;
                for(auto & i:res){

                  for(auto & j:i.second){

                      j*=prob;
                  }
                  //std::cout << i.second[0] << "\t";
                  //if(idx+1==reg/2)
                  //    std::cout << ">><<" << "\t";
                  //idx++;
                  results->AddPoint(i.first,y,areas[i.first].first,areas[i.first].second,i.second);
                }
                //std::cout << std::endl;

            }
        }
    }
    std::map<double,std::pair<double,double>> MTSystem::SetupLaserProfile(int reg,vec<dbl> rad_copy){

        using std::pow;
        using std::sqrt;
        const int s=rad_copy.size();
        const int reg_dec=asg->model.regions-reg;
        const dbl y=std::abs(rad_copy[reg/2-1]);

        if(reg!=s)
            rad_copy.erase(rad_copy.begin()+(s/2)-reg_dec/2,rad_copy.begin()+(s/2)+reg_dec/2);
        asg->generator->SetLaserIntensities(CreateIntensityVector(rad_copy));


        dbl dr=2*asg->model.max_radius*asg->model.fwhm/SIGMA_COEF/asg->model.regions;


        vec<dbl> sizes(rad_copy.size());
        std::map<double,std::pair<double,double>> areas;
        for(int c=0;c<reg;c++){
	  dbl rout=std::abs(rad_copy[c])+dr/2;
            dbl rin=rout-dr;
            dbl area=0;
            dbl dy=dr/20;
            dbl yi=y-dr/2+dy/2;
            for(int i=0;i<20;i++){
                area+=dy*(sqrt(pow(rout,2)-pow(yi,2))-sqrt(rin> y ? pow(rin,2)-pow(yi,2):0));
                yi+=dy;
            }
            if(c==reg/2-1)
	      areas[-sqrt(pow(rout,2)-pow(y,2))/2]=std::make_pair(std::abs(rad_copy[c]),area);
            else if(c==reg/2)
	      areas[sqrt(pow(rout,2)-pow(y,2))/2]=std::make_pair(std::abs(rad_copy[c]),area);
            else if(c<reg/2)
	      areas[- sqrt(pow(rad_copy[c],2)-pow(y,2))]=std::make_pair(std::abs(rad_copy[c]),area);
            else
	      areas[sqrt(pow(rad_copy[c],2)-pow(y,2))]=std::make_pair(std::abs(rad_copy[c]),area);

            sizes[c]=sqrt(rout*rout-y*y)-sqrt(rin>y ? rin*rin-y*y:0);
        }
        //std::cout << " Sizes " << sizes << std::endl;

        asg->generator->TransitRateVector(sizes);

        return areas;

    }

}
