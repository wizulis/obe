#include <mongo/client/dbclient.h>
#include <memory>
#include "assigment.hpp"
#include "mongoconnection.hpp"
#include <chrono>

using boost::any;

namespace OBE {


    DBContainer GetExperiment(const std::string & id){
        auto con=MongoConnection::getConnection();
        std::shared_ptr<mongo::DBClientCursor> cursor=(*con)->query("AlkaliModel.TheoryExperiments",QUERY( "_id" << mongo::OID(id)));

        if(!cursor->more()){
            throw std::runtime_error(std::string("Unable to find Experiment for ")+id);
        }
        con->done();
        return DBContainer(cursor->next());

    }

    DBContainer GetCalcDef(const std::string & id){
        auto con=MongoConnection::getConnection();
        std::shared_ptr<mongo::DBClientCursor> cursor=(*con)->query("AlkaliModel.TheoryCalcDefinitions",QUERY( "_id" << mongo::OID(id)));

        if(!cursor->more()){
            throw std::runtime_error(std::string("Unable to find CalcDefinition for ")+id);
        }
        con->done();
        return DBContainer(cursor->next());

    }

    DBContainer GetMod(const std::string & name){
        auto con=MongoConnection::getConnection();
        std::shared_ptr<mongo::DBClientCursor> cursor=(*con)->query("AlkaliModel.TheoryCalcModifications",QUERY( "name" << name));

        if(!cursor->more()){
            throw std::runtime_error(std::string("Unable to find modification")+name);
        }
        auto obj=DBContainer(cursor->next());
        con->done();
        obj.erase("name");
        return obj;
    }

    std::string RemoveOID(std::string val){
        std::string tmp("ObjectId('");
        auto pos=val.find(tmp);
        if(pos!=std::string::npos){
            val.replace(pos,tmp.size(),"");
            val.replace(val.find("')"),2,"");
        }
        return val;
    }

    DBContainer GetBaseDefinition(const std::string & calc_id){
        auto calc=DBContainer(GetCalcDef(calc_id));
        auto exp_str=RemoveOID(calc.get<std::string>("exp_id"));
        auto exp=GetExperiment(exp_str);
        exp.add(calc);
        return exp;
    }

    void AddMods(DBContainer & base,vec<std::string> &mods){

        for(auto & m:mods){
            base.add(GetMod(m));
        }
    }

    Assigment & Assigment::SetModification(OBE::vec<std::string> mods){
        std::swap(this->mods,mods);
        return *this;
    }

    Assigment & Assigment::SetExperiment(std::string calc){
        std::swap(this->calc_id,calc);
        return *this;
    }

    void Assigment::Load(){

        auto definition=GetBaseDefinition(calc_id);
        AddMods(definition,mods);
        trans=Transition(
                    definition.get<std::string>("element"),
                    definition.get<std::string>("transition")
                );

        loadTransition(definition);
        loadModelCoeficients(definition);
        loadPolarizations(definition);
        loadModelInfo(definition);
        LoadModels(definition);
        isLoaded=true;
    }


    Assigment & Assigment::SetRabi(dbl R){
        param[PARAMS::RABI]=R;
        return *this;
    }

    Assigment & Assigment::SetDopplerP(dbl dopplerFrom, dbl dopplerTo, dbl step){
        param[PARAMS::DOPLER_START]=dopplerFrom;
        param[PARAMS::DOPLER_END]=dopplerTo;
        param[PARAMS::DOPLER_STEP]=step;
        return *this;
    }

    Assigment & Assigment::SetB(dbl B){
        param[PARAMS::MAGNETIC_FIELD]=B*1e-4;
        return *this;
    }
    Assigment & Assigment::SetOffset(dbl off){
        param[PARAMS::LASER_OFFSET]=off*1e6*2*M_PI;
        return *this;
    }



    const std::shared_ptr<Results> & Assigment::GetResult() const {
        return res;
    }

    void Assigment::SetResult(std::shared_ptr<Results> & a){
        res=a;
    }

    bool Assigment::Run(){
        if(!isLoaded)
            Load();
        if(!(system && generator && trans.Energy_calc))
            throw std::runtime_error("Failed to load one of the models");

        auto start=std::chrono::system_clock::now();
	//std::cout << "Regions " << model.regions << std::endl;
        system->Calculate(GetParam(PARAMS::DOPLER_START),GetParam(PARAMS::DOPLER_END),GetParam(PARAMS::DOPLER_STEP));
        res=system->GetResults();
        auto end=std::chrono::system_clock::now();
        std::cout << GetParam(PARAMS::RABI) << "\t" << GetParam(PARAMS::MAGNETIC_FIELD) << " sent " << res->GetFullFluorescence()[0]
                  << " in " << std::chrono::duration_cast<std::chrono::seconds>(end-start).count() << " s" << std::endl;
        return SendResults();
    }

    bool Assigment::SendResults() const{

        mongo::BSONObjBuilder builder;

        mongo::BSONArrayBuilder arr_b;
        for(auto & m:mods)
            arr_b.append(m);
        builder << "Experiment" << calc_name
                << "calc_id" << calc_id
                << "mods" << arr_b.arr()
                << "Rabi" << GetParam(PARAMS::RABI)
                << "B" << GetParam(PARAMS::MAGNETIC_FIELD)*1e4
                << "LaserOffset" << GetParam(PARAMS::LASER_OFFSET);

	if(!Marker.empty())
        builder << "Marker" << Marker;

        mongo::BSONArrayBuilder arr_b2;


        for(const Results::Point & p:res->GetFluorDistribution()){

            mongo::BSONArrayBuilder tmp_b;
            for(auto i :p.fluorescence){
                tmp_b.append(i);
            }
            mongo::BSONArray ar=tmp_b.arr();
            arr_b2.append(BSON("x"<< p.x << "y" << p.y << "r" << p.r <<  "s" << p.S <<"f" << ar));
        }

        builder << "f_distr" << arr_b2.arr();

        mongo::BSONArrayBuilder fluor;

        vec<dbl> fullF=res->GetFullFluorescence();
        for(std::size_t i=0;i<fullF.size();i++){
            fluor.append(BSON("name" << fluroescence_polar_list[i].GetName() << "value" << fullF[i]));
        }
        builder << "Full" << fluor.arr();
        try{
            auto con=MongoConnection::getConnection();
            con->get()->insert("AlkaliModel.Results",builder.obj());
            con->done();
        }
        catch(std::exception & e){
            std::cout << "Error with writing results: "  << e.what();
            return false;
        }
        return true;

    }



}
