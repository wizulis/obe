#ifndef TWOLASERSYSTEM_HPP
#define TWOLASERSYSTEM_HPP
#include "basicsystem.hpp"

namespace OBE{
    class TwoLaserSystem : public BasicSystem
    {
    public:
        TwoLaserSystem(const AssigmentBase * parent,const DBContainer &p);

        virtual vec<std::unique_ptr<Equation_Matrix>> GenerateMatrix(int reg) const override;
        virtual std::map<dbl,vec<dbl> > Solve(vec<std::unique_ptr<Equation_Matrix>> &,const vec<dbl> &) const override;
    };
}
#endif // TWOLASERSYSTEM_HPP
