#include "basicsystem.hpp"
#include "basicgenerator.hpp"

#include "assigment.hpp"

#include <random>
#include <chrono>
namespace OBE {
    BasicSystem::BasicSystem(const AssigmentBase *parent, const DBContainer & p):asg(parent){



    }

    void BasicSystem::Rebind(const AssigmentBase *parent){
        asg=parent;
    }

    void BasicSystem::Calculate(const dbl doppler_from, const dbl doppler_to, const dbl doppler_step){

        results=std::shared_ptr<Results>(new Results);

        asg->trans.Energy_calc->SetB(asg->GetParam(Assigment::PARAMS::MAGNETIC_FIELD));

        auto rad_v=CreateRadiusVector();
        asg->generator->SetLaserIntensities(CreateIntensityVector(rad_v));

        auto r=asg->generator->GetRho();
        asg->generator->UpdateCache(*r);

        if(asg->model.regions==1)
            asg->generator->TransitRateVector(vec<dbl>(1,asg->model.fwhm));
        else
            asg->generator->TransitRateVector(vec<dbl>(rad_v.size(),
                                                       std::abs(rad_v[0]-rad_v[1])));

        dbl dopplerCoef=asg->generator->GetDopplerStdDev();

       for(dbl val=(doppler_from+doppler_step/2.0);
            val<doppler_to;
            val+=doppler_step){
//	std::cout << "doppler " << val  << std::endl;

            asg->generator->SetDopplerSpeed(val*dopplerCoef);

            auto equations=GenerateMatrix(rad_v.size());

            auto res = Solve(equations,rad_v);
	    
            dbl dx=2*asg->model.max_radius/asg->model.regions*asg->model.fwhm/SIGMA_COEF;
            dbl prob=asg->generator->GetSpeedProbability(val*dopplerCoef)*doppler_step*dopplerCoef;

            for(auto & i:res){

              for(auto & j:i.second){

                  j*=prob;
              }
              results->AddPoint(i.first,0,std::abs(i.first),std::abs(M_PI*dx*i.first),i.second);
            }
           // std::cout << val << std::endl;
        }

    }

    std::shared_ptr<Results> BasicSystem::GetResults(){
        return results;
    }

    vec<dbl> BasicSystem::CreateRadiusVector()const{

        dbl FWHM=asg->model.fwhm;

        dbl sigma=FWHM/SIGMA_COEF;
        if(asg->model.regions==1){
            vec<dbl> ret;
            ret.push_back(FWHM/2.0);
            return ret;
        }
        else{

            vec<dbl> ret;
            int reg=asg->model.regions;
            dbl maxR=asg->model.max_radius*sigma;

            for(dbl val=maxR*(-1.0+1.0/reg); // -maxR+(2*maxR/reg)/2 = maxR(-1+1/reg)
                val < maxR;
                val+= 2.0*maxR/reg){

                ret.push_back(val);
            }

            return ret;
        }
    }

    vec<dbl> BasicSystem::CreateIntensityVector(const vec<dbl> & points)const{

        dbl FWHM=asg->model.fwhm;
        dbl sigma=FWHM/SIGMA_COEF;



        dbl PeakIntesity=pow(2*M_PI*asg->GetParam(Assigment::PARAMS::RABI),2)*
                pow(1e6*HBAR/asg->trans.Reduced_matrix_elem,2)
                /AVERAGE_GAUSS;

        if(points.size()==1){
            vec<dbl> ret(1);
            ret[0]=PeakIntesity*AVERAGE_GAUSS;
            return ret;
        }
       vec<dbl> ret;
       for(const dbl & val: points)
           ret.push_back(PeakIntesity*exp(-pow(val,2)/2/sigma/sigma));

       return ret;

    }
    vec<std::unique_ptr<Equation_Matrix> > BasicSystem::GenerateMatrix(int reg)const{


        auto rho=asg->generator->GetRho();

        vec<std::unique_ptr<Equation_Matrix> > ret;
        for(int i=0;i<reg;i++)

            ret.push_back(std::unique_ptr<Equation_Matrix>(new UMFPACK_Matrix));


        vec<Rho::LEVEL> lvl_vec{Rho::LEVEL::GROUND,Rho::LEVEL::EXITED};

/*        for(Rho::LEVEL lvl_i : lvl_vec ){
            for(Rho::State i:rho->ref(lvl_i)){
               for(Rho::LEVEL lvl_j : lvl_vec ){
                    if(lvl_i!=lvl_j && !asg->generator->IsOCSupported())
                        continue;
                    for(Rho::State j:rho->ref(lvl_j)){
                        std::cout << i << "\t" << j << "\t";
                        std::cout << rho->index(i,j) << std::endl;
                        getchar();
                    }
                }
            }
        }*/

        for(Rho::LEVEL lvl_i : lvl_vec ){
            for(Rho::State i:rho->ref(lvl_i)){
                for(Rho::LEVEL lvl_j : lvl_vec ){

                   if(lvl_i!=lvl_j && !asg->generator->IsOCSupported())
                        continue;
                   for(Rho::State j:rho->ref(lvl_j)){
                        auto tmp=asg->generator->CreateRow(i,j,*rho);
                        for(std::size_t i=0;i<tmp.size();i++)
                            ret[i]->AddRow(tmp[i]);
                    }
                }

            }
        }
//        std::ofstream out("matrix_out");
//        ret[0]->print(out);
        return ret;
    }
    std::map<dbl,vec<dbl> > BasicSystem::Solve(vec<std::unique_ptr<Equation_Matrix>> & matrixVector,const vec<dbl> & xvec)const{

        std::map<dbl,vec<dbl> > ret;

        auto rho=asg->generator->GetRho();
        rho->AcquireMemory();
        asg->generator->InitDensityMatrix(*rho);

        cvec BVec;
        int i=0;
        for(auto & mat:matrixVector){

            asg->generator->FillRHVec(*rho,BVec,i);
            mat->Solve(rho->DataHandler(),BVec);
            ret[xvec[i++]]=asg->generator->CalculateFluorescence(*rho);
        }

        return ret;
    }


}
