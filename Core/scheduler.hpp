#ifndef SCHEDULER_HPP
#define SCHEDULER_HPP
#include <boost/threadpool.hpp>

#include "assigment.hpp"

namespace OBE{

    class Scheduler
    {
        boost::threadpool::pool tp;
        int threads;
    public:
        Scheduler(int init_threads=1);
        void AddAssigment(std::shared_ptr<Assigment> & asg);

    };
}
#endif // SCHEDULER_HPP
