#include "frequencycalculator.hpp"
#include "assigment.hpp"
#include "transition.hpp"



namespace OBE{


    void FrequencyCalculator::UpdateLevels(){
        if(asg->generator){
            ex=asg->generator->Exited();
            gr=asg->generator->Ground();

        }else{
            ex=Rho::LEVEL::EXITED;
            gr=Rho::LEVEL::GROUND;
        }
    }

    dbl FrequencyCalculator::Frequency(const Rho::State & i, const Rho::State & j) const{
        if(i.lvl==j.lvl){
            return GetEnergie(i)-GetEnergie(j);
        }else{

            if(i.lvl==ex){
                return GetEnergie(i)+asg->trans.Trans_freq-GetEnergie(j);
            }
            else{
                return -(GetEnergie(j)+asg->trans.Trans_freq-GetEnergie(i));
            }
        }
    }

}
