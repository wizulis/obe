#ifndef EQUATION_MATRIX_HPP
#define EQUATION_MATRIX_HPP

#include "typedefinitions.hpp"
#include "rho.hpp"
#include<armadillo>

namespace OBE {
    class Equation_Matrix
    {
        int size;
    public:
        Equation_Matrix(int matrixSize):size(matrixSize){}
        virtual void AddRow(const cvec &)=0;
        virtual void Solve(cvec &,const cvec &)=0;
        virtual void Reset()=0;
        virtual ~Equation_Matrix(){}
        virtual void print(std::ostream & os)=0;
    };


    class UMFPACK_Matrix: public Equation_Matrix{

        //Sistemas pieraksts UMFPACK
        cvec  A;
        vec<int>  Ai;
        vec<int>  Ap;
        std::size_t rowCounter,columnCounter,elementCounter,bvalCounter,count;

        void *Numeric, *Symbolic;

      public:

        UMFPACK_Matrix();
        void AddRow(const cvec &) override;
        void Solve(cvec &,const cvec &) override;
        void Reset() override;
        void print(std::ostream & os) override;

    };

    class SOR_Matrix: public Equation_Matrix{

        //Sistemas pieraksts UMFPACK
        cvec  A;
        vec<int>  Ai;
        vec<std::pair<int,int>>  Ap;


      public:

        SOR_Matrix();
        void AddRow(const cvec &) override;
        void Solve(cvec &,const cvec &) override;
        void Reset() override;
        void print(std::ostream & os) override;

    };

    class ARMA_Matrix: public Equation_Matrix{

        arma::cx_mat data;
        int row_id;
    public:
        ARMA_Matrix(int size);
        void AddRow(const cvec &) override;
        void Solve(cvec &,const cvec &) override;
        void Reset() override;
        void print(std::ostream & os) override;

        operator arma::cx_mat& (){return data;}

    };
}

#endif // MATRIX_HPP
