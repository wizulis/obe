#include "scheduler.hpp"

namespace OBE{
    Scheduler::Scheduler(int init_threads):tp(init_threads),threads(init_threads){

    }

    void Scheduler::AddAssigment(std::shared_ptr<Assigment> & asg){
        tp.schedule([asg](){asg->Run();});
    }

}
