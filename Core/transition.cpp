#include "transition.hpp"
#include <memory>
#include <sstream>
#include "mongoconnection.hpp"

namespace OBE{


    Transition::Transition(const std::string &elem, const std::string &transition)
        : Angular_moment(2),element(elem){


        auto con_ptr=MongoConnection::getConnection();
        auto con=con_ptr->get();

        std::shared_ptr<mongo::DBClientCursor> cursor=con->query("AlkaliModel.Elements",QUERY( "element" << elem));
        //std::cout << elem << std::endl;
        if(!cursor->more())
            throw std::runtime_error("Unable to find element");

        mongo::BSONObj p = cursor->next();

        Nuclear_spin=p["nuclear_spin"].Int();
        Atom_mass=p["atom_mass"].Double()*1e-3;
        J_ground=p["j_ground"].Int();
        L_ground=p["l_ground"].Int();
        Lande_J=p["lande_J"].Double();
        Lande_nuclear=p["lande_I"].Double();

        for(auto & lvl :p["ground_levels"].Array()){
            mongo::BSONObj obj=lvl.Obj();
            Ground_freq[obj["ang_momentum"].Int()]=obj["frequency"].Double()*1e6*2*M_PI;
        }

        for(int F=std::abs(Nuclear_spin-J_ground);F<=Nuclear_spin+J_ground;F+=2){
            Angular_moment[Rho::LEVEL::GROUND].push_back(F);
        }

        cursor=con->query("AlkaliModel.Transitions",QUERY("element" << elem << "transition" << transition));
        if(!cursor->more())
            throw std::runtime_error("Unable to find transition");

        p=cursor->next();

        J_exited=p["j_exited"].Int();
        L_exited=p["l_exited"].Int();

        Lande_J_exited=p["g_angular"].Double();
        Spont_rate=p["gamma"].Double()*1e6*2*M_PI;
        Trans_freq=p["trans_freq"].Double()*1e12*2*M_PI;
        Reduced_matrix_elem=p["red_matrix_elem"].Double()*ELECTRON_CHARGE*BOHR_RADIUS;

        for(auto & lvl :p["levels"].Array()){
            mongo::BSONObj obj=lvl.Obj();
            Exited_freq[obj["ang_momentum"].Int()]=obj["frequency"].Double()*1e6*2*M_PI;
        }

        for(int F=std::abs(Nuclear_spin-J_exited);F<=Nuclear_spin+J_exited;F+=2){
            Angular_moment[Rho::LEVEL::EXITED].push_back(F);
        }

        con_ptr->done();
        UpdateLevelSizes();
    }

    Transition & Transition::operator =(Transition && other){
        element=other.element;
        Nuclear_spin=other.Nuclear_spin;
        Atom_mass=other.Atom_mass;
        J_ground=other.J_ground;
        J_exited=other.J_exited;
        L_ground=other.L_ground;
        L_exited=other.L_exited;
        Lande_J_exited=other.Lande_J_exited;
        Spont_rate=other.Spont_rate;
        Trans_freq=other.Trans_freq;
        Reduced_matrix_elem=other.Reduced_matrix_elem;
        Lande_J=other.Lande_J;
        Lande_nuclear=other.Lande_nuclear;
        Ground_freq=std::move(other.Ground_freq);
        Exited_freq=std::move(other.Exited_freq);
        Angular_moment=std::move(other.Angular_moment);
        Energy_calc=std::move(other.Energy_calc);
	Level_sizes=std::move(other.Level_sizes);
        return *this;
    }

    void Transition::RemoveLevel(const std::string &lvl, int ang){

        vec<int> * tmpRef;

        if(lvl=="EXITED")
            tmpRef=&Angular_moment[Rho::LEVEL::EXITED];
        else if(lvl=="GROUND")
            tmpRef=&Angular_moment[Rho::LEVEL::GROUND];

        auto itr=std::find(tmpRef->begin(),tmpRef->end(),ang);
        if(itr==tmpRef->end())
            throw std::runtime_error("Unable to find level to throw out");
        else tmpRef->erase(itr);

        UpdateLevelSizes();
    }

    dbl Transition::TransitionFrequency(const int Fg, const int Fe) const{
        auto ground_itr=Ground_freq.find(Fg);
        auto exited_itr=Exited_freq.find(Fe);

        if(ground_itr==Ground_freq.end() || exited_itr==Exited_freq.end())
            throw std::runtime_error("Transition does not have named Fg or Fe");

        return Trans_freq-ground_itr->second+exited_itr->second;

    }
     void Transition::UpdateLevelSizes(){
         if(Level_sizes.size()!=Angular_moment.size()){
             Level_sizes.resize(Angular_moment.size());
         }
         for(auto &i:Level_sizes)
            i=0;

         for(int i=0;i<2;i++){
	   {
        // std::cout << "Updating lvl " << i << std::endl;
	     auto & lvl=Angular_moment[i];
             for(const auto & y:lvl)
                    Level_sizes[i]+=y+1;
        // std::cout << "Lvl Size:" << Level_sizes[i] << std::endl;
	   }
         }

     }
}
